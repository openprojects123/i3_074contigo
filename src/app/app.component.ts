import {
  Component,
  ViewChild
}                           from '@angular/core';
import { Nav }              from 'ionic-angular';
import { StatusBar }        from '@ionic-native/status-bar';

import {Utils}              from "../providers/utils-service";
import {GLOBALS}            from "./globals";
import {UserService}        from "../providers/user-service";
import { Events }           from 'ionic-angular';

let TAG = "MyApp";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  public rootPage:string;

  public menuEntries = [
    {title: '074 Contigo',        component: "InicioTabsPage",         icon:"imgMenu074"},
    {title: 'TAG',                component: "TagPage",                icon:"imgMenuTAG"},
    //{title: 'Solicitudes',        component: "SolicitudesTabsPage", icon:"imgMenuSolicitud"},
    {title: 'Reportes',           component: "ReportesTabsPage",       icon:"imgMenuSolicitud"},
    {title: 'Cultura vial',       component: "CulturaVialPage",        icon:"imgMenuSenhales"},
    {title: 'Alerta amber',       component: "AlertaAmberPage",        icon:"imgMenuAlertaAmber"},
    {title: 'Comunicación social',component: "ComunicacionSocialPage", icon:"imgMenuComSocial"},
    {title: 'Mi cuenta',          component: "UserAccountTabsPage",    icon:"imgMenuPerfil"},
    {title: 'Juego',              component: "JuegoPage",              icon:"imgMenuJuego"},
    {title: 'Realidad aumentada', component: "RealidadAumPage",        icon:"imgMenuRA"},
  ];

  public activePage:string;

  public avatarName:string;
  public fullName: string = "";

  constructor(statusBar: StatusBar,
              private events: Events,
              private userSvc: UserService) {
    let timeObj = Utils.getDate();
    let Hoy     = timeObj.YMD;
    let hora    = timeObj.HMS;
    Utils.LOG.d(TAG, "["+Hoy+" "+hora+"]: Bienvenido", "constructor()");

    //dentro de WarmupPage seteare la raiz correcta...
    this.rootPage = "WarmupPage";
                    //"InicioTabsPage";

    Utils.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();

      //let loader = Utils.showLoading();
      this.userSvc.loadLocalUser().subscribe(resp_bool=>{
        //loader.dismiss();

        let tabId:number;
        if(this.userSvc.isLoggedIn()){
          let extraInfo:any = this.userSvc.getExtraInfo();
          this.avatarName   = extraInfo.avatar || 'unknown';
          this.fullName     = extraInfo.nombre +" "+extraInfo.paterno;

          tabId = 0;//0:pagina Inicio (tramos, mapa, etc)
          this.activePage = this.menuEntries[tabId].title;
        }
        else{
          tabId = 6;//<-- (6) pagina Sesion (registro, login, perfil)
          this.activePage = this.menuEntries[tabId].title;//"Mi cuenta"
          this.avatarName = 'unknown';
        }
      }, reason=>{
        Utils.showAlert("No se puede iniciar la aplicación ya que"+reason);
      })
    });

    events.subscribe('avatar:changed', (extraInfo) => {
      if(extraInfo){
        this.avatarName = extraInfo.avatar || 'unknown';
      }
    });

    userSvc.onLoginStateChange.subscribe(isLoggedIn=>{
      if(!isLoggedIn){
        this.avatarName = "unknown";
        this.fullName   = "";
      }
      else{
        let extraInfo:any = this.userSvc.getExtraInfo();
        this.avatarName   = extraInfo.avatar || 'unknown';
        this.fullName     = extraInfo.nombre +" "+extraInfo.paterno;
      }
    });
  }

  ngAfterContentInit(){
    if(GLOBALS.DEBUG_MODE){
      Utils.showToast("APP en 'Modo Desarrollo'", {position:"top", duration:5*GLOBALS.ONE_SECOND});
    }
  }

  public openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    let loader = Utils.showLoading();
    this.nav.setRoot(page.component).then(()=>{
      loader.dismiss();
    });
    this.activePage = page.title;
  }
}
