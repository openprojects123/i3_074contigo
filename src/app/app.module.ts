import {
  CloudSettings,
  CloudModule
}                                 from '@ionic/cloud-angular';
import { NgModule }               from '@angular/core';
import {
  IonicApp,
  IonicModule,
  MenuController
}                                 from 'ionic-angular';
import { ErrorHandler }           from '@angular/core';
import { IonicErrorHandler }      from 'ionic-angular';
import { SplashScreen }           from '@ionic-native/splash-screen';
import { StatusBar }              from '@ionic-native/status-bar';
import { BrowserModule }          from '@angular/platform-browser';
import { HttpModule }             from "@angular/http";
import { IonicStorageModule }     from '@ionic/storage';

import { MyApp }                  from './app.component';



/*********************** PROVIDERS *********************/
import { TextToSpeech }           from '@ionic-native/text-to-speech'
import { Diagnostic }             from '@ionic-native/diagnostic';
import { Network }                from '@ionic-native/network';
import { Geolocation }            from '@ionic-native/geolocation';
import {BackgroundGeolocation}    from '@ionic-native/background-geolocation';
import { SQLite }                 from '@ionic-native/sqlite';
import { GeolocationService }     from "../providers/locationAndMap/geolocation-service";
import { LocalDbSvc }             from "../providers/localDb-service";
import { MapsGoogleService }      from "../providers/locationAndMap/mapsGoogle-service";
import { Map074Service }          from "../providers/locationAndMap/map074-service";
import { DiagnosticService }      from "../providers/diagnostic-service";
import { UserService }            from "../providers/user-service";
import { AppDataProvider }        from "../providers/app-data-provider";
import {Utils} from "../providers/utils-service";

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': '3acc03cf'
  }
};

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    CloudModule.forRoot(cloudSettings),
    BrowserModule,
    HttpModule,
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},

    TextToSpeech,
    Diagnostic,
    Network,
    Geolocation,
    SQLite,

    MapsGoogleService, GeolocationService, Map074Service,
    BackgroundGeolocation,
    LocalDbSvc,
    DiagnosticService,
    UserService,
    AppDataProvider,
    MenuController,
    Utils,
  ]
})
export class AppModule {
  constructor(u:Utils){}
}
