import { Injectable } from '@angular/core';

@Injectable()
export class GLOBALS{
  private static servers = [
    "http://eonproduccion.net/MobileApps/API/web/app_dev.php/",//<--Development
    "http://eonproduccion.net/MobileApps/API/web/app_dev.php/",//<--Production
  ];

  public static get API_URL():string {
    return this.servers[this._DEVMODE ? 0: 1] + "074Contigo/";
  };

  public static get API2_URL():string {
    return this.servers[this._DEVMODE ? 0: 1] + "074Obras/";
  };

  public static get DEBUG_MODE():boolean { return this._DEBUGMODE};

  public static get LOG_INFO():string {return "info"};
  public static get LOG_WARN():string {return "warn"};
  public static get LOG_ERROR():string {return "error"};
  

  public static get ONE_SECOND():number {return 1000};
  public static get ONE_MINUTE():number {return 60*this.ONE_SECOND};

  //---------No editar lo de ^arriba^ a menos que sepas lo que estas haciendo-------------
  private static _DEVMODE   = true;//true = server EON, false= server Zihuatanejo asi como habilitacion/muestra de botones etc
  private static _DEBUGMODE = false;//true = se escribe en consola y en alertas los detalles de errores, etc.
  public static get LOG_LEVEL():string { return this.LOG_WARN};//{info, warn, error}
}
