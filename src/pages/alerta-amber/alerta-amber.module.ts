import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {AlertaAmberPage} from "./alerta-amber";

@NgModule({
  declarations: [
    AlertaAmberPage,
  ],
  imports: [
    IonicPageModule.forChild(AlertaAmberPage),
  ],
  exports: [
    AlertaAmberPage
  ]
})
export class AlertaAmberPageModule {}
