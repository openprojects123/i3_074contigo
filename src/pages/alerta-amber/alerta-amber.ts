import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                     from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-alerta-amber',
  templateUrl: 'alerta-amber.html',
})
export class AlertaAmberPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AlertaAmber');
  }

}
