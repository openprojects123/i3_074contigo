import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {ComunicacionSocialPage} from "./comunicacion-social";

@NgModule({
  declarations: [
    ComunicacionSocialPage,
  ],
  imports: [
    IonicPageModule.forChild(ComunicacionSocialPage),
  ],
  exports: [
    ComunicacionSocialPage
  ]
})
export class ComunicacionSocialPageModule {}
