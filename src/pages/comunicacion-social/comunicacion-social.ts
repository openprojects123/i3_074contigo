import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                     from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-comunicacion-social',
  templateUrl: 'comunicacion-social.html',
})
export class ComunicacionSocialPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ComunicacionSocial');
  }

}
