import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                     from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-consejos',
  templateUrl: 'consejos.html'
})
export class ConsejosPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConsejosPage');
  }

}
