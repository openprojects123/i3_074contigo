import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {CulturaVialPage} from "./cultura-vial";

@NgModule({
  declarations: [
    CulturaVialPage,
  ],
  imports: [
    IonicPageModule.forChild(CulturaVialPage),
  ],
  exports: [
    CulturaVialPage
  ]
})
export class CulturaVialPageModule {}
