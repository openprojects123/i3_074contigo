import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                     from 'ionic-angular';

let TAG = "CulturaVialPage";

@IonicPage()
@Component({
  selector: 'page-cultura-vial',
  templateUrl: 'cultura-vial.html'
})
export class CulturaVialPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad '+TAG);
  }

  //todo: cambiar push por argumento recibido
  public goTo(page:string){
    if(page == "culturaVialSenales")
      this.navCtrl.push("SenalesTabsPage");
    else if(page == "culturaVialConsejos")
      this.navCtrl.push("ConsejosPage");
  }
}
