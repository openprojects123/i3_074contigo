import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {SenalesInformativasTabPage} from "./senales-informativas-tab";

@NgModule({
  declarations: [
    SenalesInformativasTabPage,
  ],
  imports: [
    IonicPageModule.forChild(SenalesInformativasTabPage),
  ],
  exports: [
    SenalesInformativasTabPage
  ]
})
export class SenalesInformativasTabPageModule {}
