import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {SenalesPreventivasTabPage} from "./senales-preventivas-tab";

@NgModule({
  declarations: [
    SenalesPreventivasTabPage,
  ],
  imports: [
    IonicPageModule.forChild(SenalesPreventivasTabPage),
  ],
  exports: [
    SenalesPreventivasTabPage
  ]
})
export class SenalesPreventivasTabPageModule {}
