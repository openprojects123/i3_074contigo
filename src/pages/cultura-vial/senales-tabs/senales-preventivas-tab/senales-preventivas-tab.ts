import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                     from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-senales-preventivas',
  templateUrl: 'senales-preventivas-tab.html'
})
export class SenalesPreventivasTabPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SenalesPreventivasPage');
  }

}
