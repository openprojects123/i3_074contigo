import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {SenalesRestrictivasTabPage} from "./senales-restrictivas-tab";

@NgModule({
  declarations: [
    SenalesRestrictivasTabPage,
  ],
  imports: [
    IonicPageModule.forChild(SenalesRestrictivasTabPage),
  ],
  exports: [
    SenalesRestrictivasTabPage
  ]
})
export class SenalesRestrictivasTabModule {}
