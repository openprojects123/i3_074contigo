import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {SenalesTabsPage} from "./senales-tabs";

@NgModule({
  declarations: [
    SenalesTabsPage,
  ],
  imports: [
    IonicPageModule.forChild(SenalesTabsPage),
  ],
  exports: [
    SenalesTabsPage
  ]
})
export class SenalesPageModule {}
