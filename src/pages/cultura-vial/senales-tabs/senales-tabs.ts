import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                     from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-senales',
  templateUrl: 'senales-tabs.html'
})
export class SenalesTabsPage {

  public tabsCulturaVial = [
    {component: "SenalesPreventivasTabPage",  title:'Preventivas',  icon:'warning'},
    {component: "SenalesInformativasTabPage", title:'Informativas', icon:'ios-information-circle'},
    {component: "SenalesRestrictivasTabPage", title:'Restrictivas', icon:'hand'},
  ];

  public currTabTitle: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.currTabTitle = this.tabsCulturaVial[0].title;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SenalesPage');
  }

  public onTabClicked(tabTitle:string){
    this.currTabTitle = tabTitle;
  }

}
