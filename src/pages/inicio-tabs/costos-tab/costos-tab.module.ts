import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {CostosTabPage} from "./costos-tab";

@NgModule({
  declarations: [
    CostosTabPage,
  ],
  imports: [
    IonicPageModule.forChild(CostosTabPage),
  ],
  exports: [
    CostosTabPage
  ]
})
export class CostosTabPageModule {}
