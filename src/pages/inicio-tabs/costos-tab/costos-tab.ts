import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                     from 'ionic-angular';
import {Utils}        from "../../../providers/utils-service";

let TAG = "CostosTabPage";

@IonicPage()
@Component({
  selector: 'page-costos',
  templateUrl: 'costos-tab.html',
})
export class CostosTabPage {

  public tramos = 1;
  public vehTipo = 1;
  public ok:boolean = true;
  public tipo1:boolean = false;
  public tipo2:boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  onChange() {
    this.tipo1 = true;
    Utils.LOG.i(TAG, "this.tramos => "+this.tramos, "onChange()");

    if(this.tipo1 && this.tipo2){
      this.ok = false;
    }

  }

  onChange1() {
    this.tipo2 = true;
    Utils.LOG.i(TAG, "this.vehTipo => "+this.vehTipo, "onChange1()");

    if(this.tipo1 && this.tipo2){
      this.ok = false;
    }

  }

}
