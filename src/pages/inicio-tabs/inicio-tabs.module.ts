import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {InicioTabsPage} from "./inicio-tabs";

@NgModule({
  declarations: [
    InicioTabsPage,
  ],
  imports: [
    IonicPageModule.forChild(InicioTabsPage),
  ],
  exports: [
    InicioTabsPage
  ]
})
export class InicioTabsPageModule {}
