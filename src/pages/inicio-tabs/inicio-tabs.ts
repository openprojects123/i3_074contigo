import {
  Component,
  ViewChild
} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  Tabs
} from 'ionic-angular';
import {
  LocalDbSvc,
  tramoType
}                       from "../../providers/localDb-service";
import {Utils}          from "../../providers/utils-service";
import { Events }       from 'ionic-angular';
import {AppDataProvider} from "../../providers/app-data-provider";

let TAG = "InicioTabsPage";

@IonicPage()
@Component({
  selector: 'page-inicio',
  templateUrl: 'inicio-tabs.html'
})
export class InicioTabsPage {

  public tabsInicio = [
    {component: "TramosTabPage",    title:'Tramos',       icon:'git-merge'},
    {component: "ListaTabPage",     title:'Lista',        icon:'list'},
    {component: "UbicacionTabPage", title:'Mi ubicación', icon:'pin'},
    {component: "MapaTabPage",      title:'Mapa',         icon:'map'},
    {component: "CostosTabPage",    title:'Costos',       icon:'logo-usd'},
  ];

  @ViewChild('inicioPageTabs') tabsRef: Tabs;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private DB: LocalDbSvc,
              private events: Events,
              private appSvc: AppDataProvider) {


    if(Utils.runsOnDevice()){
      Utils.platform.ready().then(()=>{
        DB.getSelectedTramos().subscribe((tramosArray:Array<tramoType>)=>{
          this.tabsRef.getByIndex(1).show = (tramosArray.length>0);
        }, reason=>{
          this.tabsRef.getByIndex(1).show = false;
          Utils.LOG.w(TAG, "No se pudo obtener el catálogo de tramos ya que"+reason, "constructor");
        });
      })
    }
    else{
      this.appSvc.getTramosFromStorage().subscribe(catTramos=>{
        this.tabsRef.getByIndex(1).show = (catTramos && catTramos.numSelected>0);
      })
    }


    this.events.unsubscribe('showTramosTab');
    events.subscribe('showTramosTab', (show) => {
      this.tabsRef.getByIndex(1).show = show;
    });
  }

  ionViewDidLoad(){
    console.log("InicioTabsPage loaded");
    setTimeout(()=>{
      this.tabsRef.select(0);
      this.tabsRef.getByIndex(0).show = true;
    }, 200)
  }
}
