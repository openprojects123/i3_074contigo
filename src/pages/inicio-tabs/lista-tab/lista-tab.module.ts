import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {ListaTabPage} from "./lista-tab";

@NgModule({
  declarations: [
    ListaTabPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaTabPage),
  ],
  exports: [
    ListaTabPage
  ]
})
export class ListaTabPageModule {}
