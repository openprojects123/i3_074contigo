import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                     from 'ionic-angular';
import {
  LocalDbSvc,
  obraType,
  tramoType,
  incidenciaType,
  latlngType
}                             from "../../../providers/localDb-service";
import {Utils}                from "../../../providers/utils-service";
import {Map074Service}        from "../../../providers/locationAndMap/map074-service";
import {roadDistDurationType} from "../../../providers/locationAndMap/mapsGoogle-service";

let TAG = "ListaTabPage";

@IonicPage()
@Component({
  selector: 'tab-lista',
  templateUrl: 'lista-tab.html'
})
export class ListaTabPage {
  public progressBarValue = 0;
  private hasEnterBefore = false;
  public tramosList:Array<any> = [];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private DB: LocalDbSvc,
              private map074svc: Map074Service) {}

  ionViewDidLoad(){
    //console.log('ionViewDidLoad '+TAG);
  }

  ionViewDidEnter() {
    Utils.LOG.i(TAG, 'ionViewDidEnter',  'ionViewDidEnter');
    if(!this.hasEnterBefore || this.DB.hasModelChanged()){
      this.hasEnterBefore   = true;
      this.progressBarValue = 0;

      if(this.map074svc.maps.isSdkLoaded())
        this.createList();
      else{
        let loader = Utils.showLoading();
        this.map074svc.maps.loadSDK().subscribe((alwaysTrue)=>{
          loader.dismiss();
          this.createList();
        },reason=>{
          loader.dismiss();
          this.createList();
        })
      }
    }
  }

  private createList(calcDistLocally?:boolean){
    this.tramosList = [];
    if(Utils.runsOnDevice()){
      let loader = Utils.showLoading();
      this.DB.getSelectedTramos().subscribe((tramosArray:Array<tramoType>)=>{
        let tramosArrayLength = tramosArray.length;

        if(!tramosArrayLength)
          loader.dismiss();

        let errores = "";
        tramosArray.forEach((tramo)=>{
          this.tramosList.push({
            id:           tramo.id,
            nombre:       tramo.nombre,
            longitudText: "Desconocida",
            duracionText: "Desconocido",
            incidentes:   [],
            obras:        [],
            POI:          []
          });
        });

        let totalQueriesResolved = 0;
        let magnitud = 0;
        let recursiveFunction = ()=>{
          this.progressBarValue = (totalQueriesResolved*100)/tramosArrayLength;

          //Utils.LOG.i(TAG, "Query: "+(totalQueriesResolved+1), "createList...");
          //para que cada N querie se detenga por un rato (delay) para evitar un error de OVER_QUERY_LIMIT
          if((totalQueriesResolved+1)%11 === 0){
            Utils.LOG.i(TAG, "Aplicando delay...", "createList...");
            magnitud++;
          }

          this.DB.getStartEndOfTramo(this.tramosList[totalQueriesResolved].id).subscribe((minMaxCoords:[latlngType])=>{
            if(minMaxCoords.length != 2){
              //if(magnitud>0)
                //magnitud=0;

              totalQueriesResolved++;
              if(totalQueriesResolved == tramosArrayLength)
                getIncidenciasAndObras();
              else
                recursiveFunction();
            }
            else{
              //Pongo un delay para evitar un error de OVER_QUERY_LIMIT
              setTimeout(()=>{
                this.map074svc.maps.
                calcRoadDistanceAndDuration(minMaxCoords[0], minMaxCoords[1]).subscribe((resp:roadDistDurationType)=>{
                  if(magnitud>0)
                    magnitud=0;

                  if(resp.status == true){
                    this.tramosList[totalQueriesResolved].longitudText = resp.distance.text;
                    this.tramosList[totalQueriesResolved].duracionText = resp.duration.text;
                    totalQueriesResolved++;

                    if(totalQueriesResolved == tramosArrayLength)
                      getIncidenciasAndObras();
                    else
                      recursiveFunction();
                  }
                  else{
                    Utils.LOG.w(TAG, resp.statusText, "createList->...->calcRoadDistanceAndDuration()");
                    calcRoadDistanceAndDurationLocally();
                  }
                }, reason=>{
                  if(magnitud>0)
                    magnitud=0;

                  Utils.LOG.w(TAG, reason, "createList->...->calcRoadDistanceAndDuration()");
                  calcRoadDistanceAndDurationLocally();
                });
              }, magnitud*2000);
            }
          }, reason=>{
            totalQueriesResolved++;
            Utils.LOG.w(TAG, reason, "createList->...->getStartEndOfTramo()");
            errores += "<br>Tramo: "+this.tramosList[totalQueriesResolved].nombre+"; "+reason;
            if(totalQueriesResolved == tramosArrayLength)
              getIncidenciasAndObras();
            else
              recursiveFunction();
          });
        };

        let calcRoadDistanceAndDurationLocally = ()=>{
          //Si no se pudo calcular la distancia mediante el servicio de google
          //La calculo localmente...
          this.DB.getTotalKmsOfTramo(this.tramosList[totalQueriesResolved].id).subscribe((totKms)=>{
            let estTime = totKms / 90;//longitud entre la velocidad promedio

            let hrs  = Math.floor(estTime);
            let mins = Math.ceil((estTime - hrs)*60);

            this.tramosList[totalQueriesResolved].longitudText = totKms+" km (*)";
            this.tramosList[totalQueriesResolved].duracionText = (hrs>0? hrs+" h ":'')+(mins>0?mins+' min ':'')+"(*)";

            totalQueriesResolved++;
            if(totalQueriesResolved == tramosArrayLength)
              getIncidenciasAndObras();
            else
              recursiveFunction();
          }, reason=>{
            totalQueriesResolved++;
            Utils.LOG.i(TAG, "totalQueriesResolved: "+totalQueriesResolved, "calcRoadDistanceAndDurationLocally()");
            Utils.LOG.w(TAG, reason, "calcRoadDistanceAndDurationLocally()");
            if(totalQueriesResolved == tramosArrayLength)
              getIncidenciasAndObras();
            else
              recursiveFunction();
          })
        };

        let totalQueries = 0;
        let getIncidenciasAndObras = ()=> {
          this.progressBarValue = (totalQueriesResolved*100)/tramosArrayLength;

          if(errores.length){
            Utils.showAlert("No se pudo obtener la longitud de uno o mas tramos:"+errores);
            if(!this.tramosList.length)
              loader.dismiss();
          }

          if(!this.map074svc.mustShowIncidencias() && !this.map074svc.mustShowObras()){
            loader.dismiss();
            return;
          }

          errores = "";
          this.tramosList.forEach((tramo, idxTr) => {
            if(this.map074svc.mustShowIncidencias()){
              this.DB.getIncidenciasByIdAutopista(tramo.id).subscribe((incidenciasArray: [incidenciaType]) => {
                this.tramosList[idxTr].incidentes = incidenciasArray;
                totalQueries++;
                if(totalQueries == this.tramosList.length*2)//Porque necesito incidencias y obras
                  finishGetIncidenciasAndObras();
              }, (reason) => {
                errores += "<br>Incidencias en Autopista: " + tramo.nombre + "; " + reason;
                totalQueries++;
                if(totalQueries == this.tramosList.length*2)//Porque necesito incidencias y obras
                  finishGetIncidenciasAndObras();
              });
            }
            else
              totalQueries++;

            if(this.map074svc.mustShowObras()){
              this.DB.getObrasOnTramos(tramo.id).subscribe((obras: [obraType]) => {
                //Utils.LOG.i(TAG, JSON.stringify(obras), "createList->DB.getSelectedTramos->DB.getObras()");
                if (obras.length)
                  this.tramosList[idxTr].obras = obras;

                totalQueries++;
                if(totalQueries == this.tramosList.length*2)//Porque necesito incidencias y obras
                  finishGetIncidenciasAndObras();
              }, reason => {
                errores += "<br>Obras en Autopista: " + tramo.nombre + "; " + reason;
                totalQueries++;
                if(totalQueries == this.tramosList.length*2)//Porque necesito incidencias y obras
                  finishGetIncidenciasAndObras();
              });
            }
            else
              totalQueries++;
          });
        };

        let finishGetIncidenciasAndObras = ()=>{
          loader.dismiss();
          if (errores.length) {
            Utils.showAlert("No se pudieron obtener los incidentes/obras de uno o mas tramos:" + errores);
          }
        };

        //Aqui comienzo el proceso :)
        recursiveFunction();
      }, reason=>{
        loader.dismiss();
        Utils.showAlert("No se pudieron obtener los tramos ya que"+reason);
      });
    }
  }
}
