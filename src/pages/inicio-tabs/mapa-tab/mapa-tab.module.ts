import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {MapaTabPage} from "./mapa-tab";

@NgModule({
  declarations: [
    MapaTabPage,
  ],
  imports: [
    IonicPageModule.forChild(MapaTabPage),
  ],
  exports: [
    MapaTabPage
  ]
})
export class MapaTabPageModule {}
