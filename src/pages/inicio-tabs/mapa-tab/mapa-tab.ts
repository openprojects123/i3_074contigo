import {
  Component,
  ViewChild
} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  FabContainer, MenuController
} from 'ionic-angular';
import {Observable} from "rxjs";
import {Utils}      from "../../../providers/utils-service";
import {
  LocalDbSvc,
  obraType,
  latlngType,
  incidenciaType,
  tramoType,
  neighborType,
  POIType,
  coordsPOIType
} from "../../../providers/localDb-service";
import {Map074Service}  from "../../../providers/locationAndMap/map074-service";
import {GLOBALS}        from "../../../app/globals";
import {
  GeolocationService,
  GeolocationServiceResponse
}                       from "../../../providers/locationAndMap/geolocation-service";
import {AppDataProvider} from "../../../providers/app-data-provider";
import { DiagnosticService }  from '../../../providers/diagnostic-service';


let TAG = "MapaTabPage";
let lastLatLng: latlngType;

@IonicPage()
@Component({
  selector: 'tab-mapa',
  templateUrl: 'mapa-tab.html'
})
export class MapaTabPage {
  @ViewChild('fabTopRight') fabTopRight: FabContainer;

  public showMapLayer = false;
  public cantInitMap = false;
  public isOnLine:boolean;
  public isOnTramo  = false;

  public mapStyles:Array<any>;
  public mapStyle:{id, value, title, strokeColor, strokeOpacity};
  public mapStyleId:number;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private menuCtrl: MenuController,
              private map074Svc: Map074Service,
              private DB:LocalDbSvc,
              private appSvc: AppDataProvider,
              private locationSvc: GeolocationService,
              public  diagnosticSvc:  DiagnosticService) {

    this.mapStyles = map074Svc.getMapStylesList();

    this.mapStyleId = this.map074Svc.getSelectedMapStyle().id;
  }

  /*ngOnInit() {
   Utils.LOG.i(TAG, "ngOnInit()!!!", "ngOnInit()");
   }

   ionViewDidLoad(){
   Utils.LOG.i(TAG, "ionViewDidLoad", "ionViewDidLoad");
   }*/

  ionViewDidLoad(){
    Utils.LOG.i(TAG, "ionViewDidLoad", "ionViewDidLoad()");

    this.prepareForInitMap();
    /*this.diagnosticSvc.networkConnectionOnChange.subscribe((state)=>{
      console.info("XD");
      this.isOnLine = state
      this.showMapLayer = false;

      if(this.isOnLine)

    });*/
  }

  ionViewDidEnter(){
    if(this.showMapLayer)
      this.map074Svc.maps.reloadMap();

    this.menuCtrl.swipeEnable(false, 'menu1');
  }

  ionViewWillLeave(){
    this.closeFabTopRight();
    this.menuCtrl.swipeEnable(true, 'menu1');
  }

  private prepareForInitMap(){
    this.mapStyle = this.mapStyles[0];

    //let loader = Utils.showLoading("Inicializando mapa");
    if(lastLatLng){
      this.inicializarMapa(lastLatLng).subscribe(() => {
        //loader.dismiss();
        this.startMainProcess();
      }, reason=>{
        //loader.dismiss();
        Utils.showAlert("No se puede mostrar el mapa ya que"+reason);
      });
    }
    else{
      this.locationSvc.getCurrentPosition().subscribe((location:GeolocationServiceResponse) => {
        lastLatLng = {lat: location.coords.latitude, lng: location.coords.longitude};

        this.inicializarMapa(lastLatLng).subscribe(() => {
          //loader.dismiss();
          this.startMainProcess();
        }, reason=>{
          //loader.dismiss();
          Utils.showAlert("No se puede mostrar el mapa ya que"+reason);
        });
      }, reason=>{
        //loader.dismiss();
        Utils.showAlert("No se pudo obtener la ubicación actual ya que"+reason);
        this.cantInitMap = true;
      })
    }
  }

  private inicializarMapa(center:latlngType):Observable<boolean>{
    return Observable.create(observer=>{
      this.map074Svc.maps.setShowTrafficLayer(this.map074Svc.mustShowTrafico());
      this.map074Svc.maps.setZoom(5);
      let container:any = document.getElementById("map");

      this.map074Svc.maps.loadSDK().subscribe((alwaysTrue)=>{
        this.map074Svc.maps.create(container, center).subscribe((map)=>{
          this.showMapLayer = true;

          /*this.map074Svc.maps.setClickMapListener((position:latlngType)=>{
            Utils.LOG.i(TAG, "Hiciste clic en: "+JSON.stringify(position), "inicializarMapa->setClickMapListener()")
          });

          this.map074Svc.maps.setCenterChangedListener((newCenter:latlngType)=>{
            Utils.LOG.i(TAG, "Nuevo centro: "+JSON.stringify(newCenter), "inicializarMapa->setCenterChangedListener()")
          });*/

          this.mapStyle = this.map074Svc.getSelectedMapStyle().id;
          setTimeout(()=>{
            this.map074Svc.setMapStyle(this.map074Svc.getSelectedMapStyle()).subscribe();
            observer.next(true);
          }, 100);

          //let mapContainer:any = document.querySelector("#mapStyle-container");
          //mapContainer.style.display = "block";
        }, reason=>{
          observer.error(Utils.LOG.e(TAG,reason, "inicializarMapa...->create()"))
        });
      }, reason=>{
        observer.error(Utils.LOG.e(TAG,reason, "inicializarMapa...->loadSDK()"))
      })
    });
  }

  private startMainProcess(){
    if(!this.map074Svc.getTotalTramosSelected())return;

    if(!Utils.runsOnDevice()){
      if(this.map074Svc.mustShowIncidencias()){
        let loader = Utils.showLoading("Colocando incidencias");
        this.appSvc.getIncidenciasFromServer().subscribe((incidenciasArray:Array<incidenciaType>)=>{
          this.map074Svc.drawIncidencias(incidenciasArray);
          loader.dismiss();
        }, reason=>{
          loader.dismiss();
          Utils.showAlert("No se pudieron obtener las incidencias ya que"+reason)
        });
      }

      let loader1 = Utils.showLoading("Leyendo tramos");
      this.appSvc.getTramosFromStorage().subscribe((catTramosStrg:{tramos:[tramoType], numSelected:number})=>{
        loader1.dismiss();
        if(!catTramosStrg){return;}

        let idsTramos:Array<number> = [];
        let selectedTramos:Array<number> = [];
        catTramosStrg.tramos.forEach((tramo)=>{
          idsTramos.push(tramo.id);
          if(tramo.selected)
            selectedTramos.push(tramo.id);
        });

        let loader2 = Utils.showLoading("Mostrando tramos");
        this.map074Svc.drawSelectedTramos2(selectedTramos).subscribe(total=>{
          loader2.dismiss();
          Utils.LOG.i(TAG, "Total tramos pintados: "+total, "showSelectedTramos->getSelectedTramos->drawSelectedTramos()");
          this.drawObras(idsTramos);
        }, reason=>{
          setTimeout(()=>{
            loader2.dismiss();
          }, 300);
          Utils.showAlert("No se pudieron mostrar los tramos ya que"+reason);
        });
      }, reason=>{
        Utils.showAlert("No se pudieron obtener los tramos de localStorage ya que"+reason);
      });
      return;
    }

    if(Utils.runsOnDevice()){
      let loader1 = Utils.showLoading("Leyendo tramos");
      this.DB.getSelectedTramos().subscribe((tramosArray:Array<tramoType>)=>{
        loader1.dismiss();

        if(!tramosArray.length)return;
        //Utils.LOG.i(TAG, "Se obtuvieron "+tramosArray.length+" tramos", "startMainProcess->getSelectedTramos()");

        let idsTramos:Array<number> = [];
        tramosArray.forEach((tramo)=>{
          idsTramos.push(tramo.id);
        });

        if(!idsTramos.length) return;

        let loader2 = Utils.showLoading("Mostrando tramos");
        this.map074Svc.drawSelectedTramos(idsTramos).subscribe(total=>{
          loader2.dismiss();
          Utils.LOG.i(TAG, "Total tramos pintados: "+total, "showSelectedTramos->getSelectedTramos->drawSelectedTramos()");
          this.drawIncidencias(idsTramos);
          this.drawObras(idsTramos);
          this.drawPOI(idsTramos);
        }, reason=>{
          loader2.dismiss();
          Utils.showAlert("No se pudieron mostrar los tramos ya que"+reason);
        });
      }, reason=>{
        Utils.showAlert("No se pudieron obtener los tramos seleccionados ya que"+reason);
      });
    }
  }

  public changeMapStyle(id){
    this.closeFabTopRight();

    this.map074Svc.setMapStyle(this.mapStyles[id]).subscribe(resp_bool=>{
      if(resp_bool){
        this.mapStyleId = this.map074Svc.getSelectedMapStyle().id;
        this.map074Svc.updateTramosColor();//Actualizo con el mismo estilo de mapa
      }
    });
  }

  public closeFabTopRight(){
    if(this.showMapLayer)
      this.fabTopRight.close();
  }

  private drawIncidencias(idsTramos:Array<number>){
    if(this.map074Svc.mustShowIncidencias()) {

      let loader = Utils.showLoading("Buscando incidentes...");
      this.DB.getIncidenciasOnTramos(idsTramos).subscribe((incidenciasArray:Array<incidenciaType>)=>{
        this.map074Svc.drawIncidencias(incidenciasArray);
        loader.dismiss();

        if (incidenciasArray.length) {
          this.showClosestNeighbor(lastLatLng).subscribe((neighbor:neighborType)=>{
            this.speechClosestIncidencia(incidenciasArray);
          });
        }
      }, reason=>{
        loader.dismiss();
        Utils.showAlert("No se pudieron obtener las incidencias desde la BD local ya que"+reason);
      });
    }
    else{
      this.map074Svc.clearIncidencias();
    }
  }

  private drawObras(idsTramos:Array<number>){
    if(this.map074Svc.mustShowObras()){

      let ids = "";
      for(let i=0; i<idsTramos.length; i++){
        ids += idsTramos[i] + (i< idsTramos.length-1 ? ',':'');
      }

      let loader = Utils.showLoading("Obteniendo obras");
      this.appSvc.getObrasFromServer(ids).subscribe((obrasArray:[obraType])=>{
        loader.dismiss();

        let totalObras               = obrasArray.length;
        let totalObrasDrawed         = 0;
        let totalDrawQueriesResolved = 0;

        if(!obrasArray.length){return}

        let obrasCoordsArrays:Array<Array<latlngType>> = [];

        let loader2 = Utils.showLoading("Mostrando obras");
        obrasArray.forEach((obra, idx)=>{
          this.appSvc.getKilometrosObras(obra).subscribe((kmArray:Array<latlngType>)=>{
            obrasCoordsArrays.push(kmArray);

            this.map074Svc.drawSelectedObras(obrasCoordsArrays).subscribe(totalDrawed=>{
              totalDrawQueriesResolved++;
              totalObrasDrawed++;
              //Si ya se dibujaron todas las obras regreso el total.
              if(totalDrawQueriesResolved == totalObras){
                loader2.dismiss();
                Utils.LOG.i(TAG, "Total obras colocadas: "+totalObrasDrawed, "drawObras->...->drawSelectedObras");
              }
            }, reason=>{
              totalDrawQueriesResolved++;
              if(totalDrawQueriesResolved == totalObras){
                loader2.dismiss();
                Utils.LOG.i(TAG, "Total obras colocadas: "+totalObrasDrawed, "drawObras->...->drawSelectedObras");
              }
              Utils.LOG.e(TAG, "No se pudo dibujar la obra ya que: "+reason, "showObrasOnTramos->drawSelectedObras()");
            });
          }, reason=>{
            if(idx == totalObras-1){
              loader2.dismiss();
              Utils.LOG.i(TAG, "Total obras colocadas: "+totalObrasDrawed, "drawObras->...->getKilometrosObras");
            }

            Utils.LOG.e(TAG, "No se pudieron obtener los kilometros de la obra con id ("+obra.idObras+") ya que"+reason, "showObrasOnTramos->getKilometrosObras()");
          });
        })
      }, reason=>{
        loader.dismiss();
        Utils.LOG.e(TAG, reason, "drawObras->getObrasFromServer()");
      })
    }
    else{
      this.map074Svc.clearObras();
    }
  }

  private drawPOI(idsTramos:Array<number>){
    if(this.map074Svc.getTotalPOISelected()<1) return;
    Utils.LOG.i(TAG, "getTotalPOISelected: "+this.map074Svc.getTotalPOISelected(), "drawPOI()");

    this.DB.getSelectedPOI().subscribe((poiArray:[POIType])=>{
      let poiIds:Array<number> = [];
      let totalPOI = poiArray.length;
      for(let i=0; i<totalPOI; i++){
        poiIds.push(poiArray[i].id);// + (i< totalPOI-1 ? ',':'');
      }

      this.DB.getPOICoordsInTramos(idsTramos, poiIds).subscribe((coordsPOI:[coordsPOIType])=>{
        this.map074Svc.drawSelectedPOI(coordsPOI).subscribe(total=>{
          Utils.LOG.i(TAG, "Total Puntos de interés colocados: "+total, "drawPOI()");
        }, reason=>{
          Utils.LOG.w(TAG, "No se pudieron colocar los Puntos de interés seleccionados ya que"+reason, "showPOIOnTramos->...->DB.drawSelectedPOI()");
        });
      }, reason=>{
        Utils.LOG.w(TAG, "No se pudieron obtener las coordenadas de los Puntos de interés seleccionados ya que"+reason, "showPOIOnTramos->DB.getPOICoordsInTramos()");
      });
    }, reason=>{
      Utils.LOG.w(TAG, "No se pudieron obtener los Puntos de interés seleccionados ya que"+reason, "showPOIOnTramos->DB.getSelectedPOI()");
    })
  }

  private showClosestNeighbor(latlng: latlngType):Observable<neighborType>{
    return Observable.create(observer=>{
      //this.locationWebSvc.getLocationOnce().subscribe(latlng=>{
      this.map074Svc.getNearestTramo(latlng, this.map074Svc.distFromCenter).subscribe((neighbor:neighborType)=>{
        if(neighbor.tramo){
          this.showClosestNeighborInfo(neighbor);
          this.isOnTramo = true;
        }
        else{
          this.appSvc.speech("Usted se encuentra fuera de la red CAPUFE.");
        }

        return observer.next(neighbor);
      });

      if(GLOBALS.DEBUG_MODE)
        this.map074Svc.drawROI(latlng, this.map074Svc.distFromCenter);
      /*}, reason=>{
       Utils.showAlert("No se pudo obtener su ubicación ya que"+reason)
       })*/
    });
  }

  private showClosestNeighborInfo(neighbor: neighborType){
    let unidades;
    if(neighbor.distancia>=1000){
      neighbor.distancia = this.truncateNumber(neighbor.distancia/1000, 1);
      unidades = "Kilómetros";
    }else{unidades = "Metros";}

    if(neighbor.tramo && neighbor.tramo.id != null){
      this.DB.getTramoById(neighbor.tramo.idAutopista).subscribe((tramo:tramoType)=>{
        if(!tramo){
          Utils.showAlert("No se pudo determinar la información del tramo CAPUFE");
        }
        else{
          Utils.showAlert("Usted se encuentra sobre el tramo "+tramo.nombre+
            " a "+ neighbor.distancia+" "+ unidades +" del Km. "+neighbor.tramo.km/*/1000*/+".");

          this.map074Svc.drawArrow(
            lastLatLng, {lat:neighbor.tramo.lat, lng: neighbor.tramo.lng}
          );
        }
      }, (reason)=>{
        Utils.showAlert("No se pudo determinar si se encuentra dentro de la red CAPUFE debido a: "+reason);
      });
    }
    else{
      Utils.showAlert("el ID del tramo es nulo"+JSON.stringify(neighbor));
    }
  }

  private speechClosestIncidencia(incidenciasArray:Array<incidenciaType>){
    if(!incidenciasArray.length){
      Utils.LOG.i(TAG, "No hay incidencias de que hablar!", "speechClosestIncidencia()");
      return;
    }


    let msg = "Hay " + (incidenciasArray.length === 1 ? "un incidente." : incidenciasArray.length + ' incidentes.');

    let incidencia:{id: number, distancia: number} = this.map074Svc.getClosestIncidencia(lastLatLng, incidenciasArray);

    let unidades;
    if(incidencia.distancia>=1000){
      incidencia.distancia = this.truncateNumber(incidencia.distancia/1000, 1);
      unidades = "Kilómetros";
    }else{unidades = "Metros";}

    this.appSvc.speech(msg).subscribe(()=>{
      if(incidenciasArray.length){
        //Hago una pausa... antes de la sig frase.
        setTimeout(()=>{
          let msg2 = "El incidente más cercano se encuentra a "+incidencia.distancia+" "+unidades;
          this.appSvc.speech(msg2).subscribe(
            ()=>{},
            ()=>{Utils.showAlert(msg2);}
          );
        }, 1500);
      }
    }, ()=>{
      Utils.showAlert(msg);
    });
  }

  public locateMe(){
    this.closeFabTopRight();

    let loader = Utils.showLoading();
    this.locationSvc.getCurrentPosition().subscribe((location:GeolocationServiceResponse)=>{
      let pos = {lat: location.coords.latitude, lng: location.coords.longitude};
      this.map074Svc.maps.setMapCenter(pos);
      this.map074Svc.maps.placeMarker(this.map074Svc.maps.createMarker(pos, {}));
      loader.dismiss();
    }, (reason)=>{
      Utils.showAlert("No se pudo obtener su ubicación ya que"+reason);
      loader.dismiss();
    })
  }

  public sos(){}

  private truncateNumber(num, decimals){
    return ((num*=Math.pow(10,decimals))-(num%1))/Math.pow(10,decimals);
  }

}
