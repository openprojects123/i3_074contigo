import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {ModalPoiPage} from "./modal-poi";

@NgModule({
  declarations: [
    ModalPoiPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalPoiPage),
  ],
  exports: [
    ModalPoiPage
  ]
})
export class ModalPoiPageModule {}
