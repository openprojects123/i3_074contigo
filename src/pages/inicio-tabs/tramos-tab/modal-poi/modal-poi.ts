import { Component }  from '@angular/core';
import {
  IonicPage,
  ViewController,
  NavParams
}                     from 'ionic-angular';
import {catPOIType}   from "../../../../providers/localDb-service";
import {Utils} from "../../../../providers/utils-service";


let TAG = "ModalPoiPage";

@IonicPage()
@Component({
  selector: 'page-modal-poi',
  templateUrl: 'modal-poi.html'
})
export class ModalPoiPage {

  public catPOI:catPOIType;
  public numSelectedPOI = 0;
  public totalPOI       = 0;

  constructor(public viewCtrl: ViewController,
              public navParams: NavParams) {
    let catPOI:catPOIType = navParams.get("data");
    //Utils.LOG.i(TAG, "---- catPOI: "+JSON.stringify(catPOI), "constructor()");
    this.catPOI         = catPOI;
    this.totalPOI       = catPOI.poi.length;
    this.numSelectedPOI = catPOI.numSelected;
  }

  public closeModal(){
    let loader = Utils.showLoading();
    setTimeout(()=>{
      this.viewCtrl.dismiss(this.catPOI).then(()=>{
        loader.dismiss();
      });
    }, 800);
  }

  public togglePOI(listIdx){
    //console.log(listIdx, tableIdx);
    this.numSelectedPOI += this.catPOI.poi[listIdx].selected ? -1: 1;
    this.catPOI.poi[listIdx].selected = !this.catPOI.poi[listIdx].selected;
    this.catPOI.numSelected = this.numSelectedPOI;
  }

  public selectAllPOI(){
    this.catPOI.poi.forEach((poi)=>{
      poi.selected = true;
    });
    this.numSelectedPOI     = this.totalPOI;
    this.catPOI.numSelected = this.totalPOI;
  }

  public unselectAllPOI(){
    this.catPOI.poi.forEach((poi)=>{
      poi.selected = false;
    });
    this.numSelectedPOI     = 0;
    this.catPOI.numSelected = 0;
  }

}
