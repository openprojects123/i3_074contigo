import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {ModalTramosPage} from "./modal-tramos";

@NgModule({
  declarations: [
    ModalTramosPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalTramosPage),
  ],
  exports: [
    ModalTramosPage
  ]
})
export class ModalTramosPageModule {}
