import { Component }    from '@angular/core';
import {
  IonicPage,
  ViewController,
  NavParams
}                       from 'ionic-angular';
import {catTramosType}  from "../../../../providers/localDb-service";
import {Utils} from "../../../../providers/utils-service";


let TAG = "ModalTramosPage";

@IonicPage()
@Component({
  selector: 'page-modal-tramos',
  templateUrl: 'modal-tramos.html'
})
export class ModalTramosPage {

  public catTramos:catTramosType;
  public numSelectedTramos = 0;
  public totalTramos       = 0;

  constructor(public viewCtrl: ViewController,
              public navParams: NavParams) {
    let catTramos = navParams.get("data");

    this.catTramos         = catTramos;
    this.totalTramos       = catTramos.tramos.length;
    this.numSelectedTramos = catTramos.numSelected;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad '+TAG);
  }

  /*public onSubmit(){
   setTimeout(()=>{
   this.viewCtrl.dismiss(this.data);
   }, 800);
   }*/

  public closeModal(){
    let loader = Utils.showLoading();
    setTimeout(()=>{
      this.viewCtrl.dismiss(this.catTramos).then(()=>{
        loader.dismiss();
      });
    }, 800);
  }

  public toggleTramo(listIdx){
    //console.log(listIdx, tableIdx);
    this.numSelectedTramos += this.catTramos.tramos[listIdx].selected ? -1: 1;
    this.catTramos.tramos[listIdx].selected = !this.catTramos.tramos[listIdx].selected;
    this.catTramos.numSelected = this.numSelectedTramos;
  }

  public selectAllTramos(){
    this.catTramos.tramos.forEach((tramo)=>{
      tramo.selected = true;
    });
    this.numSelectedTramos = this.totalTramos;
    this.catTramos.numSelected = this.totalTramos;
  }

  public unselectAllTramos(){
    this.catTramos.tramos.forEach((tramo)=>{
      tramo.selected = false;
    });
    this.numSelectedTramos = 0;
    this.catTramos.numSelected = 0;
  }

}
