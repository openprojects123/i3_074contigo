import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {TramosTabPage} from "./tramos-tab";

@NgModule({
  declarations: [
    TramosTabPage,
  ],
  imports: [
    IonicPageModule.forChild(TramosTabPage),
  ],
  exports: [
    TramosTabPage
  ]
})
export class TramosTabPageModule {}
