import {
  HostListener,
  Component
 }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
}                           from 'ionic-angular';
import { ModalController }  from 'ionic-angular';
import {
  LocalDbSvc,
  obraType,
  catTramosType,
  tramoType,
  incidenciaType,
  catPOIType
} from "../../../providers/localDb-service";
import {Utils} from "../../../providers/utils-service";
import {Map074Service} from "../../../providers/locationAndMap/map074-service";
import { Events } from 'ionic-angular';
import {AppDataProvider} from "../../../providers/app-data-provider";
import { DiagnosticService }  from '../../../providers/diagnostic-service';


let TAG = "TramosTabPage";

@IonicPage()
@Component({
  selector: 'tab-tramos',
  templateUrl: 'tramos-tab.html'
})
export class TramosTabPage {
  public isOnLine   = false;
  //public isLoggedIn = false;

  public mustShowIncidencias = false;
  public mustShowObras       = false;
  public mustShowTrafico     = false;

  public catTramos:catTramosType;
  public catPOI:   catPOIType;

  public numSelectedTramos = 0;
  public numSelectedPOI    = 0;

  private scrollContainer:any;
  private elemHeigth:number;
  private scrollTop:number;
  private numHiddenElements:number;
  public  scrollEnd:boolean   = false;
  public  scrollStart:boolean = false;

  private networkSubsc:any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private modalCtrl: ModalController,
              private DB: LocalDbSvc,
              private mapsvc:Map074Service,
              private appSvc: AppDataProvider,
              private events: Events,
              public  diagnosticSvc:  DiagnosticService
            ) {

  }

  ionViewDidEnter() {
    Utils.LOG.i(TAG, 'ionViewDidEnter TramosPage', "ionViewDidEnter()");

    //console.log("Querying mustShowIncidencias, resp: "+this.mapsvc.mustShowIncidencias());
    setTimeout(()=>{
      this.mustShowIncidencias = this.mapsvc.mustShowIncidencias();
      this.mustShowObras       = this.mapsvc.mustShowObras();
      this.mustShowTrafico     = this.mapsvc.mustShowTrafico();
    }, 600);

    this.networkSubsc = this.diagnosticSvc.networkConnectionChange().subscribe((state)=>{
      this.isOnLine = state;
    });
  }

  ionViewWillLeave(){
    this.networkSubsc.unsubscribe();
  }

  ionViewDidLoad(){
    if(Utils.runsOnDevice()){
      let loader0 = Utils.showLoading();
      this.DB.getCatTramos().subscribe((tramosCat:catTramosType)=>{
        this.DB.getCatPOI().subscribe((catPOI:catPOIType)=>{
          this.catPOI         = catPOI;
          this.numSelectedPOI = catPOI.numSelected;
          loader0.dismiss();
          //Utils.LOG.i(TAG, "---- this.catPOI: "+JSON.stringify(this.catPOI), "ionViewDidLoad->DB.getCatPOI()")
        }, reason=>{
          loader0.dismiss();
          Utils.showAlert("No se pudo cargar el catálogo de puntos de interés ya que"+reason);
        });

        this.catTramos           = tramosCat;
        this.numSelectedTramos   = tramosCat.numSelected;

        if(!tramosCat.numSelected) return;

        let contQueries = 0;
        let loader = Utils.showLoading("Sincronizando incidencias y obras");
        this.appSvc.getIncidenciasFromServer().subscribe((incidenciasArray:Array<incidenciaType>)=> {
          if(incidenciasArray.length){
            this.DB.populateIncidencias(incidenciasArray).subscribe((totalInserts) => {
              Utils.LOG.i(TAG, "Se han sincronizado " + totalInserts + " Incidencias", "drawIncidencias->getIncidenciasFromServer->populateIncidencias()");
              contQueries++;
              if(contQueries==2)
                loader.dismiss();
            }, reason=>{
              contQueries++;
              if(contQueries==2)
                loader.dismiss();
              Utils.showAlert("No se pudieron sincronizar las incidencias ya que"+reason)
            })
          }
          else{
            contQueries++;
            if(contQueries==2)
              loader.dismiss();
          }
        }, reason=>{
          contQueries++;
          if(contQueries==2)
            loader.dismiss();
          Utils.showAlert("No se pudieron obtener las incidencias desde el servidor ya que"+reason)
        });

        this.DB.getSelectedTramos().subscribe((tramosArray:[tramoType])=>{
          if(!tramosArray.length){
            contQueries++;
            if(contQueries==2)
              loader.dismiss();

            setTimeout(()=>{
              this.scrollContainer = document.querySelector("#scroll-indicator-container");
              this.scrollContainer.style.display = 'none';
              this.setScrollVars();
            }, 1400);
            return;
          }

          setTimeout(()=>{
            this.scrollContainer = document.querySelector("#scroll-indicator-container");
            this.setScrollVars();
          }, 1400);

          let ids = "";
          for(let i=0; i<tramosArray.length; i++){
            ids += tramosArray[i].id + (i< tramosArray.length-1 ? ',':'');
          }

          this.appSvc.getObrasFromServer(ids).subscribe((obrasArray:[obraType])=>{
            if(obrasArray.length){
              this.DB.populateObras(obrasArray).subscribe(totalSync=>{
                contQueries++;
                if(contQueries==2)
                  loader.dismiss();

                Utils.LOG.i(TAG, "Se sincronizaron "+totalSync+" obras", "ionViewDidLoad->...->populateObras()")
              }, reason=>{
                contQueries++;
                if(contQueries==2)
                  loader.dismiss();

                Utils.showAlert("No se pudieron sincronizar las obras ya que"+reason)
              });
            }
            else{
              contQueries++;
              if(contQueries==2)
                loader.dismiss();
            }
          }, reason=>{
            contQueries++;
            if(contQueries==2)
              loader.dismiss();

            Utils.showAlert("No se pudieron obtener las obras desde el servidor ya que"+reason)
          })
        }, reason=>{
          contQueries++;
          if(contQueries==2)
            loader.dismiss();

          Utils.showAlert("No se pudieron obtener los tramos seleccionados ya que"+reason)
        })
      }, reason=>{
        loader0.dismiss();
        Utils.showAlert("No se pudo cargar el catálogo de tramos ya que"+reason);
      });
    }
    else{
      let loader0 = Utils.showLoading("Cargando cat. tramos");
      this.appSvc.getTramosFromStorage().subscribe((catTramosStrg:{tramos:[tramoType], numSelected:number})=>{
        if(!catTramosStrg || !catTramosStrg.tramos.length){
          this.appSvc.getTramosFromServer().subscribe((catTramosSrv:[{
            id,
            nombre,
            acronimo, //<- no viene en el objeto de la respuesta
            autopista,//<- no viene en el objeto de la respuesta
            selected  //<- no viene en el objeto de la respuesta
          }])=>{

            catTramosSrv.forEach(tramo=>{
              tramo.acronimo  = "";
              tramo.autopista = "";
              tramo.selected  = false;
            });

            this.catTramos = {tramos: catTramosSrv, numSelected:0};
            this.numSelectedTramos = 0;

            Utils.LOG.i(TAG,
              "Se obtuvieron "+ catTramosSrv.length+" tramos del servidor",
              "ionViewDidLoad->getTramosFromStorage()"
            );

            this.appSvc.persistCatTramos(this.catTramos).then(()=>{
              loader0.dismiss();
              Utils.LOG.i(TAG, "Se almacenaron los tramos en localstorage", "ionViewDidLoad->...->persistCatTramos()"
              );
            }, localStrgError=>{
              Utils.showAlert("No se pudieron almacenar los tramos en localstorage ya que: "+JSON.stringify(localStrgError));
            })
          }, reason=>{
            loader0.dismiss();
            Utils.showAlert("No se pudo obtener el catálogo de tramos del servidor ya que"+reason);
          });
        }
        else{
          loader0.dismiss();
          this.catTramos = catTramosStrg;
          this.numSelectedTramos = catTramosStrg.numSelected;
          this.mapsvc.setTotalTramosSelected(catTramosStrg.numSelected);


          /*setTimeout(()=>{
            this.scrollContainer = document.querySelector("#scroll-indicator-container");
            this.scrollContainer.style.display = this.numSelectedTramos>7? 'block':'none';
            this.setScrollVars();
          }, 2400);*/

          Utils.LOG.i(TAG,
            "Se encontraron "+ catTramosStrg.tramos.length+" tramos en localstorage",
            "ionViewDidLoad->getTramosFromStorage()"
          );
        }
      }, reason=>{
        loader0.dismiss();
        Utils.showAlert("No se pudo obtener el catálogo de tramos de la BD local ya que"+reason);
      })
    }
  }



  public showModalTramos(){
    if(!this.catTramos){
      Utils.showAlert("No se puede mostrar el catálogo de Tramos ya que estos no se pudieron obtener");
      return;
    }

    let modal = this.modalCtrl.create(
      "ModalTramosPage",
      {data: JSON.parse(JSON.stringify(this.catTramos))},
      {enableBackdropDismiss: false}
    );

    modal.onDidDismiss(newCatTramos => {
      if(!newCatTramos){
        console.log("Cancelaste...");
        return;
      }

      this.checkTramosChanges(newCatTramos);
    });

    let loader = Utils.showLoading();
    modal.present().then(()=>{
      loader.dismiss();
    });
  }

  private checkTramosChanges(newCatTramos:any){
    let totalSelected = 0;

    newCatTramos.tramos.forEach((tramo, idx)=>{

      if(tramo.selected)
        totalSelected++;

      if(tramo.selected != this.catTramos.tramos[idx].selected && Utils.runsOnDevice()){
        this.DB.toggleTramo(tramo.id, tramo.selected ? 1: 0).subscribe(res=>{
          //Utils.LOG.i(TAG, "Tramo actualizado: "+res, "checkChanges()");
        }, sqliteError=>{
          Utils.LOG.e(TAG, sqliteError, "checkChanges->newCatTramos.forEach->toggleTramo()");
        })
      }
    });

    //Utils.LOG.i(TAG, "this.numSelectedTramos: "+this.numSelectedTramos+" totalSelected: "+totalSelected, "checkChanges()");

    if(this.numSelectedTramos == 0 && totalSelected >0)
      this.events.publish("showTramosTab", true);
    else if(this.numSelectedTramos > 0 && totalSelected == 0)
      this.events.publish("showTramosTab", false);

    this.catTramos             = newCatTramos;
    //this.catTramos.numSelected = totalSelected;
    this.numSelectedTramos     = totalSelected;
    setTimeout(()=>{
      this.setScrollVars();
    }, 1400);

    this.appSvc.persistCatTramos(newCatTramos);

    this.mapsvc.setTotalTramosSelected(totalSelected);
  }

  public showModalPOI(){
    if(!this.numSelectedTramos){
      Utils.showAlert("Seleccione al menos un tramo");
      return;
    }

    if(!this.catPOI){
      Utils.showAlert("No se puede mostrar el catálogo de Puntos de interes ya que estos no se pudieron obtener");
      return;
      /*this.catPOI = {
        poi:[
          {id:1,nombre:"Plazas de Cobro",selected:true},
          {id:2,nombre:"Gasolineras",selected:false},
          {id:3,nombre:"Servicio Médico",selected:false},
          {id:4,nombre:"Paraderos",selected:false}
        ],
        numSelected:1
      }*/
    }

    let modal = this.modalCtrl.create(
      "ModalPoiPage",
      {data: JSON.parse(JSON.stringify(this.catPOI))},
     {enableBackdropDismiss: false}
    );

    modal.onDidDismiss(newCatPOI => {
      if(!newCatPOI){
        console.log("Cancelaste...");
        return;
      }

      this.checkPOIChanges(newCatPOI);
    });

    let loader = Utils.showLoading();
    modal.present().then(()=>{
      loader.dismiss();
    });
  }

  private checkPOIChanges(newCatTPOI:any){
    let totalSelected = 0;

    newCatTPOI.poi.forEach((poi, idx)=>{
      if(poi.selected)
        totalSelected++;

      if(poi.selected != this.catPOI.poi[idx].selected && Utils.runsOnDevice()){
        this.DB.togglePOI(poi.id, poi.selected ? 1: 0).subscribe(res=>{
          //Utils.LOG.i(TAG, "Tramo actualizado: "+res, "checkChanges()");
        }, sqliteError=>{
          Utils.LOG.e(TAG, sqliteError, "checkPOIChanges->newCatPOI.forEach->togglePOI()");
        })
      }
    });

    this.catPOI         = newCatTPOI;
    this.numSelectedPOI = totalSelected;

    this.appSvc.persistCatPOI(newCatTPOI);

    this.mapsvc.setTotalPOISelected(totalSelected);
  }

  public toggleShowIncidentes(){
    this.mustShowIncidencias = !this.mapsvc.mustShowIncidencias();
    this.mapsvc.setMustShowIncidencias(this.mustShowIncidencias);
  }

  public toggleShowObras(){
    this.mustShowObras = !this.mapsvc.mustShowObras();
    this.mapsvc.setMustShowObras(this.mustShowObras);
  }

  public toggleShowTrafico(){
    this.mustShowTrafico = !this.mapsvc.mustShowTrafico();
    this.mapsvc.setMustShowTrafico(this.mustShowTrafico);
  }

  private lastScrollTriggerState = false;
  @HostListener('window:scroll', ['$event'])
  public onScrollEvent($event){
    this.setScrollVars();
    /*if($event.offsetHeight + $event.scrollTop == $event.scrollHeight){
        alert("End");
    }*/

    if(this.numSelectedTramos<=7)return;
    let targetScroll = this.numHiddenElements * this.elemHeigth;
    let show         = !(this.scrollTop+10 < targetScroll);//porque son 10px de padding)

    if(this.lastScrollTriggerState == show){

      this.lastScrollTriggerState = !show;
      this.scrollContainer.style.display = !show ? 'block' : 'none';
    }
  }

  private setScrollVars(){
    setTimeout(()=>{
        let scrollContainer = document.querySelector("#scrolled-list");
        let contentDiv      = document.getElementById("scrolled-list");
        if(contentDiv.offsetHeight + scrollContainer.scrollTop == scrollContainer.scrollHeight){
            this.scrollEnd = true;
        }else{
            this.scrollEnd = false;
        }

        if(scrollContainer.scrollTop==0){
          this.scrollStart = false;
        }else{
          this.scrollStart = true;
        }

    }, 500);
  }
}
