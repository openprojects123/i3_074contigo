import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {UbicacionTabPage} from "./ubicacion-tab";

@NgModule({
  declarations: [
    UbicacionTabPage,
  ],
  imports: [
    IonicPageModule.forChild(UbicacionTabPage),
  ],
  exports: [
    UbicacionTabPage
  ]
})
export class UbicacionTabPageModule {}
