import {
  Component,
  ChangeDetectorRef
}                     from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                     from 'ionic-angular';
import {
  GeolocationService,
  GeolocationServiceResponse
}              from "../../../providers/locationAndMap/geolocation-service";
import {Utils} from "../../../providers/utils-service";
import {
  LocalDbSvc,
  tramoType,
  latlngType,
  neighborType,
  coordsTramoType
}                       from "../../../providers/localDb-service";
import {Map074Service}  from "../../../providers/locationAndMap/map074-service";
import {AppDataProvider} from "../../../providers/app-data-provider";
import {UserService}    from "../../../providers/user-service";
import { DiagnosticService }  from '../../../providers/diagnostic-service';


let TAG = "UbicacionTabPage";

@IonicPage()
@Component({
  selector: 'tab-ubicacion',
  templateUrl: 'ubicacion-tab.html'
})
export class UbicacionTabPage {
  public isOnLine     = false;
  public isGpsEnabled = false;
  public isSessionOK  = false;
  public isOnRoad:boolean;

  public tramoDetail: {tramoData: tramoType, km:number}= {
    tramoData: {
      id:       null,
      nombre:   "",
      acronimo: "",
      autopista:"",
      selected: false
    },
    km: null
  };

  private userLocation:latlngType;

  private networkSubsc:any;
  private gpsSubsc:any;

  constructor(private cd: ChangeDetectorRef,
              public navCtrl: NavController,
              public navParams: NavParams,
              private locationSvc: GeolocationService,
              private map074Service: Map074Service,
              private appSvc: AppDataProvider,
              private DB: LocalDbSvc,
              private userSvc: UserService,
              public  diagnosticSvc:  DiagnosticService
            ) {

  }

  ionViewDidEnter(){
    this.isSessionOK = this.userSvc.isLoggedIn();

    this.networkSubsc = this.diagnosticSvc.networkConnectionChange().subscribe((state)=>{
      this.isOnLine = state;
    });

    this.diagnosticSvc.isLocationEnabled().subscribe(isEnabled=>{
      this.gpsSubsc = this.diagnosticSvc.gpsConnectionChange().subscribe((enabled)=>{
        this.isGpsEnabled = enabled;
      }, reason=>{
        this.isGpsEnabled = false;
        Utils.showAlert("No se pudo verificar el estado del GPS dinámicamente ya que"+reason);
      });

      if(isEnabled)
        this.getNearestTramo();
    }, reason=>{
      this.isGpsEnabled = false;
      Utils.showAlert("No se pudo verificar el estado del GPS ya que"+reason);
    })
  }

  ionViewWillLeave(){
    this.networkSubsc.unsubscribe();

    if(this.gpsSubsc){
      this.gpsSubsc.unsubscribe();
      this.diagnosticSvc.gpsStopWatching();
    }
  }


  public getNearestTramo(){
    this.userLocation = null;

    let loader = Utils.showLoading("Localizando...");
    this.locationSvc.getCurrentPosition().subscribe((location: GeolocationServiceResponse)=>{
      let position = {lat: location.coords.latitude, lng: location.coords.longitude};
      this.userLocation = position;
      
      this.map074Service.maps.loadSDK().subscribe(()=>{
        
        this.map074Service.getNearestTramo(position, this.map074Service.distFromCenter).subscribe((neighbor:neighborType)=>{
          
          Utils.LOG.i(TAG, "neighbor: "+JSON.stringify(neighbor), "getNearestTramo..->map074Service.getNearestTramo()");
          loader.dismiss();
          
          this.getTramoDetails(neighbor.tramo);
        }, reason=>{
          loader.dismiss();
          Utils.showAlert("No se pudo obtener el tramo más cercano ya que"+reason);
        })
      }, reason=>{
        loader.dismiss();
        Utils.showAlert("No se pudo inicializar la librería de mapas ya que"+reason);
      })
    }, reason=>{
      loader.dismiss();
      Utils.showAlert("No se pudo obtener la ubicación inicial ya que"+reason);
    })
  }

  private getTramoDetails(tramo:coordsTramoType){
    if(tramo && tramo.id){
      let loader = Utils.showLoading("...");

      Utils.LOG.i(TAG, JSON.stringify(tramo), "getTramoDetails->getTramoById()");
      this.DB.getTramoById(tramo.idAutopista).subscribe((tramoData:tramoType)=>{
        loader.dismiss();

        if(tramoData){
          this.tramoDetail.tramoData = tramoData;
          this.tramoDetail.km        = tramo.km;// /1000;
          this.isOnRoad              = true;
        }
        else{
          this.isOnRoad = false;
        }

        this.cd.detectChanges();
      }, error=>{
        loader.dismiss();
        this.isOnRoad = false;
        Utils.showAlert("No se pudo derterminar la información del tramo más cercano ya que"+error);
      })
    }
    else{
      Utils.LOG.w(TAG, "El tramo o el id del tramo son nulos", "getTramoDetails()");
    }
  }

  public onPressBtn(btnId:number){
    Utils.showConfirm("Sólo debes usar este botón en caso de una emergencia <b>REAL</b>", {
      title: "Solicitar ayuda",
      okHandler: ()=>{
        this.requestEmergency(btnId);
      }
    })
  }

  public onPressBtnError(){
    let msg = "";
    if(!this.isSessionOK)
      msg = "Debes iniciar sesión para poder usar esta función";
    else if(!this.isOnRoad)
      msg = "Sólo puedes usar esta función si estás sobre un tramo CAPUFE";
    else
      msg = "No es posible usar esta función ya que hay un error de configuración";

    Utils.showAlert(msg);
  }

  private requestEmergency(emergencyType:number){
    let loader = Utils.showLoading();

    let solicitud = {
      idusuario: this.userSvc.getId(),
      idtipoemergencia: emergencyType,
      idautopista: this.tramoDetail.tramoData.id,
      km: this.tramoDetail.km,
      lat: this.userLocation.lat,
      lng: this.userLocation.lng,
      detalles: "desde botón de emergencia",
    };

    Utils.LOG.i(TAG, "solicitud: "+ JSON.stringify(solicitud), "requestEmergency()");
    this.appSvc.sendSolicitud(solicitud).subscribe((resp)=>{
      loader.dismiss();
      Utils.showAlert(
        "Su reporte se registró con el número: <b>"+resp.id+"</b><br>"+
        "Puede ver los detalles en el menú <b>Reportes</b>",
      );
    }, (reason)=>{
      loader.dismiss();
      Utils.showAlert("Su solicitud no pudo ser enviada ya que"+ reason);
    });
  }
}
