import { Component,
         Input }            from '@angular/core';
import { IonicPage,
         NavController,
         NavParams }        from 'ionic-angular';
import { Utils }            from "../../providers/utils-service";
import { Storage  }         from '@ionic/storage';
import { GamePage }         from "./game/game";
import {AppDataProvider}    from "../../providers/app-data-provider";


let contador = 1;
let acumuladorResp = 0;
let acumuladorPts  = 0;

@IonicPage()
@Component({
  selector: 'page-juego',
  templateUrl: 'juego.html',
})
export class JuegoPage {

  @Input('progress') progress;
  public start:boolean = false;

  public preguntas:         any       = [];
  public estado_pregunta:   boolean   = false;
  public preg_opc:          any;
  public preg_cont:         number    = 0;
  public opList:            any;
  public arrayPlayer:       any       = [];
  public opcPlayer:         any;
  public gamerPlay:         any;
  public choose_player:     boolean   = false;
  
  public reShow:boolean     = false;
  public resCont:number     = 0;
  public resCont1:number    = 0;
  public resPreg:boolean    = false;
  public res_on:boolean     = false;
  public res_final:boolean  = true;
  public res_on_coment:any;
  public res_on_coment_title:any;
  public respuesta_texto:any;

  public gamer1:any;
  public gamer2:any;
  public gamer3:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage, private appSvc: AppDataProvider) {
    contador = 1;
    acumuladorResp = 0;
    acumuladorPts  = 0;

    let aux_array  = localStorage.getItem("players");
    if(aux_array){
        this.arrayPlayer = JSON.parse(localStorage['players']);
        this.bubbleSort();
    }else{
      let obj = {id:0,nombre:"Device",score:0,partidas:0,promedio:0};
      this.arrayPlayer.push(obj);
      localStorage['players'] = JSON.stringify(this.arrayPlayer);
      this.arrayPlayer        = JSON.parse(localStorage['players']);
    }

    this.openSettings();
  }


  public startPlay(){
    this.start = true;
    this.progress = 0;
  }

  public newUser(){
    Utils.showPrompt({title:"Nuevo usuario",
    inputs:[{
      name: 'name',
      placeholder: 'Nombre'
    },],buttons:[{
        text: 'Cancelar',
        handler: data => {
          console.log('Cancel clicked');
        }
    },{
        text: 'Aceptar',
        handler: data => {
          let aux  = this.arrayPlayer[this.arrayPlayer.length-1];
          let obj = {id:aux.id+1,nombre:data.name,score:0,partidas:0,promedio:0};
          this.arrayPlayer.push(obj);
          localStorage['players'] = JSON.stringify(this.arrayPlayer);
          this.arrayPlayer        = JSON.parse(localStorage['players']);
        }
    }]});
  }

  public choosePlayer(){
    this.gamerPlay = this.arrayPlayer[this.opcPlayer];
    this.choose_player = true;
    console.log(JSON.stringify(this.arrayPlayer));
  }


  public changeState(){
    if(this.estado_pregunta == false){
      this.preg_cont = this.preg_cont +1;
      this.estado_pregunta = true;
    }
  }

  public calificar(){
    if(this.estado_pregunta==true){
      this.estado_pregunta = false;

      this.progress = this.progress + (100/7);
      this.progress = Math.floor(this.progress);

      this.preguntas[this.preg_cont-1].estado = false;
      let respCorrecta,numberOfPreg;

      if(this.preguntas[this.preg_cont-1].respuesta[0].value == true){
        respCorrecta = "a";
        numberOfPreg = 0;
      }
      if(this.preguntas[this.preg_cont-1].respuesta[1].value == true){
        respCorrecta = "b";
        numberOfPreg = 1;
      }
      if(this.preguntas[this.preg_cont-1].respuesta[2].value == true){
        respCorrecta = "c";
        numberOfPreg = 2;
      }
      if(this.preguntas[this.preg_cont-1].respuesta[3].value == true){
        respCorrecta = "d";
        numberOfPreg = 3;
      }

      if(this.opList == respCorrecta){
        this.resPreg              = true;
        acumuladorPts             = acumuladorPts+this.preguntas[this.preg_cont-1].pts;
        acumuladorResp            = acumuladorResp+1;
        this.res_on_coment_title  = "!Correcto! 😊";
        this.res_on_coment        = "!Tienes razon!, la respuesta correcta es: ";
        this.respuesta_texto      = this.preguntas[this.preg_cont-1].respuesta[numberOfPreg].opc;
      }else{
        this.resPreg = false;
        this.res_on_coment_title  = "!Incorrecto! 🙁";
        this.res_on_coment        ="La respuesta correcta es:";
        this.respuesta_texto      = this.preguntas[this.preg_cont-1].respuesta[numberOfPreg].opc;
      }
      this.opList = "";
      this.res_on = true;

      if(this.preguntas.length==this.preg_cont){
        let aux1                = parseInt(this.progress);
        let aux2                = 100-aux1;
        this.progress           = aux1+aux2;
        this.gamerPlay.score    = this.gamerPlay.score+acumuladorPts;
        this.gamerPlay.partidas = this.gamerPlay.partidas+1;
        this.gamerPlay.promedio = this.gamerPlay.score/this.gamerPlay.partidas;
        this.gamerPlay.promedio = this.gamerPlay.promedio.toFixed(2); 
        this.arrayPlayer[this.gamerPlay.id] = this.gamerPlay;
        this.bubbleSort();
        localStorage['players'] = JSON.stringify(this.arrayPlayer);
      }
    }else{
      Utils.showAlert("Necesitas seleccionar una respuesta para seguir...");
    }
  }

  public nextQuestion(){
    this.res_on     = false;
    if(this.preg_cont < this.preguntas.length){
      this.preguntas[this.preg_cont].estado = true;
    }else{
      this.res_final = false;
      this.resCont   = acumuladorResp;
      this.resCont1  = acumuladorPts;
      this.reShow    = true;
    }
  }

  public goToGame(){
    this.navCtrl.push(GamePage);
  }

  public openSettings(){
    this.appSvc.getSettingsGame().subscribe(config=>{
      this.preguntas = config;
    })
  }

  public restarGame(){
    //little comment
    this.openSettings();
    contador = 1;
    acumuladorResp      = 0;
    acumuladorPts       = 0;
    this.resCont        = 0;
    this.resCont1       = 0;
    this.progress       = 0;
    this.start          = true;
    this.res_final      = true;
    this.reShow         = false;
    this.preg_cont      = 0;
    this.choose_player  = false;
    this.opcPlayer      = "";
    this.gamerPlay      = "";
    this.start          = false;
  }


  public bubbleSort() {
    var len = this.arrayPlayer.length;
  
    for (var i = 0; i < len ; i++) {
      for(var j = 0 ; j < len - i - 1; j++){ // this was missing
      if (this.arrayPlayer[j].promedio < this.arrayPlayer[j + 1].promedio) {
        // swap
        var temp = this.arrayPlayer[j];
        this.arrayPlayer[j] = this.arrayPlayer[j+1];
        this.arrayPlayer[j + 1] = temp;
      }
      }
    }
    for (var i = 0; i < len ; i++) {
      this.arrayPlayer[i].id = i;
    }
  }


}
