import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {RealidadAumPage} from "./realidad-aum";

@NgModule({
  declarations: [
    RealidadAumPage,
  ],
  imports: [
    IonicPageModule.forChild(RealidadAumPage),
  ],
  exports: [
    RealidadAumPage
  ]
})
export class RealidadAumPageModule {}
