import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                     from 'ionic-angular';
import {Utils}        from "../../providers/utils-service";
import {GLOBALS}      from "../../app/globals";
import {
  LocalDbSvc,
  POIType
}                     from "../../providers/localDb-service";


let easyrtc:any;
let examplePOI:any;
declare let cordova:any;
let DB: LocalDbSvc;
let wikitudePlugin:any;
let onLocationUpdated:any;
let onLocationError:any;

let TAG = "RealidadAumPage";

@IonicPage()
@Component({
  selector: 'page-realidad-aum',
  templateUrl: 'realidad-aum.html'
})
export class RealidadAumPage {

  private samples = [
    {
      "path": "www/world/VIDEO/index.html",
      "requiredFeatures": ["2d_tracking"],
      "startupConfiguration": {"camera_position": "back"}
    },
    {
      "path": "www/world/RADAR/index.html",
      "requiredFeatures": ["geo"],
      "startupConfiguration": {"camera_position": "back"}
    }
  ];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private db: LocalDbSvc,
            ) {
    DB = this.db;

    examplePOI = [
      {id:946,name:"Torre de Auxilio",tramoName:"x",pistaName:"p1",km:123, mt:456,cuerpo:"c1", latitude:19.02429,longitude:-99.16811,description:"Gasolinera 1 Pemex, magna, premium y diesel",altitude:100},
      {id:947,name:"Gasolineras",tramoName:"y",pistaName:"p2",km:223, mt:356,cuerpo:"c2",latitude:18.97405,longitude:-99.23648,description:"Gasolinera 2 Pemex, magna, premium y diesel",altitude:100},
      {id:949,name:"Torre de Auxilio",tramoName:"z",pistaName:"p3",km:133, mt:466,cuerpo:"c3",latitude:19.04304,longitude:-99.2333,description:"Gasolinera 3 Pemex, magna, premium y diesel",altitude:100}
    ];

    Utils.platform.ready().then(()=>{
      this.initialize();
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad '+TAG);

    setTimeout(()=>{
      easyrtc = window['easyrtc'];
      this.connect();
    }, 2000)
  }

  private selfEasyrtcid = "";


  connect() {
    easyrtc.setVideoDims(640,480);
    //easyrtc.setRoomOccupantListener(convertListToButtons);
    easyrtc.easyApp("easyrtc.audioVideoSimple", "selfVideo", [], this.loginSuccess, this.loginFailure);
  }

  loginSuccess(easyrtcid) {
    this.selfEasyrtcid = easyrtcid;
    console.log(easyrtc.cleanId(easyrtcid))
  }


  loginFailure(errorCode, message) {
    easyrtc.showError(errorCode, message);
    console.error(errorCode);
    console.error(message);
  }

  // Application Constructor
  private initialize() {
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    document.addEventListener('deviceready', this.onDeviceReady, false);
  }

  // deviceready Event Handler
  private onDeviceReady() {
    wikitudePlugin = cordova.require("com.wikitude.phonegap.WikitudePlugin.WikitudePlugin");
    wikitudePlugin._sdkKey = "WgHoCiE1AR7tfH4TIEuoqJ3KlIzOijWJaH90BM5yF0JQ8tV/UOWMo+mwNo0SaSS3CDxUXoHIZwSZqpipZASEjdgiMnhaAiHctBS1rSlbbjgXGT3AG78cDtnE+XuCbdTjhqCwbbF29QhvFSlBtl9c+wEW3sj8ZLcetRAS8uV4Z6RTYWx0ZWRfX7ci0TuN8MwV3nvIQe8vNY+LL4g2msDGzEC/RVqsZVe3gjU6gUP6xZuBVEs6mFS4DhdmnFy+PXS43w7BT2bJfvF4/ismPjJ+Y07BT0E6UpkoxoVdWqnzaSQGsXYwMZttSK1AeKP3aN3CRpV5Bf8rnhUgEZ2duo3YjCOFRxT/w3MhTrCnG5Qe7WbKWIyg71dGzgLU5Leh28Az87pGud+IP0so3G8wobumVinPKg/zDOdIfpNt9Rf9+3LDb0mMx36CF6TV/+U4RxxQbPFgz/p1/PpYW6UOYGgpvC9sWDJPw0diH3TfA7S2kR4A+krY2OpPLADdQTNMbN86gHTjmqahP6b9nSLmwIfLSRhRTY9u9G+D3aQ1Td0dlqYMbS87jB3uj2lRMrFEO4etZPury8z2Syu3Y0Ls9C4MWHQfN3iY/qYHzt6RqCXR7y2O1bQRCu2cVC4pk9GXX4Zq2ZZ7QCKqNOmiNhhPAjqVlcShTxUaOveUXSuW81zDZb0=";

    window["onUrlInvoke"] = this.onUrlInvoke;
  }

  // --- Wikitude Plugin ---
  // Use this method to load a specific ARchitect World from either the local
  //  file system or a remote server
  public loadARchitectWorld (example) {
    let loader = Utils.showLoading("loading world");
    setTimeout(()=>{
      loader.dismiss();
      this.showARWorld(example);
    }, GLOBALS.ONE_SECOND)
  }

  private showARWorld(example){
    try{
      // check if the current device is able to launch ARchitect Worlds
      wikitudePlugin.isDeviceSupported(()=>{
        wikitudePlugin.setOnUrlInvokeCallback(this.onUrlInvoke);
        // inject poi data using phonegap's GeoLocation API and inject data using
        //  World.loadPoisFromJsonData
        if (example.requiredExtension === "ObtainPoiDataFromApplicationModel") {
          navigator.geolocation.getCurrentPosition(onLocationUpdated, onLocationError);
        }

        wikitudePlugin.loadARchitectWorld((loadedURL)=> {
            /* Respond to successful world loading if you need to */
          },  (error)=> {
            Utils.showAlert('No se pudo cargar el módulo AR debido a: ' + error);
          },example.path, example.requiredFeatures, example.startupConfiguration
        );
      }, (errorMessage)=> {
        Utils.showAlert("Dispositivo no soportado :( {"+errorMessage+"}");
      }, example.requiredFeatures);
    }
    catch (e){
      Utils.showAlert("No se pudo iniciar el módulo ya que"+Utils.LOG.w(TAG, e, "loadARchitectWorld->wikitudePlugin.isDeviceSupported()"));
    }
  }

  // This function gets called if you call "document.location = architectsdk://" in your ARchitect World
  private onUrlInvoke(url) {
    if (url.indexOf('captureScreen') > -1) {
      wikitudePlugin.captureScreen((absoluteFilePath)=> {
          alert("snapshot stored at:\n" + absoluteFilePath);
        },
        (errorMessage) =>{
          alert(errorMessage);
        },
        true, null
      );
    }
    else if(url.indexOf('retrievePOIS') > -1){
      wikitudePlugin.callJavaScript('retrievePOIS('+JSON.stringify(examplePOI)+')');

      DB.getSelectedTramos().subscribe((tramos)=>{
        let idsTramos:[number] = [null];
        tramos.forEach((tr)=>{
          idsTramos.push(tr.id);
        });

        DB.getSelectedPOI().subscribe((POIS:[POIType])=>{
          Utils.LOG.i(TAG, "TRAMOS: "+idsTramos.length+ ", POIS: "+POIS.length, "onUrlInvoke->getSelectedTramos->getSelectedPOI()");
          let ids = [];
          POIS.forEach(poi=>{
            ids.push(poi.id);
          });

          DB.getPoiWithNameByTramo(idsTramos, ids).subscribe((resp)=>{
            let data = [];
            resp.forEach((poi)=>{
              //console.log("poi: " + JSON.stringify(poi));
              data.push({
                id:          poi.id,
                name:        poi.nombrepoi || "Sin título",
                tramoName:   poi.nombretramo,
                pistaName:   poi.nombrepista,
                km:          poi.km,
                mt:          poi.mt,
                latitude:    poi.lat,
                longitude:   poi.lng,
                cuerpo:      poi.cuerpo  || "*Sin descripción",
                description: poi.detalle || "Sin descripción",
                altitude:    100
              });
            });
            Utils.LOG.i(TAG, "TOTAL POIS A ENVIAR:"+ data.length, "onUrlInvoke->getSelectedTramos->getSelectedPOI->getPoiWithNameByTramo()");

            //wikitudePlugin.callJavaScript('retrievePOIS('+JSON.stringify(data)+')');
          }, (error)=>{alert(error);});
        });
      }, (error)=>{alert(error);});
    }
    else if(url.indexOf('closeARWorld') > -1){
      //console.log("Cerrando AR World");
      wikitudePlugin.close();
    }
    else {
      alert(url + "not handled");
    }
  }


  public getSamplePath(sample) {
    return this.samples[sample];
  };

}

export interface latlngType{
  lat:number, lng:number
}
