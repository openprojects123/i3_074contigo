import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {RealizarReporteTabPage} from "./realizar-reporte-tab";

@NgModule({
  declarations: [
    RealizarReporteTabPage,
  ],
  imports: [
    IonicPageModule.forChild(RealizarReporteTabPage),
  ],
  exports: [
    RealizarReporteTabPage
  ]
})
export class RealizarReporteTabModule {}
