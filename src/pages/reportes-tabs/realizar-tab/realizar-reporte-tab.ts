import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                     from 'ionic-angular';
import {Utils}        from "../../../providers/utils-service";
import {UserService}  from "../../../providers/user-service";
import {
  AppDataProvider,
  catTipoReporteType,
  catsubTipoReporteType
} from "../../../providers/app-data-provider";
import { DiagnosticService }  from '../../../providers/diagnostic-service';

let TAG = "RealizarReporteTabPage";

@IonicPage()
@Component({
  selector: 'tab-realizarReporte',
  templateUrl: 'realizar-reporte-tab.html'
})
export class RealizarReporteTabPage {
  public isOnLine    = false;
  public isSessionOK = false;
  public detalles:string = "";

  public catTipoReporte: [catTipoReporteType];
  public tipoReporte;
  public catSubtipoReporte:[catsubTipoReporteType];
  public subtipoReporte;

  private networkSubsc:any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private appSvc: AppDataProvider,
              private userSvc: UserService,
              public  diagnosticSvc:  DiagnosticService
            ) {

    let loader = Utils.showLoading("Cargando Tipo");
    appSvc.getCatTipoReporte().subscribe((catTipoReporte:[catTipoReporteType])=>{
      loader.dismiss();
      this.catTipoReporte = catTipoReporte;
      this.tipoReporte    = catTipoReporte[0].id;
      this.getSubTipoReporte(catTipoReporte[0].id);
    }, reason=>{
      loader.dismiss();
      Utils.showAlert("No se pudo obtener el catálogo de Tipos ya que"+reason);
    });
  }

  ionViewDidEnter(){
    this.networkSubsc = this.diagnosticSvc.networkConnectionChange().subscribe((state)=>{
      this.isOnLine = state;
    });
    this.isSessionOK = this.userSvc.isLoggedIn();
  }

  ionViewWillLeave(){
    this.networkSubsc.unsubscribe();
  }

  private getSubTipoReporte(idTipo:number){
    let loader2 = Utils.showLoading("Cargando Subtipo");
    this.appSvc.getCatSubtipoReporte(idTipo).subscribe((catSubtipoReporte:[catsubTipoReporteType])=>{
      loader2.dismiss();
      this.catSubtipoReporte = catSubtipoReporte;
      this.subtipoReporte    = catSubtipoReporte[0].id;
    }, reason=>{
      loader2.dismiss();
      Utils.showAlert("No se pudo obtener el catálogo de Subtipos ya que"+reason);
    });
  }

  public enviarReporte(){
    let loader = Utils.showLoading();
    let form = {
      idsubtiporeporte:this.subtipoReporte,
      idusuario:       this.userSvc.getId(),
      notas:           this.detalles
    };
    this.appSvc.newReporte(form).subscribe((response)=>{
      loader.dismiss();
      this.detalles = "";

      Utils.showAlert("El reporte se ha almacenado con id: "+response.id);
    }, reason=>{
      loader.dismiss();
      Utils.showAlert("No se pudo enviar el reporte ya que"+reason);
    });
  }
}
