import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {ReportesTabsPage} from "./reportes-tabs";

@NgModule({
  declarations: [
    ReportesTabsPage,
  ],
  imports: [
    IonicPageModule.forChild(ReportesTabsPage),
  ],
  exports: [
    ReportesTabsPage
  ]
})
export class ReportesTabsPageModule {}
