import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                     from 'ionic-angular';

let TAG = "ReportesTabsPage";

@IonicPage()
@Component({
  selector: 'page-reportes',
  templateUrl: 'reportes-tabs.html'
})
export class ReportesTabsPage {

  tab1Root = "RealizarReporteTabPage";
  tab2Root = "SeguimientoReporteTabPage";

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {

  }

}
