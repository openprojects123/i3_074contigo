import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {SeguimientoReporteTabPage} from "./seguimiento-reporte-tab";
import {PipesModule} from "../../../pipes/pipes.module";


@NgModule({
  declarations: [
    SeguimientoReporteTabPage,
  ],
  imports: [
    IonicPageModule.forChild(SeguimientoReporteTabPage),
    PipesModule,
  ],
  exports: [
    SeguimientoReporteTabPage
  ]
})
export class SeguimientoReporteTabPageModule {}
