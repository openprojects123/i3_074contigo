import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                     from 'ionic-angular';
import {AppDataProvider} from "../../../providers/app-data-provider";
import {Utils} from "../../../providers/utils-service";
import {UserService} from "../../../providers/user-service";

let TAG = "SeguimientoReporteTabPage";

@IonicPage()
@Component({
  selector: 'tab-seguimientoReporte',
  templateUrl: 'seguimiento-reporte-tab.html',
})
export class SeguimientoReporteTabPage {

  public reportesList = [];
  public order     = ["-idreporte"];
  public orderAttr = "idreporte";
  public orderType = "down"; //'up' | 'down'

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private appSvc: AppDataProvider,
              private userSvc: UserService) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad '+TAG);
    let loader = Utils.showLoading();
    this.appSvc.getReportesUser(this.userSvc.getId()).subscribe((reportes:[any])=>{
      this.reportesList = reportes;
      loader.dismiss();
    }, reason=>{
      loader.dismiss();
      Utils.showAlert("No se pudieron obtener las solicitudes ya que"+reason);
    })
  }

  public showDetails(idReporte){

  }


  public sortByProperty (property:string) {
    if(this.orderAttr == property){
      if(this.orderType == "down"){
        this.order[0]  = property;
        this.orderType = "up";
      }
      else{
        this.order[0]  = "-"+property;
        this.orderType = "down";
      }
    }
    else{
      this.order[0]  = "-"+property;
      this.orderType = "down";
    }

    this.orderAttr = property;
  }


}
