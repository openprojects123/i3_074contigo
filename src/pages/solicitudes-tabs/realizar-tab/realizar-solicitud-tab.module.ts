import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {RealizarSolicitudTabPage} from "./realizar-solicitud-tab";

@NgModule({
  declarations: [
    RealizarSolicitudTabPage,
  ],
  imports: [
    IonicPageModule.forChild(RealizarSolicitudTabPage),
  ],
  exports: [
    RealizarSolicitudTabPage
  ]
})
export class RealizarSolicitudTabPageModule {}
