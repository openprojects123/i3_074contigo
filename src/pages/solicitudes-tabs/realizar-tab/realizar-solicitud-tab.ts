import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                       from 'ionic-angular';
import {AppDataProvider} from "../../../providers/app-data-provider";
import {Utils}          from "../../../providers/utils-service";
import {UserService}    from "../../../providers/user-service";
import {
  GeolocationService,
  GeolocationServiceResponse
} from "../../../providers/locationAndMap/geolocation-service";
import {Map074Service} from "../../../providers/locationAndMap/map074-service";
import {
  neighborType,
  latlngType
} from "../../../providers/localDb-service";
import { DiagnosticService }  from '../../../providers/diagnostic-service';

let TAG = "RealizarSolicitudTab";

@IonicPage()
@Component({
  selector: 'tab-realizarSolicitud',
  templateUrl: 'realizar-solicitud-tab.html'
})
export class RealizarSolicitudTabPage {
  public isOnLine   = false;
  public isLoggedIn = false;
  public idtipoemergencia:number;
  public detalles:string = "";

  public catEmergencias = [];

  private networkSubsc:any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private appSvc: AppDataProvider,
              private userSvc: UserService,
              private locationSvc: GeolocationService,
              private map074Svc: Map074Service,
              public  diagnosticSvc:  DiagnosticService
            ) {

    let loader = Utils.showLoading();
    appSvc.getCatEmergencias().subscribe(catEmergencias=>{
      loader.dismiss();
      this.catEmergencias = catEmergencias;
    }, reason=>{
      loader.dismiss();
      Utils.showAlert("No se pudo obtener el catálogo de emergencias ya que"+reason);
    });
  }

  ionViewDidEnter(){
    this.networkSubsc = this.diagnosticSvc.networkConnectionChange().subscribe((state)=>{
      this.isOnLine = state;
    });
    this.isLoggedIn = this.userSvc.isLoggedIn();
  }

  ionViewWillLeave(){
    this.networkSubsc.unsubscribe();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RealizarPage');
  }

  public enviarSolicitud(){
    let preError = "No se puede enviar la solicitud en este momento ya que";

    let loader = Utils.showLoading();
    this.locationSvc.getCurrentPosition().subscribe((location: GeolocationServiceResponse)=>{
      let latlng = {lat: location.coords.latitude, lng: location.coords.longitude};
      this.map074Svc.getNearestTramo(latlng, this.map074Svc.distFromCenter).subscribe((neighbor:neighborType)=> {
        loader.dismiss();
        if (neighbor.tramo) {
          this.sendSolicitud(latlng, neighbor);
        }
      }, reason=>{
        loader.dismiss();
        Utils.showAlert(preError+" no se pudo obtener la información del tramo por que"+reason)
      })
    }, reason=>{
      loader.dismiss();
      Utils.showAlert(preError+" no se pudo obtener la ubicación actual por que"+reason)
    });
  }

  private sendSolicitud(latlng: latlngType, neighbor: neighborType){
    let loader = Utils.showLoading();

    let form = {
      idusuario:        this.userSvc.getId(),
      idtipoemergencia: this.idtipoemergencia,
      idautopista:      neighbor.tramo.idAutopista,
      km:               neighbor.tramo.km,
      lat:              latlng.lat,
      lng:              latlng.lng,
      detalles:         this.detalles
    };

    this.appSvc.sendSolicitud(form).subscribe(resp=>{
      loader.dismiss();
      console.log(resp);
      Utils.showAlert("Su solicitud se registró correctamente");
      //todo: redireccionar a mis solicitudes???
    }, reason=>{
      loader.dismiss();
      Utils.showAlert("No se pudo enviar su solicitud ya que"+reason+" inténtelo nuevamente más tarde.");
    })
  }
}
