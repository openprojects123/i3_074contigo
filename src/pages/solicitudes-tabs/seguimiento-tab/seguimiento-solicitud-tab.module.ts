import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {SeguimientoSolicitudTabPage} from "./seguimiento-solicitud-tab";
import {PipesModule} from "../../../pipes/pipes.module";

@NgModule({
  declarations: [
    SeguimientoSolicitudTabPage,
  ],
  imports: [
    IonicPageModule.forChild(SeguimientoSolicitudTabPage),
    PipesModule
  ],
  exports: [
    SeguimientoSolicitudTabPage
  ]
})
export class SeguimientoSolicitudTabPageModule {}
