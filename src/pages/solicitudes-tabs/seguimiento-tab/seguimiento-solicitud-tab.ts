import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                       from 'ionic-angular';
import {AppDataProvider} from "../../../providers/app-data-provider";
import {Utils} from "../../../providers/utils-service";
import {UserService} from "../../../providers/user-service";

let TAG = "SeguimientoSolicitudTabPage";

@IonicPage()
@Component({
  selector: 'tab-seguimientoSolicitud',
  templateUrl: 'seguimiento-solicitud-tab.html'
})
export class SeguimientoSolicitudTabPage {

  public solicitudesList = [];
  public order     = ["-idemergencia"];
  public orderAttr = "idemergencia";
  public orderType = "down"; //'up' | 'down'

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private appSvc: AppDataProvider,
              private userSvc: UserService) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad '+TAG);
    let loader = Utils.showLoading();
    this.appSvc.getSolicitudesUser(this.userSvc.getId()).subscribe((solicitudes:[any])=>{
      this.solicitudesList = solicitudes;
      loader.dismiss();
    }, reason=>{
      loader.dismiss();
      Utils.showAlert("No se pudieron obtener las solicitudes ya que"+reason);
    })
  }

  public showDetails(solicitud){

  }


  sortByProperty (property:string) {
    if(this.orderAttr == property){
      if(this.orderType == "down"){
        this.order[0]  = property;
        this.orderType = "up";
      }
      else{
        this.order[0]  = "-"+property;
        this.orderType = "down";
      }
    }
    else{
      this.order[0]  = "-"+property;
      this.orderType = "down";
    }

    this.orderAttr = property;
  }
}
