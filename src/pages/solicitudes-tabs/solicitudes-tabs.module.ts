import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {SolicitudesTabsPage} from "./solicitudes-tabs";

@NgModule({
  declarations: [
    SolicitudesTabsPage,
  ],
  imports: [
    IonicPageModule.forChild(SolicitudesTabsPage),
  ],
  exports: [
    SolicitudesTabsPage
  ]
})
export class SolicitudesTabsPageModule {}
