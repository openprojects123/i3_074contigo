import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                       from 'ionic-angular';
import {RealizarSolicitudTabPage} from "./realizar-tab/realizar-solicitud-tab";
import {SeguimientoSolicitudTabPage} from "./seguimiento-tab/seguimiento-solicitud-tab";

let TAG = "SolicitudesTabsPage";

@IonicPage()
@Component({
  selector: 'page-solicitudes',
  templateUrl: 'solicitudes-tabs.html'
})
export class SolicitudesTabsPage {

  tab1Root: any = RealizarSolicitudTabPage;
  tab2Root: any = SeguimientoSolicitudTabPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad '+TAG);
  }

}
