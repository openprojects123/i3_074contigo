import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {TagAdminPage} from "./tag-admin";

@NgModule({
  declarations: [
    TagAdminPage,
  ],
  imports: [
    IonicPageModule.forChild(TagAdminPage),
  ],
  exports: [
    TagAdminPage
  ]
})
export class TagAdminPageModule {}
