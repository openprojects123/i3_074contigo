import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                       from 'ionic-angular';
import { Utils} from "../../../providers/utils-service";

@IonicPage()
@Component({
  selector: 'page-tag-admin',
  templateUrl: 'tag-admin.html',
})
export class TagAdminPage {

  public tag:any;
  public cars:any;
  public car:any;
  public tagList:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.tag = null;//objeto que pasare a detalles
    this.cars = [
      {tipo: "Automóvil", placa: "abc1234", anio: "2015"},
      {tipo: "Motocicleta", placa: "wxy987", anio: "2007"}
    ];

    this.car = null;

    this.tagList = [
      {id: 1, name: "MMD1 25"},
      {id: 2, name: "XDPQ 035"}
    ];
  }


  public addTag (){
    Utils.showToast("Estamos implementando esta funcionalidad");
  };

  public deleteTag (id){
    /*utilsSvc.yellowConfirm("¿Desea eliminar el TAG :<br><b>"+$scope.tagList[id-1].name+"</b>"+
            " y los datos del vehículo asociado?")
    .then(function(resp){
      if(resp){
        utilsSvc.toastShortCenter("Estamos implementando esta funcionalidad");
      }
    });*/
  };


}
