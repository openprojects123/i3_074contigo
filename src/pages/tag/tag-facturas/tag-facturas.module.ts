import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {TagFacturasPage} from "./tag-facturas";

@NgModule({
  declarations: [
    TagFacturasPage,
  ],
  imports: [
    IonicPageModule.forChild(TagFacturasPage),
  ],
  exports: [
    TagFacturasPage
  ]
})
export class TagFacturasPageModule {}
