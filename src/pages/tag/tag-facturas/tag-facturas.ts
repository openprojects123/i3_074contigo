import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                       from 'ionic-angular';
import {Utils} from "../../../providers/utils-service";

let TAG = "TagFacturasPage";

@IonicPage()
@Component({
  selector: 'page-tag-facturas',
  templateUrl: 'tag-facturas.html',
})
export class TagFacturasPage {
  public facturas:any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.facturas = [
      {id: 1, desc: "RFC: abcd050505", monto: 200, fecha: "2016-06-06", hora:"23:59:59"},
      {id: 2, desc: "RFC: xyzw050505", monto: 150, fecha: "2016-05-31", hora:"23:59:59"}
    ];
  }

  ionViewDidLoad() {
    Utils.LOG.i(TAG, "ionViewDidLoad", "TagFacturas");
  }

}
