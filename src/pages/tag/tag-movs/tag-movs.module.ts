import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {TagMovsPage} from "./tag-movs";

@NgModule({
  declarations: [
    TagMovsPage,
  ],
  imports: [
    IonicPageModule.forChild(TagMovsPage),
  ],
  exports: [
    TagMovsPage
  ]
})
export class TagMovsPageModule {}
