import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                       from 'ionic-angular';
import {Utils} from "../../../providers/utils-service";

let TAG = "TagMovs";

@IonicPage()
@Component({
  selector: 'page-tag-movs',
  templateUrl: 'tag-movs.html',
})
export class TagMovsPage {

  public movimientos:any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.movimientos = [
      {id: 1, tipo: "cruce", desc: "Caseta Tlalpan", monto: -123.5, fecha: "2016-06-06", hora:"23:56:56"},
      {id: 2, tipo: "recarga", desc: "Recarga IAVE", monto: 200, fecha: "2016-06-06", hora:"23:59:59"},
      {id: 3, tipo: "comision", desc: "Comision por recarga", monto: -8, fecha: "2016-06-06", hora:"23:59:59"}
    ];
  }

  ionViewDidLoad() {
    Utils.LOG.i(TAG, 'ionViewDidLoad', 'TagMovs');
  }

}
