import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {TagRecargasPage} from "./tag-recargas";

@NgModule({
  declarations: [
    TagRecargasPage,
  ],
  imports: [
    IonicPageModule.forChild(TagRecargasPage),
  ],
  exports: [
    TagRecargasPage
  ]
})
export class TagRecargasPageModule {}
