import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                       from 'ionic-angular';
import { Utils} from "../../../providers/utils-service";

let TAG = "TagRecargas";

@IonicPage()
@Component({
  selector: 'page-tag-recargas',
  templateUrl: 'tag-recargas.html',
})
export class TagRecargasPage {
  public userTAG: any =  [];
  public selectedTAG:any;

  public userCards:any;
  public selectedCard:any;


  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.userTAG = [
      {id: 1, name: "MMD1 25", balance: 120},
      {id: 2, name: "XDPQ 035",balance: 0}
    ];

    this.selectedTAG = this.userTAG[0].id;

    this.userCards = [
      {id: 1, type: "crédito", number: "************1234"},
      {id: 2, type: "débito", number: "************9596"}
    ];

    this.selectedCard = this.userCards[0].id;

  }

  ionViewDidLoad() {
    Utils.LOG.i(TAG, 'ionViewDidLoad',  'TagRecargas');
  }

  public doRecharge(){
    Utils.showToast("Estamos implementando esta funcionalidad");
  }

  public onTAGChange(){}

  public onCardChange(){}
}
