import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {TagSaldoPage} from "./tag-saldo";

@NgModule({
  declarations: [
    TagSaldoPage,
  ],
  imports: [
    IonicPageModule.forChild(TagSaldoPage),
  ],
  exports: [
    TagSaldoPage
  ]
})
export class TagSaldoPageModule {}
