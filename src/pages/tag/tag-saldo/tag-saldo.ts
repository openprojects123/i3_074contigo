import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                       from 'ionic-angular';
import {Utils} from "../../../providers/utils-service";

let TAG = "TagSaldo";

@IonicPage()
@Component({
  selector: 'page-tag-saldo',
  templateUrl: 'tag-saldo.html',
})
export class TagSaldoPage {

  public userTAG:any = [];
  public selectedTAG:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.userTAG = [
      {id: 1, name: "MMD1 25", balance: 120},
      {id: 2, name: "XDPQ 035",balance: 0}
    ];

    this.selectedTAG = this.userTAG[0];

  }

  ionViewDidLoad() {
    Utils.LOG.i(TAG, 'ionViewDidLoad', 'TagSaldo');
  }

}
