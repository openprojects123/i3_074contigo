import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {TagTarjetaAdminPage} from "./tag-tarjeta-admin";

@NgModule({
  declarations: [
    TagTarjetaAdminPage,
  ],
  imports: [
    IonicPageModule.forChild(TagTarjetaAdminPage),
  ],
  exports: [
    TagTarjetaAdminPage
  ]
})
export class TagTarjetaAdminPageModule {}
