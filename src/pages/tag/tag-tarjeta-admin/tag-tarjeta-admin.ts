import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                       from 'ionic-angular';
import { Utils} from "../../../providers/utils-service";

@IonicPage()
@Component({
  selector: 'page-tag-tarjeta-admin',
  templateUrl: 'tag-tarjeta-admin.html',
})
export class TagTarjetaAdminPage {

  public cardList:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.cardList = [
        {id: 1, type: "crédito", number: "************1234"},
        {id: 2, type: "débito", number: "************9596"}
      ];
  }

  public addCard (){
    Utils.showToast("Estamos implementando esta funcionalidad");
  };


}
