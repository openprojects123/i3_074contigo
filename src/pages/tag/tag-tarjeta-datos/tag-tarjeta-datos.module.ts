import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {TagtTarjetaDatosPage} from "./tag-tarjeta-datos";

@NgModule({
  declarations: [
    TagtTarjetaDatosPage,
  ],
  imports: [
    IonicPageModule.forChild(TagtTarjetaDatosPage),
  ],
  exports: [
    TagtTarjetaDatosPage
  ]
})
export class TagtTarjetaDatosPageModule {}
