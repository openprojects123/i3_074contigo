import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                       from 'ionic-angular';
import { Utils} from "../../../providers/utils-service";

@IonicPage()
@Component({
  selector: 'page-tag-tarjeta-datos',
  templateUrl: 'tag-tarjeta-datos.html',
})
export class TagtTarjetaDatosPage{

  public user:any;


  constructor(public navCtrl: NavController, public navParams: NavParams) {
      this.user = {
        rfc: "abcd990523RHMO25",
        dirCalle: "Pino",
        dirNumero: "Ocho",
        dirColonia: "Nueva Tenochtitlán",
        dirCP: "1234567",
        dirEstado: "México",
        dirPais: "México"
      };

  }

  public editFacturaData (){
    Utils.showToast("Estamos implementando esta funcionalidad");
  };


}
