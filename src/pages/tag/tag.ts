import { Component }  from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                       from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tag',
  templateUrl: 'tag.html',
})
export class TagPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Tags');
  }

  //todo: Arreglar estas jaladas
  public goTo(name){

    switch (name) {
      case 'mov':
        this.navCtrl.push("TagMovsPage");
        break;
      case 'fac':
        this.navCtrl.push("TagFacturasPage");
        break;
      case 'recTAG':
        this.navCtrl.push("TagRecargasPage");
        break;
      case 'conTAG':
        this.navCtrl.push("TagSaldoPage");
        break;
      case 'admTAG':
        this.navCtrl.push("TagAdminPage");
        break;
      case 'admTAR':
        this.navCtrl.push("TagTarjetaAdminPage");
        break;
      case 'admDF':
        this.navCtrl.push("TagtTarjetaDatosPage");
        break;
    }

  }

}
