import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {UserAccountTabsPage} from "./user-account-tabs";

@NgModule({
  declarations: [
    UserAccountTabsPage,
  ],
  imports: [
    IonicPageModule.forChild(UserAccountTabsPage),
  ],
  exports: [
    UserAccountTabsPage
  ]
})
export class UserAccountTabsPageModule {}
