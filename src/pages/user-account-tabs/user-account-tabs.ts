import {
  Component,
  ViewChild
}                     from '@angular/core';
import {
  NavController,
  Tabs,
  IonicPage
}                     from "ionic-angular";
import {UserService}  from "../../providers/user-service";
import {Utils}              from "../../providers/utils-service";

let hasLoginObserver = false;
let TAG = "UserAccountTabsPage";

@IonicPage()
@Component({
  templateUrl: 'user-account-tabs.html',
})
export class UserAccountTabsPage {
  public isLoggedIn:boolean   = false;

  @ViewChild('userAccountTabsRef') tabsRef: Tabs;

  public userAccountTabs = [];

  private loginPage = {component: "UserLoginTabPage",    title:'Sesión',    icon:'key'};
  private profilaTab = {component: "UserProfileTabPage",  title:'Mi perfil', icon:'person'};
  private registerTab = {component: "UserRegisterTabPage", title:'Registro',  icon:'person-add'};

  constructor(private userSvc:UserService, public navCtrl: NavController,) {
    this.isLoggedIn = this.userSvc.isLoggedIn();
    if(this.isLoggedIn){
      this.userAccountTabs = [this.loginPage, this.profilaTab];
    }
    else{
      this.userAccountTabs = [this.loginPage, this.registerTab];
    }
  }

  ionViewDidLoad() {
    if(this.isLoggedIn){this.tabsRef.select(1);}

    if(!hasLoginObserver){
      this.userSvc.onLoginStateChange.subscribe(isLoggedIn=>{
        this.isLoggedIn = isLoggedIn;

        if(hasLoginObserver){
          let loader = Utils.showLoading();
          this.navCtrl.setRoot("UserAccountTabsPage").then(()=>{
            loader.dismiss();
          });
        }

        hasLoginObserver = true;
      });
    }
  }
}
