import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {UserLoginTabPage} from "./user-login-tab";

@NgModule({
  declarations: [
    UserLoginTabPage,
  ],
  imports: [
    IonicPageModule.forChild(UserLoginTabPage),
  ],
  exports: [
    UserLoginTabPage
  ]
})
export class UserSessionTabPageModule {}
