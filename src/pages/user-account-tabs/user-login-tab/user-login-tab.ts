import { Component }  from '@angular/core';
import {
  NavController,
  NavParams,
  IonicPage}          from 'ionic-angular';
import {UserService}  from "../../../providers/user-service";
import {Utils}        from "../../../providers/utils-service";
import { DiagnosticService }  from '../../../providers/diagnostic-service';

let TAG = "UserLoginTabPage";

declare let CryptoJS:any;

@IonicPage()
@Component({
  selector: 'page-user-session',
  templateUrl: 'user-login-tab.html',
})
export class UserLoginTabPage {
  public isOnLine   = false;
  public isLoggedIn = false;

  public loginForm = {
    usuario: "",
    passwordField: "",
    rememberMeField: false
  };

  public utilitiesForm = {
    oldPassword: "",
    newPassword: "",
    newPasswordRepeat: "",
  };

  private networkSubsc:any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private userSvc: UserService,
              public  diagnosticSvc:  DiagnosticService
            ) {

  }

  ionViewDidLoad() {
  }

  ionViewDidEnter(){
    //console.log('ionViewDidEnter '+TAG);
    this.networkSubsc = this.diagnosticSvc.networkConnectionChange().subscribe((state)=>{
      this.isOnLine = state;
    });

    this.isLoggedIn = this.userSvc.isLoggedIn();
  }

  ionViewWillLeave(){
    this.networkSubsc.unsubscribe();
  }

  public passRecoveryPrompt(){
    if(!this.loginForm.usuario){
      Utils.showAlert("Ingrese el nombre de usuario para recuperar la contraseña");
      return;
    }

    Utils.showConfirm("Para recuperar la contraseña presione ACEPTAR", {
      okHandler: ()=>{
        let loader = Utils.showLoading();

        this.userSvc.recoveryPassword(this.loginForm.usuario).subscribe((resp)=>{
          loader.dismiss();

          Utils.showAlert("Su contraseña ha sido enviada al correo: "+resp.email);
        }, (reason)=>{
          loader.dismiss();
          Utils.showAlert("No se pudo recuperar la contraseña ya que"+reason);
        })
      }
    })
  }

  public showLogoutPrompt(){
    Utils.showConfirm("¿Realmente desea cerrar la sesión?",{
      okHandler:()=>{
        this.userSvc.logout();
        this.isLoggedIn = this.userSvc.isLoggedIn();
      }
    });
  }

  public doLogin(){
    let loader = Utils.showLoading();
    this.userSvc.login(
      this.loginForm.usuario,
      CryptoJS.MD5(this.loginForm.passwordField).toString(),
      this.loginForm.rememberMeField
    ).subscribe(resp_bool=>{
      loader.dismiss();
      this.isLoggedIn        = resp_bool;
      this.loginForm.usuario  = "";
      this.loginForm.passwordField = "";
      this.loginForm.rememberMeField = false;
    }, reason=>{
      loader.dismiss();
      Utils.showAlert("No se pudo iniciar la sesión ya que"+reason);
    })
  }

  public doChgPasswd(){
    let loader = Utils.showLoading();
    this.userSvc.changePassword(
      this.userSvc.getId(),
      CryptoJS.MD5(this.utilitiesForm.oldPassword).toString(),
      CryptoJS.MD5(this.utilitiesForm.newPassword).toString()
    ).subscribe((resp:boolean)=>{
      loader.dismiss();
      Utils.showAlert("La contraseña se actualizó correctamente", {title: "Éxito"});

      this.utilitiesForm = {
        oldPassword: "",
        newPassword: "",
        newPasswordRepeat: "",
      }
    }, reason=>{
      loader.dismiss();
      Utils.showAlert("No se pudo actualizar la contraseña ya que"+reason);
    });
  }

}
