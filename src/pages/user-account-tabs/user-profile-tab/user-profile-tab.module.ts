import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {UserProfileTabPage} from "./user-profile-tab";

@NgModule({
  declarations: [
    UserProfileTabPage,
  ],
  imports: [
    IonicPageModule.forChild(UserProfileTabPage),
  ],
  exports: [
    UserProfileTabPage
  ]
})
export class UserProfileTabPageModule {}
