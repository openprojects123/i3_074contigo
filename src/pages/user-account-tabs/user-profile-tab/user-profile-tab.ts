import { Component }    from '@angular/core';
import {
  NavController,
  NavParams,
  Events,
  IonicPage
}                       from 'ionic-angular';
import {Utils}          from "../../../providers/utils-service";
import {AppDataProvider} from "../../../providers/app-data-provider";
import {UserService}    from "../../../providers/user-service";
import { DiagnosticService }  from '../../../providers/diagnostic-service';

let TAG = "UserProfileTabPage";

@IonicPage()
@Component({
  selector: 'page-user-profile',
  templateUrl: 'user-profile-tab.html',
})
export class UserProfileTabPage {
  public isOnLine   = false;
  public isLoggedIn = false;

  private maxNumOfTels = 7;
  private extraInfo:any;

  public usuario = {
    tels: [{tel:"", ref: ""}]
  };

  public avatars = [
    {name: "boy",           selectable: true, selected: false},
    {name: "girl",          selectable: true, selected: false},
    {name: "female",        selectable: true, selected: false},
    {name: "male",          selectable: true, selected: false},
    {name: "malecostume",   selectable: true, selected: false},
    {name: "matureman1",    selectable: true, selected: false},
    {name: "supportfemale", selectable: true, selected: false},
    {name: "supportmale",   selectable: true, selected: false},
    {name: "matureman2",    selectable: true, selected: false},
    {name: "maturewoman",   selectable: true, selected: false},
    {name: "unknown",       selectable: true, selected: true},
  ];

  private networkSubsc:any;

  constructor(private events: Events,
              public navCtrl: NavController,
              public navParams: NavParams,
              private userSvc: UserService,
              private app074Svc: AppDataProvider,
              public  diagnosticSvc:  DiagnosticService
            ) {

  }

  ionViewDidEnter(){
    this.networkSubsc = this.diagnosticSvc.networkConnectionChange().subscribe((state)=>{
      this.isOnLine = state;
    });
    this.isLoggedIn = this.userSvc.isLoggedIn();
  }

  ionViewWillLeave(){
    this.networkSubsc.unsubscribe();
  }

  ionViewDidLoad() {
    Utils.LOG.i(TAG, 'ionViewDidLoad ', "ionViewDidLoad()");

    this.extraInfo    = this.userSvc.getExtraInfo();
    this.usuario.tels = this.extraInfo.telefonos;

    if(this.extraInfo.avatar){
      this.avatars.forEach((avatar)=>{
        avatar.selected = avatar.name == this.extraInfo.avatar;
      })
    }
    else{
      this.avatars[this.avatars.length-1].selected = true;
      this.extraInfo["avatar"] = this.avatars[this.avatars.length-1].name;

      this.userSvc.setExtraInfo(this.extraInfo).subscribe(()=>{}, reason=>{
        Utils.showAlert("No se puede guardar la información adicional del usuario ya que"+reason);
      });
    }
  }

  public registerTelefono(){
    if(this.usuario.tels.length >= this.maxNumOfTels){
      Utils.showAlert("Se permite un máximo de "+this.maxNumOfTels+" teléfonos.");
      return;
    }
    Utils.showPrompt({title: "Télefono", message:"Ingresa el número de télefono",
      inputs:[{
        name: 'telefono',
        type:"number",
        placeholder: 'Teléfono'
      }, {
        name: 'referencia',
        placeholder: 'Referencia'
      }],
      buttons:[
        {text: 'Cancelar'},
        {
          text: 'Guardar',
          handler: data => {
            if(this.verifyContactInfo(data))
              this.doRegisterTel(data.telefono, data.referencia);
          }
        }
      ]
    });
  }

  private doRegisterTel(numeroTel:string, refTel: string){
    let loader = Utils.showLoading();

    this.app074Svc.registerTelefono(this.userSvc.getId(), numeroTel, refTel).subscribe((data)=>{
      loader.dismiss();

      this.extraInfo = this.userSvc.getExtraInfo();

      //this.usuario.tels.push({tel:numeroTel, ref: refTel});

      this.extraInfo.telefonos.push({tel:numeroTel, ref: refTel});

      this.userSvc.setExtraInfo(this.extraInfo).subscribe(()=>{});

      Utils.showToast("Contacto agregado correctamente");
    }, reason=>{
      loader.dismiss();
      Utils.showAlert("No se pudo agregar el teléfono ya que"+reason);
    })
  }

  public updateContact(id){
    Utils.showPrompt({
      title: "Editar télefono",
      message:"Ingresa los nuevos datos",
      inputs:[{
        name: 'telefono',
        type:"number",
        value:this.usuario.tels[id].tel
      }, {
        name: 'referencia',
        value:this.usuario.tels[id].ref
      }],
      buttons:[
        {text: 'Cancelar'},
        {
          text: 'Actualizar',
          handler: newData => {
            if(newData.telefono == this.usuario.tels[id].tel && newData.referencia == this.usuario.tels[id].ref)
              Utils.showAlert("La información es la misma, no se actualizará nada");
            else if(this.verifyContactInfo(newData))
              this.doUpdateContact(id, newData);
          }
        }
      ]
    });
  }

  private doUpdateContact(telId:number, newData){
    let loader = Utils.showLoading();

    this.app074Svc.updateEmergencyContact(this.userSvc.getId(), this.usuario.tels[telId].tel, newData.telefono, newData.referencia).subscribe((respBool)=>{
      loader.dismiss();
      Utils.showToast("El teléfono se actualizó correctamente");

      //Actualizo los datos en memoria (actualizar vista)...
      this.usuario.tels[telId].tel = newData.telefono;
      this.usuario.tels[telId].ref = newData.referencia;

      //Actualizo los datos en localStorage...
      this.extraInfo = this.userSvc.getExtraInfo();
      this.extraInfo.telefonos = this.usuario.tels;
      this.userSvc.setExtraInfo(this.extraInfo).subscribe(()=>{});
    }, reason=>{
      loader.dismiss();
      Utils.showAlert("No se pudo actualizar el teléfono ya que"+reason);
    })
  }

  public deleteContact(id){
    Utils.showConfirm(
      "¿Seguro que desea eliminar el contacto "+this.usuario.tels[id].tel+"?", {
        okHandler: ()=>{
          this.doDeleteContact(id);
        }
      }
    );
  }

  private doDeleteContact(telId:number){
    let loader = Utils.showLoading();

    this.app074Svc.deleteEmergencyContact(this.userSvc.getId(), this.usuario.tels[telId].tel).subscribe((respBool)=>{
      loader.dismiss();

      //Actualizo los datos en memoria (actualizar vista)...
      this.usuario.tels.splice(telId, 1);

      //Actualizo los datos en localStorage...
      this.extraInfo = this.userSvc.getExtraInfo();
      this.extraInfo.telefonos = this.usuario.tels;
      this.userSvc.setExtraInfo(this.extraInfo).subscribe(()=>{});

      Utils.showToast("El teléfono ha sido eliminado");
    }, reason=>{
      loader.dismiss();
      Utils.showAlert("No se pudo actualizar el teléfono ya que"+reason);
    })
  }

  private verifyContactInfo(data):boolean{
    if(data.telefono.length != 10){
      Utils.showAlert("El número debe tener 10 dígitos");
      return false;
    }else if(!data.referencia.length){
      Utils.showAlert("Debes ingresar el campo Referencia");
      return false;
    }

    let yaExiste = false;
    this.usuario.tels.some((contact)=>{
      if(contact.tel == data.telefono)
        return yaExiste = true;
    });

    if(yaExiste){
      Utils.showAlert("El teléfono ya existe en sus contactos, verifique.");
      return false;
    }

    return true;
  }

  public changeAvatar(id:number){
    if(this.avatars[id].name == this.extraInfo.avatar){
      return;
    }

    Utils.showConfirm("¿Deseas actualizar el avatar?", {
      okHandler: ()=>{
        this.extraInfo.avatar = this.avatars[id].name;

        this.avatars.forEach((avatar)=>{
          avatar.selected = avatar.name == this.extraInfo.avatar;
        });

        this.userSvc.setExtraInfo(this.extraInfo).subscribe(()=>{
          Utils.showToast("Avatar actualizado");

          this.events.publish("avatar:changed", this.extraInfo);
        }, (e)=>{
          Utils.LOG.w(TAG, e, "changeAvatar()");
        });
      }
    });
  }

}
