import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {UserRegisterTabPage} from "./user-register-tab";

@NgModule({
  declarations: [
    UserRegisterTabPage,
  ],
  imports: [
    IonicPageModule.forChild(UserRegisterTabPage),
  ],
  exports: [
    UserRegisterTabPage
  ]
})
export class UserRegisterTabPageModule {}
