import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams
}                     from 'ionic-angular';
import {Utils}        from "../../../providers/utils-service";
import {UserService}  from "../../../providers/user-service";
import { DiagnosticService }  from '../../../providers/diagnostic-service';

let TAG = "UserRegisterTabPage";

declare let CryptoJS:any;

@IonicPage()
@Component({
  selector: 'page-user-register',
  templateUrl: 'user-register-tab.html',
})
export class UserRegisterTabPage {
  public isOnLine   = false;

  public registerForm = {
    nombre: "",
    paterno: "",
    materno: "",
    genero: 1,
    email:  "",
    loginField: "",
    telefono: "",
    passwordField: "",
    password2: "",
    rememberMeField: false
  };

  private networkSubsc:any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private userSvc: UserService,
              public  diagnosticSvc:  DiagnosticService
            ) {

  }

  ionViewDidEnter(){
    this.networkSubsc = this.diagnosticSvc.networkConnectionChange().subscribe((state)=>{
      this.isOnLine = state;
    });

  }

  ionViewWillLeave(){
    this.networkSubsc.unsubscribe();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistroPage');
  }

  public registerPrompt(){
    Utils.showConfirm("¿Todos los datos son correctos?",{
      okHandler:()=>{
        let loader = Utils.showLoading();
        let form = JSON.parse(JSON.stringify(this.registerForm));
        form.passwordField = CryptoJS.MD5(form.passwordField).toString();

        this.userSvc.register(form).subscribe(resp=>{

          this.userSvc.login(
            form.loginField,
            form.passwordField,
            form.rememberMeField
          ).subscribe(resp_bool=>{
            loader.dismiss();
            Utils.LOG.i(TAG, "login resp: "+resp_bool, "registerPrompt->register->authSvc.login()");
          }, reason=>{
            loader.dismiss();
            Utils.showAlert("El registro se completó pero no se pudo iniciar la sesión ya que"+reason);
          })
        }, reason=>{
          loader.dismiss();
          Utils.showAlert("No se pudo realizar el registro ya que"+reason);
        })
      }
    })
  }

}
