import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  MenuController
} from 'ionic-angular';
import { SplashScreen }     from '@ionic-native/splash-screen';
import {Deploy}             from '@ionic/cloud-angular';
import {GLOBALS}            from "../../app/globals";
import {UserService}        from "../../providers/user-service";
import {LocalDbSvc}         from "../../providers/localDb-service";
import {DiagnosticService}  from "../../providers/diagnostic-service";

import {Utils}              from "../../providers/utils-service";
import {Storage}            from "@ionic/storage";

let TAG = "WarmupPage";

@IonicPage()
@Component({
  selector: 'page-warmup',
  templateUrl: 'warmup.html',
})
export class WarmupPage {
  public runsOnDevice:boolean;
  public isOnLine = false;

  //Posibles estados: initState->unknownState->okState|errorState
  public dbState     =  "initState";
  public wifiState   =  "initState";
  public gpsState    =  "initState";
  public updateState =  "initState";
  public sessionState  =  "initState";

  public checkingModule:string; //{DB, WIFI, GPS, UPDATES, SESSION}
  private delay = 400;

  public dbMessages:any;
  public dbStateClass = this.dbState;

  public wifiMessages:any;
  public wifiStateClass    = this.wifiState;
  public showEnableWifiBtn = true;
  public readyToEnableWifi = false;

  public gpsMessages:any;
  public gpsStateClass    = this.gpsState;
  public showEnableGpsBtn = true;
  public readyToEnableGps = false;

  public updatesMessages:any;
  public updateStateClass       = this.updateState;
  public readyToCheckForUpdates = false;
  public readyToUpdate          = false;

  public sessionMessages:any;
  public sessionStateClass = this.sessionState;

  private networkSubsc:any;
  private gpsSubsc:any;

  constructor(private splashScreen: SplashScreen,
              public navCtrl: NavController,
              public navParams: NavParams,
              private menuCtrl: MenuController,
              private DB: LocalDbSvc,
              private userSvc: UserService,
              private storage: Storage,
              private diagnostic: DiagnosticService,
              public deploy: Deploy) {


    //let loader = Utils.showLoading();
    Utils.platform.ready().then(()=>{
      //loader.dismiss();

      setTimeout(()=>{
        this.loadLocalDB();
      }, this.delay);

      /*this.diagnostic.isLocationEnabled().subscribe(gpsEnabled=>{

      }, reason=>{
        Utils.showAlert("No se pudo verificar el estado del GPS ya que"+reason);
      })*/

    });

    this.runsOnDevice = Utils.runsOnDevice();

  }

  ngAfterContentInit(){
    this.splashScreen.hide();
  }

  ionViewDidLoad() {
    Utils.LOG.i(TAG, 'ionViewDidLoad WarmupPage', "ionViewDidLoad()");
    this.menuCtrl.enable(false);

    this.networkSubsc = this.diagnostic.networkConnectionChange().subscribe(isOnLine=>{
      this.isOnLine = isOnLine;
      if(this.wifiState != 'initState'){
        this.wifiStateClass = this.wifiState = this.isOnLine ? 'okState':'errorState';
        if(this.checkingModule == "WIFI" && this.isOnLine){
          this.checkLocationState();
        }
      }

      if(isOnLine && this.readyToCheckForUpdates && this.checkingModule =="UPDATES"){
        this.checkForUpdates();
      }
    });
  }

  ionViewWillLeave(){
    this.menuCtrl.enable(true);
    this.networkSubsc.unsubscribe();
    if(this.gpsSubsc){
      this.gpsSubsc.unsubscribe();
      this.diagnostic.gpsStopWatching();
    }
  }

  /*private initializeApp(){
    if(Utils.runsOnDevice())
      this.loadLocalDB();
    else{
      this.checkingModule = "DB";
      this.dbState = 'unknownState';
      setTimeout(()=>{
        this.dbState = 'errorState';
        this.checkNetworkState();
      }, this.delay);
    }
  }*/

  private loadLocalDB(){
    this.checkingModule = "DB";
    this.dbStateClass = this.dbState = 'unknownState';

    this.DB.loadDB().subscribe(res=>{
      Utils.LOG.i(TAG, "Base de datos cargada: "+res, "loadDB()");
      this.dbStateClass = this.dbState = 'okState';
      /*this.DB.testDB().subscribe(tablas=>{
       Utils.LOG.i(TAG, "TestDB: "+JSON.stringify(tablas), "openDB->testDB()")
       });*/
      setTimeout(()=>{this.checkNetworkState();}, this.delay)
    },error=>{
      this.dbStateClass = this.dbState = 'errorState';
      this.dbMessages = Utils.assembleHTMLItem("No se pudo cargar la base de datos ya que"+error);
    });
  }

  private checkNetworkState(){
    this.checkingModule = "WIFI";
    this.wifiStateClass = this.wifiState = 'unknownState';

    if(!this.diagnostic.isOnLine()){
      this.wifiStateClass = this.wifiState = 'errorState';

      if(!Utils.runsOnDevice()){
        this.wifiMessages = "La aplicación requiere de una conexión a internet para funcionar correctamente";
        return;
      }

      this.diagnostic.isWiFiEnabled().subscribe(isEnabled=>{
        if(isEnabled){
          this.wifiStateClass = this.wifiState = 'okState';
          setTimeout(()=>{this.checkLocationState();}, this.delay);
          return;
        }

        if(Utils.platform.is("android")){
          this.wifiMessages = "El Wi-Fi no está habilitado ¿Desea habilitarlo ahora?";
          this.readyToEnableWifi = true;
        }
        else{//Es iOS
          Utils.showToast("El WiFi no está habilitado",{position:"top", duration:5*GLOBALS.ONE_SECOND});
        }
      }, error=>{
        this.wifiStateClass = this.wifiState = 'errorState';
        this.wifiMessages = Utils.assembleHTMLItem("No se pudo determinar el estado del WiFi ya que"+error);
        this.showEnableWifiBtn = false;
      });
    }
    else{
      this.wifiStateClass = this.wifiState = 'okState';
      setTimeout(()=>{this.checkLocationState();}, this.delay);
    }
  }
  public didntWantEnableWiFi(){
    if(!this.showEnableWifiBtn){
      this.checkLocationState();
      return;
    }

    Utils.showAlert("La aplicación requiere de una conexión a internet para funcionar correctamente",{
      enableBackdropDismiss:false,
      okText:"Entendido",
      okHandler:()=>{
        setTimeout(()=>{this.checkLocationState();}, this.delay);
      }
    });
  }
  public switchToWifiSettings(){
    this.diagnostic.switchToWifiSettings();
    this.checkLocationState();
  }

  private checkLocationState(){
    this.checkingModule = "GPS";
    this.gpsStateClass = this.gpsState = 'unknownState';

    this.diagnostic.isLocationEnabled().subscribe(isEnabled=>{
      this.gpsSubsc = this.diagnostic.gpsConnectionChange().subscribe((enabled)=>{
        if(this.gpsState != 'initState'){
          this.gpsStateClass = this.gpsState = (enabled ? 'okState':'errorState');
          if(this.checkingModule == "GPS" && enabled){
            this.askForUpdates();
          }
        }
      });

      if(isEnabled){
        this.gpsStateClass = this.gpsState = 'okState';
        setTimeout(()=>{this.askForUpdates();}, this.delay);
        return;
      }

      this.gpsStateClass = this.gpsState = 'errorState';
      if(Utils.platform.is("android")){
        this.gpsMessages = "La geolocalización no está habilitada ¿Desea habilitarla ahora?";
        this.readyToEnableGps = true;
      }
      else{
        Utils.showAlert("Algunos módulos de la aplicación necesitan usar la geolocalización.",{
          enableBackdropDismiss:false,
          okText:"Entendido",
          okHandler:()=>{
            setTimeout(()=>{this.askForUpdates();}, this.delay);
          }
        });
      }
    }, error=>{
      this.gpsStateClass = this.gpsState = 'errorState';
      this.gpsMessages = Utils.assembleHTMLItem("No se pudo determinar el estado de la geolocalización ya que"+error);
      this.showEnableGpsBtn = false;
    });
  }
  public didntWantEnableGps(){
    if(!this.showEnableGpsBtn){
      this.askForUpdates();
      return;
    }
    Utils.showAlert("La aplicación requiere que habilite la geolocalización para funcionar correctamente",{
      enableBackdropDismiss:false,
      okText:"Entendido",
      okHandler:()=>{
        this.askForUpdates();
      }
    });
  }
  public switchToLocationSettings(){
    this.diagnostic.switchToLocationSettings();
    this.cleanUpdates();
  }

  private askForUpdates(){
    this.checkingModule = "UPDATES";
    this.updateStateClass = this.updateState = 'unknownState';

    if(!this.runsOnDevice){
      this.updateStateClass = this.updateState = 'okState';
      this.checkSessionStatus();
      return
    }

    this.storage.get("mustCheckForUpdates").then((check)=>{
      if(check === false)
        this.checkSessionStatus();
      else{
        this.readyToCheckForUpdates = true;
        this.updatesMessages = "¿Desea buscar actualizaciones ahora?";
      }
    }, error=>{
      Utils.LOG.w(TAG,
        "No se pudo obtener el valor de mustCheckForUpdates ya que: "+
        error, "askForUpdates->storage.get(mustCheckForUpdates)"
      );

      this.readyToCheckForUpdates = true;
      this.updatesMessages = "¿Desea buscar actualizaciones ahora?";
    });
  }

  public neverAskForUpdates(){
    this.storage.set("mustCheckForUpdates", false).then(()=>{
      this.checkSessionStatus();
    })
  }

  private cleanUpdates(){

    if(!Utils.runsOnDevice()){
      this.updateStateClass = this.updateState = 'okState';
      this.updatesMessages  = "La aplicación ya está actualizada";
      this.readyToCheckForUpdates = false;
      this.readyToUpdate = false;
      return;
    }

    this.deploy.info().then(currentSnapshot=>{
      //Utils.LOG.i(TAG, "currentSnapshot: "+JSON.stringify(currentSnapshot), "checkForUpdates->info()");
      this.deploy.getSnapshots().then(snapShotsArray=>{
        //Utils.LOG.i(TAG, JSON.stringify(snapShotsArray), "checkForUpdates->getSnapshots()");
        snapShotsArray.forEach(s=>{
          if(currentSnapshot && currentSnapshot.deploy_uuid != s)
            this.deploy.deleteSnapshot(s).then(()=>{
              Utils.LOG.i(TAG, "El snapshot "+s+" Ha sido borrado!", "checkForUpdates->...->deploy.deleteSnapshot()");
            });
        });
        this.checkForUpdates();
      },reason=>{
        this.checkForUpdates();
      });
    }, reason =>{
      this.updateStateClass = this.updateState = 'errorState';
      Utils.showAlert("No se puede determinar si la app está actualizada ya que"+
               Utils.LOG.w(TAG, reason, "checkForUpdates->...->deploy.info()"),{
        enableBackdropDismiss:false,
        okText:"Entendido",
        okHandler:()=>{this.checkSessionStatus();}
      });
    });
  }

  private checkForUpdates(){
    this.readyToUpdate = false;

    if(!this.isOnLine){
      this.readyToCheckForUpdates = true;
      this.updateStateClass = this.updateState = 'unknownState';
      this.updatesMessages = "No se puede determinar si hay actualizaciones ya que el internet no está habilitado";
      return;
    }

    let loader = Utils.showLoading("Buscando actualizaciones");
    this.deploy.check().then((snapshotAvailable: boolean) => {
      loader.dismiss();
      if (snapshotAvailable) {
        this.updatesMessages = "Hay una actualización disponible ¿Desea instalarla ahora?";
        this.readyToUpdate = true;
      }
      else{
        this.updateStateClass = this.updateState = 'okState';
        this.checkSessionStatus()
      }
    }, reason=>{
      loader.dismiss();
      this.updateStateClass = this.updateState = 'errorState';
      this.updatesMessages = Utils.assembleHTMLItem(
        "No se pudo determinar si hay actualizaciones ya que"+
        Utils.LOG.w(TAG, reason, "checkForUpdates->deploy.check()")
      );
      this.checkSessionStatus();
    });
  }
  public downloadUpdates(){
    let loader = Utils.showLoading("Descargando...");
    this.deploy.download().then(() => {
      loader.dismiss();

      loader = Utils.showLoading("Instalando...");
      this.deploy.extract().then(()=>{
        loader.dismiss();
        this.updateStateClass = this.updateState = 'okState';
        this.deploy.load();
      }, reason=>{
        this.updateStateClass = this.updateState = 'errorState';
        Utils.showAlert("No se pudo extraer la actualización ya que"+
          Utils.LOG.w(TAG, reason, "checkForUpdates->deploy.extract()"),{
          enableBackdropDismiss:false,
          okText:"Entendido",
          okHandler:()=>{
            setTimeout(()=>{this.checkSessionStatus();}, this.delay);
          }
        });
      });
    }, reason=>{
      loader.dismiss();
      this.updateStateClass = this.updateState = 'errorState';
      Utils.showAlert("No se pudo descargar la actualización ya que"+
        Utils.LOG.w(TAG, reason, "checkForUpdates->deploy.extract()"),{
        enableBackdropDismiss:false,
        okText:"Entendido",
        okHandler:()=>{
          setTimeout(()=>{this.checkSessionStatus();}, this.delay);
        }
      });
    });
  }

  private checkSessionStatus(){
    this.checkingModule = "SESSION";
    this.sessionStateClass = this.sessionState = 'unknownState';

    if(this.userSvc.isLoggedIn() && !this.userSvc.mustRemember()){
      this.userSvc.logout();
    }

    if(!this.userSvc.isLoggedIn()){
      this.sessionStateClass = this.sessionState = 'errorState';
      this.sessionMessages = Utils.assembleHTMLItem(
         "No has iniciado sesión pero aún así podrás hacer uso de algunos de nuestros servicios."+
        "<br>Regístrate o inicia sesión para poder usar todas los servicios."
      );
    }
    else{
      this.sessionStateClass = this.sessionState = 'okState';
      this.sessionMessages = "Enviando a la página principal";
      setTimeout(()=>{
        this.goToMainPage();
      }, 2000)
    }
  }
  public goToMainPage(){
    let loader = Utils.showLoading();
    this.navCtrl.setRoot(
      this.userSvc.isLoggedIn() ? "InicioTabsPage" :"UserAccountTabsPage").then(()=>{
      loader.dismiss();
    });
  }
}
