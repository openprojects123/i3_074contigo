import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {Utils} from "./utils-service";
import {Observable} from "rxjs";
import { Storage } from '@ionic/storage';
import {GLOBALS} from "../app/globals";
import {obraType, incidenciaType, catTramosType, catPOIType} from "./localDb-service";
import {latlngType} from "./locationAndMap/geolocation-service";
import { TextToSpeech } from '@ionic-native/text-to-speech';

declare let X2JS:any;

let TAG = "AppDataProvider";

@Injectable()
export class AppDataProvider {

  public avatarName:string;

  constructor(private storage: Storage,
              private tts: TextToSpeech) {
  }

  public getAlertas():Observable<[{estatus, fecha, idAlerta, mensaje, titulo}]>{
    return Observable.create(observer=>{
      Utils.httpGet(GLOBALS.API_URL + "alertas").subscribe((API:APIType)=>{
        if(!API.result){
          return observer.error(Utils.LOG.e(TAG, API, "getAlertas(API.result==false)"));
        }
        else if(!API.data){
          return observer.error(Utils.LOG.e(TAG, "El servidor no devolvió datos", "getAlertas(API.result==true)"));
        }

        observer.next(API.data);
        observer.complete();
      }, httpError =>{
        observer.error(Utils.LOG.w(TAG, httpError, "getAlertas()"))
      })
    });
  }

  public getIncidenciasFromServer():Observable<Array<incidenciaType>>{
    return Observable.create(observer=>{
      Utils.httpGet(GLOBALS.API_URL + "incidentes").subscribe((API:APIType)=>{
        if(!API.result){
          return observer.error(Utils.LOG.e(TAG, API, "getIncidenciasFromServer(API.result==false)"));
        }
        else if(!API.data){
          return observer.error(TAG, "El servidor no devolvió datos", "getIncidenciasFromServer(API.result==true)");
        }
        else{
          let x2js = new X2JS();
          let resp = x2js.xml_str2json(API.data);

          let incArray:Array<incidenciaType> = [];
          resp.markers.marker.forEach((mark)=>{
            incArray.push({
              id:           mark._idReporte,
              seguimiento:  mark._seguimiento,
              idtipo:       mark._idTipo,
              idsubtipo:    mark._idSubTipo,
              fhreporte:    mark._fhReporte,
              idautopista:  mark._idAutopista,
              km:           mark._km,
              mt:           mark._mt,
              tipocierre:   mark._tipoCierre,
              ficha:        mark._ficha,
              lat:          parseFloat(mark._lat),
              lng:          parseFloat(mark._lng)
            });
          });

          observer.next(incArray);
          observer.complete();
        }
      }, httpError =>{
        observer.error(Utils.LOG.w(TAG, httpError, "getIncidenciasFromServer()"))
      })
    });
  }

  public getObrasFromServer(tramosString:string):Observable<Array<obraType>>{
    return Observable.create(observer=>{

      if(tramosString == ""){
        return observer.error(Utils.LOG.w(TAG, "La cadena con ids está vacia", "getObrasFromServer()"));
      }
      //Utils.LOG.i(TAG, "------* idsTramos: "+tramosString, "getObrasFromServer()");

      Utils.httpPost(
        GLOBALS.API2_URL+"obras",
        {idsTramosString: tramosString}).subscribe((API:APIType)=>{
        if(!API.result){
          return observer.error(Utils.LOG.e(TAG, API, "getObrasFromServer(API.result==false)"));
        }
        else if(!API.data){
          return observer.error(Utils.LOG.e(TAG, "El servidor no devolvió datos", "getObrasFromServer(API.result==true)"));
        }
        else{
          observer.next(API.data);
          observer.complete();
        }
      }, httpError =>{
        observer.error(Utils.LOG.w(TAG, JSON.stringify(httpError), "getObrasFromServer()"))
      })
    });
  }

  public getKilometrosObras(obra:obraType):Observable<Array<latlngType>>{
    return Observable.create(observer=>{
      Utils.httpGet(
        GLOBALS.API2_URL+"kilometros/inicio/"+
        obra.kmInicial+"/fin/"+obra.kmFinal+"/tramo/"+obra.idTramo
      ).subscribe(resp=>{
        let coordsArray:Array<latlngType> = [];
        //Utils.LOG.i(TAG, "-------* Coordenadas: "+JSON.stringify(resp.data), "showObrasOnTramos()");
        resp.data.forEach((coord)=>{
          coordsArray.push({lat: parseFloat(coord.latitud), lng: parseFloat(coord.longitud)})
        });

        observer.next(coordsArray);
        observer.complete();
      }, httpError=>{
        observer.error(Utils.LOG.e(TAG, httpError, "getKilometrosObras->httpGet()"));
      });
    });
  }

  public getTramosFromServer():Observable<any>{
    return Observable.create(observer=>{
      Utils.httpGet(GLOBALS.API2_URL+"tramos").subscribe((API:APIType)=>{
        if(!API.result){
          return observer.error(Utils.LOG.e(TAG, API, "getTramosFromServer(API.result==false)"));
        }
        else if(!API.data){
          return observer.error(Utils.LOG.e(TAG, "El servidor no devolvió datos", "getTramosFromServer(API.result==true)"));
        }
        else{
          observer.next(API.data);
          observer.complete();
        }
      }, httpError =>{
        observer.error(Utils.LOG.w(TAG, JSON.stringify(httpError), "getTramosFromServer()"))
      })
    })
  }

  public getPOIFromServer(idsTramosString:string):Observable<any>{
    return Observable.create(observer=>{
      if(idsTramosString == ""){
        return observer.error(Utils.LOG.w(TAG, "La cadena con ids está vacia", "getPOIFromServer()"));
      }
      //Utils.LOG.i(TAG, "------* idsTramosString: "+idsTramosString, "getPOIFromServer()");

      Utils.httpPost(
        GLOBALS.API2_URL+"POIbyTramos",
        {idsTramosString: idsTramosString}
      ).subscribe((API:APIType)=>{
        if(!API.result){
          return observer.error(Utils.LOG.e(TAG, API, "getPOIFromServer(API.result==false)"));
        }
        else if(!API.data){
          return observer.error(Utils.LOG.e(TAG, "El servidor no devolvió datos", "getPOIFromServer(API.result==true)"));
        }
        else{
          observer.next(API.data);
          observer.complete();
        }
      }, httpError =>{
        observer.error(Utils.LOG.w(TAG, JSON.stringify(httpError), "getPOIFromServer()"))
      })
    })
  }

  public getTramosCoordinatesFromServer(idsTramosString:string):Observable<Array<obraType>>{
    return Observable.create(observer=>{

      if(idsTramosString == ""){
        return observer.error(Utils.LOG.w(TAG, "La cadena con ids está vacia", "getObrasFromServer()"));
      }
      //Utils.LOG.i(TAG, "------* idsTramosString: "+idsTramosString, "getObrasFromServer()");

      Utils.httpPost(
        GLOBALS.API2_URL+"tramos/coordinates",
        {idsTramosString: idsTramosString}).subscribe((API:APIType)=>{
        if(!API.result){
          return observer.error(Utils.LOG.e(TAG, API, "getTramosCoordinatesFromServer(API.result==false)"));
        }
        else if(!API.data){
          return observer.error(Utils.LOG.e(TAG, "El servidor no devolvió datos", "getTramosCoordinatesFromServer(API.result==true)"));
        }
        else{
          observer.next(API.data);
          observer.complete();
        }
      }, httpError =>{
        observer.error(Utils.LOG.w(TAG, JSON.stringify(httpError), "getTramosCoordinatesFromServer()"))
      })
    });
  }

  public getTramosFromStorage():Observable<any>{
    return Observable.create(observer=>{
      this.storage.get("catTramos").then(tramosStr=>{

        if(tramosStr){
          let tramosCat = JSON.parse(tramosStr);
          observer.next(tramosCat);
          observer.complete();
        }
        else{
          observer.next(null);
          observer.complete();
        }
      }, storageError=>{
        observer.error(Utils.LOG.e(TAG, storageError, "getTramos()"));
      });
    })
  }

  public persistCatTramos(catTramos:catTramosType):Promise<any>{
    return this.storage.set("catTramos", JSON.stringify(catTramos));
  }

  public persistCatPOI(catPOI:catPOIType):Promise<any>{
    return this.storage.set("catPOI", JSON.stringify(catPOI));
  }

  public getCatEmergencias():Observable<[{idtipoemergencia, nombre}]>{
    return Observable.create(observer=>{
      Utils.httpGet(GLOBALS.API_URL + "catEmergencias").subscribe((API:APIType)=>{
        if(!API.result){
          return observer.error(Utils.LOG.e(TAG, API, "getCatEmergencias(API.result==false)"));
        }
        else if(!API.data){
          return observer.error(Utils.LOG.e(TAG, "El servidor no devolvió datos", "getCatEmergencias(API.result==true)"));
        }

        observer.next(API.data);
        observer.complete();
      }, httpError =>{
        observer.error(Utils.LOG.w(TAG, httpError, "getCatEmergencias()"))
      })
    });
  }

  public sendSolicitud(formData):Observable<{id:number}>{
    return Observable.create(observer=>{
      Utils.httpPost(GLOBALS.API_URL + "request/emergencia", formData).subscribe((API:APIType)=>{
        if(!API.result){
          return observer.error(Utils.LOG.e(TAG, API, "sendSolicitud()"));
        }
        else if(!API.data){
          return observer.error(Utils.LOG.e(TAG, "El servidor no devolvió datos", "sendSolicitud()"));
        }
        //Utils.LOG.i(TAG, "Tipo respuesta: "+JSON.stringify(API.data), "sendSolicitud()");
        observer.next(API.data);
        observer.complete();
      }, httpError =>{
        observer.error(Utils.LOG.w(TAG, httpError, "sendSolicitud()"))
      })
    })
  }

  public getCatTipoReporte():Observable<[catTipoReporteType]>{
    return Observable.create(observer=>{
      Utils.httpGet(GLOBALS.API_URL + "catTipoReporte").subscribe((API:APIType)=>{
        if(!API.result){
          return observer.error(Utils.LOG.e(TAG, API, "getCatTipoReporte()"));
        }
        else if(!API.data){
          return observer.error(Utils.LOG.e(TAG, "El servidor no devolvió datos", "getCatTipoReporte()"));
        }
        observer.next(API.data);
        observer.complete();
      }, httpError =>{
        observer.error(Utils.LOG.w(TAG, httpError, "getCatTipoReporte()"))
      })
    })
  }

  public getCatSubtipoReporte(idTipo:number):Observable<[catsubTipoReporteType]>{
    return Observable.create(observer=>{
      if(!idTipo){
        return observer.error(Utils.LOG.e(TAG, "El id del tipo de reporte es cero o nulo", "getCatTipoReporte()"));
      }

      Utils.httpGet(GLOBALS.API_URL + "catSubtipoReporte/idTipo/"+idTipo).subscribe((API:APIType)=>{
        if(!API.result){
          return observer.error(Utils.LOG.e(TAG, API, "getCatTipoReporte()"));
        }
        else if(!API.data){
          return observer.error(Utils.LOG.e(TAG, "El servidor no devolvió datos", "getCatTipoReporte()"));
        }
        observer.next(API.data);
        observer.complete();
      }, httpError =>{
        observer.error(Utils.LOG.w(TAG, httpError, "getCatTipoReporte()"))
      })
    })
  }

  public newReporte(form:{idsubtiporeporte:number, idusuario:number, notas:string}):Observable<{id:number}>{
    return Observable.create(observer=>{
      if(!form.idsubtiporeporte)
        return observer.error(Utils.LOG.e(TAG, "El id del tipo de subtipo es cero o nulo", "newReporte()"));
      else if(!form.idusuario)
        return observer.error(Utils.LOG.e(TAG, "El id del tipo de usuario es cero o nulo", "newReporte()"));

      Utils.httpPost(GLOBALS.API_URL + "reporte", form).subscribe((API:APIType)=>{
        if(!API.result){
          return observer.error(Utils.LOG.e(TAG, API, "getCatTipoReporte()"));
        }
        else if(!API.data){
          return observer.error(Utils.LOG.e(TAG, "El servidor no devolvió datos", "newReporte()"));
        }
        observer.next(API.data);
        observer.complete();
      }, httpError =>{
        observer.error(Utils.LOG.w(TAG, httpError, "newReporte()"))
      })
    })
  }

  /**
   *
   * @param userId
   * @returns [{
   * autopista:string,
   * emergencia:string,
   * fhatencion:string,
   * fhconclusion:string,
   * fhregistro:string,
   * idautopista:number,
   * idemergencia:number,
   * idtipoemergencia:number,
   * km:number,
   * status:number
   * }]
   */
  public getSolicitudesUser(userId:number):Observable<[any]>{
    return Observable.create(observer=>{
      Utils.httpGet(GLOBALS.API_URL + "solicitudes/user/"+userId).subscribe((API:APIType)=>{
        if(!API.result){
          return observer.error(Utils.LOG.e(TAG, API, "getSolicitudesUser()"));
        }
        else if(!API.data){
          return observer.error(Utils.LOG.e(TAG, "El servidor no devolvió datos", "getSolicitudesUser()"));
        }

        observer.next(API.data);
        observer.complete();
      }, httpError =>{
        observer.error(Utils.LOG.w(TAG, httpError, "getSolicitudesUser()"))
      })
    })
  }

  public getReportesUser(userId:number):Observable<[any]>{
    return Observable.create(observer=>{
      Utils.httpGet(GLOBALS.API_URL + "reportes/user/"+userId).subscribe((API:APIType)=>{
        if(!API.result){
          return observer.error(Utils.LOG.e(TAG, API, "getReportesUser()"));
        }
        else if(!API.data){
          return observer.error(Utils.LOG.e(TAG, "El servidor no devolvió datos", "getReportesUser()"));
        }

        observer.next(API.data);
        observer.complete();
      }, httpError =>{
        observer.error(Utils.LOG.w(TAG, httpError, "getReportesUser()"))
      })
    })
  }

  public registerTelefono(userId: number, numeroTel:string, refTel: string):Observable<any>{
    let form = {
      uid: userId,
      tel: numeroTel,
      ref: refTel
    };

    return Observable.create(observer=>{
      Utils.httpPost(GLOBALS.API_URL+"usuario/contacto", form).subscribe((API:APIType)=>{
        if(!API.result){
          return observer.error(Utils.LOG.e(TAG, API, "registerTelefono()"));
        }
        else if(!API.data){
          return observer.error(Utils.LOG.e(TAG, "El servidor no devolvió datos", "registerTelefono()"));
        }

        observer.next(API.data);
        observer.complete();
      }, httpError=>{
        observer.error(Utils.LOG.w(TAG, httpError, "registerTelefono()"))
      })
    })
  }

  public updateEmergencyContact(usuId:number, oldTelefono: string, newTel:string, refTel: string):Observable<any>{
    let form = {
      uid:    usuId,
      oldTel: oldTelefono,
      newTel: newTel,
      newRef: refTel
    };

    return Observable.create(observer=>{
      Utils.httpPut(GLOBALS.API_URL+"usuario/contacto", form).subscribe((API:APIType)=>{
        if(!API.result){
          return observer.error(Utils.LOG.e(TAG, API, "updateEmergencyContact()"));
        }
        else if(!API.data){
          return observer.error(Utils.LOG.e(TAG, "El servidor no devolvió datos", "updateEmergencyContact()"));
        }

        observer.next(API.data);
        observer.complete();
      }, httpError=>{
        observer.error(Utils.LOG.w(TAG, httpError, "updateEmergencyContact()"))
      })
    })
  }

  public deleteEmergencyContact(usuId:number, tel:string):Observable<any>{
    return Observable.create(observer=>{
      Utils.httpDelete(GLOBALS.API_URL+"usuario/contacto/uid/"+usuId+"/tel/"+tel).subscribe((API:APIType)=>{
        if(!API.result){
          return observer.error(Utils.LOG.e(TAG, API, "deleteEmergencyContact()"));
        }
        else if(!API.data){
          return observer.error(Utils.LOG.e(TAG, "El servidor no devolvió datos", "deleteEmergencyContact()"));
        }

        observer.next(API.data);
        observer.complete();
      }, httpError=>{
        observer.error(Utils.LOG.w(TAG, httpError, "deleteEmergencyContact()"))
      })
    })
  }

  public avatarHasChanged(newName:string){
    this.avatarName = newName;
  }

  //speech
  public speech(msg):Observable<boolean>{
    return Observable.create(observer=>{
      if(typeof this.tts != 'undefined'){
        this.tts.speak({
          text: msg,
          locale: 'es-MX',
          rate: Utils.platform.is("android") ? 1.4 : 1.7
        }).then(()=> {
          observer.next(true);
        }).catch(reason => {
          observer.error(Utils.LOG.w(TAG, "ERROR en TextToSpeech: " + reason, "speech()"));
        });
      }
      else{
        observer.error(Utils.LOG.w(TAG, "TextToSpeech no está definido", "speech()"));
      }
    })
  }

  public getSettingsGame():Observable<any>{
    return Observable.create(observer=>{

      let API:[{pregunta:string,respuesta:[{opc:any,value:boolean}],pts:number,estado:boolean,numero:number}];
      Utils.httpGet("assets/vendor/game.json").subscribe((API1:any)=>{
        API = API1;

        for (let i = 0; i < API.length; i++) {
        let p1 = Math.floor(Math.random() * (3 - 0) + 0);
        let p2 = Math.floor(Math.random() * (7 - 4) + 4);

        let obj1 = API[p1];
        let obj2 = API[p2];

        let aux  = obj1;
        API[p1]  = obj2;
        API[p2]  = aux;
      }

      API[0].estado = true;
      for (let i = 0; i < API.length; i++) {
        API[i].numero = i;
        for (let y = 0; y < 4; y++) {
          let p1 = Math.floor(Math.random() * (2 - 0) + 0);
          let p2 = Math.floor(Math.random() * (4 - 2) + 2);

          let obj1_preg = API[i].respuesta[p1];
          let obj2_preg = API[i].respuesta[p2];

          let aux               = obj1_preg;
          API[i].respuesta[p1]  = obj2_preg;
          API[i].respuesta[p2]  = aux;
        }
      }

      for (let i = 0; i < API.length; i++) {
        for (let y = 0; y < 4; y++) {
          if(y==0){
            API[i].respuesta[y].opc = "A) "+API[i].respuesta[y].opc;
          }
          if(y==1){
            API[i].respuesta[y].opc = "B) "+API[i].respuesta[y].opc;
          }
          if(y==2){
            API[i].respuesta[y].opc = "C) "+API[i].respuesta[y].opc;
          }
          if(y==3){
            API[i].respuesta[y].opc = "D) "+API[i].respuesta[y].opc;
          }
          
        }
      }

        observer.next(API);
        observer.complete();
      }, httpError =>{
        observer.error(Utils.LOG.w(TAG, httpError, "getReportesUser()"))
      })
       /*= 
      [
        {
          pregunta:"¿Qué significa CAPUFE?",
          respuesta:[ 
            {opc:"Camiones, Puertas y Federales", value:false},
            {opc:"Canastas y Puentes Federales",  value:false},
            {opc:"Caminos y Puentes Federales",   value:true},
            {opc:"Ninguna es correcta",           value:false} ],
          pts:1,
          estado:false,
          numero:1
        },
        {
          pregunta:"¿Por dónde puedo rebasar a otro vehículo?",
          respuesta:[ 
            {opc:"Derecha",             value:false},
            {opc:"Izquierda",           value:true},
            {opc:"Acotamiento",         value:false},
            {opc:"Todas son correctas", value:false} ],
          pts:2,
          estado:false,
          numero:2
        },
        {
          pregunta:"Respetar los límites de velocidad es importante para…",
          respuesta:[ 
            {opc:"Prevenir accidentes",         value:true },
            {opc:"Llegar a tiempo",             value:false},
            {opc:"No gastar mucho combustible", value:false},
            {opc:"No gastar mucho las llantas", value:false} ],
          pts:3,
          estado:false,
          numero:3
        },
        {
          pregunta:"El acotamiento sirve para… ",
          respuesta:[ 
            {opc:"Puestos ambulantes",                                                                    value:false},
            {opc:"Rebasar cuando tengo prisa",                                                            value:false},
            {opc:"Paso de unidades de emergencia y cuando <br> necesito detenerme por alguna emergencia", value:true },
            {opc:"Todas son correctas",                                                                   value:false} ],
          pts:4,
          estado:false,
          numero:4
        },
        {
          pregunta:"¿Qué debo hacer si veo personal de CAPUFE bandereando (ondeando una bandera preventiva)?",
          respuesta:[ 
            {opc:"Saludarlo",                                                  value:false},
            {opc:"Disminuir mi velocidad y estar <br> atento a indicaciones",  value:true },
            {opc:"Aumentar la velocidad",                                      value:false},
            {opc:"Frenar de golpe",                                            value:false} ],
          pts:5,
          estado:false,
          numero:5
        },
        {
          pregunta:"¿Cuál es el número de asistencia/emergencia de CAPUFE?",
          respuesta:[ 
            {opc:"066", value:false},
            {opc:"072", value:false},
            {opc:"089", value:false},
            {opc:"074", value:true } ],
          pts:4,
          estado:false,
          numero:6
        },
        {
          pregunta:"¿Quién es el Director General de CAPUFE?",
          respuesta:[ 
            {opc:"Eduardo Verástegui", value:false},
            {opc:"Enrique Peña Nieto", value:false},
            {opc:"Benito Neme Sastré", value:true },
            {opc:"Barack Obama",       value:false} ],
          pts:5,
          estado:false,
          numero:7
        },
      ];*/
    })
  }

}

export interface APIType {
  result:boolean, data:[any], error_message: string, api_exception:string
}

export interface catTipoReporteType {
  id:number, nombre:string
}

export interface catsubTipoReporteType {
  id:number, nombre:string
}
