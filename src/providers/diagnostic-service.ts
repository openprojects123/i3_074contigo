import { Injectable }     from '@angular/core';
import {Observable}       from 'rxjs/Rx';
import {Diagnostic}       from '@ionic-native/diagnostic';
import {BehaviorSubject}  from 'rxjs/BehaviorSubject';
import {Utils}            from "./utils-service";
import {Network}          from "@ionic-native/network";
import {BackgroundGeolocation} from '@ionic-native/background-geolocation';

let TAG = "DiagnosticService";

@Injectable()
export class DiagnosticService {
  private runsOnDevice: boolean;

  private isGpsEnabled = false;


  constructor(private diagnostic: Diagnostic,
              private network: Network,
              private bg: BackgroundGeolocation,) {
    this.runsOnDevice = Utils.runsOnDevice();

    Utils.platform.ready().then(()=>{
      this.isLocationEnabled().subscribe((resp)=>{this.isGpsEnabled = resp});
    });
  }

  public isOnLine():boolean {
    return navigator.onLine;
  }

  public networkConnectionChange():BehaviorSubject<boolean>{
    let bh = new BehaviorSubject<boolean>(navigator.onLine);

    this.network.onDisconnect().subscribe(() => {
      bh.next(false);
    });

    this.network.onConnect().subscribe(() => {
      bh.next(true);
    });

    return bh;
  }


  /**
   * Se dispara cuando se activa o desactiva el wifi o Datos
   * No dice si está en linea o no.
   * @returns {any}
   */
  public networkOnChange():Observable<any>{
    if(this.runsOnDevice)
      return this.network.onchange();
    else
      return Observable.create(observer=>{
        observer.next(true);
        observer.complete();
      });
  }

  public networkOnDisconnect():Observable<any>{
    return this.network.onDisconnect();
  }

  public networkOnConnect():Observable<any>{
    return this.network.onConnect();
  }


  public gpsConnectionChange():BehaviorSubject<boolean>{
    let bh = new BehaviorSubject<boolean>(this.isGpsEnabled);

    let watch = ()=> {
      this.bg.watchLocationMode().then((enabled) => {
          this.isGpsEnabled = enabled;
          bh.next(enabled);
          stopWatchGPSState();
        },
        error => {
          bh.error(Utils.LOG.e(TAG, error, "gpsConnectionChange...->watchLocationMode()"))
        }).catch(error=>{
        bh.error(Utils.LOG.e(TAG, error, "gpsConnectionChange...->watchLocationMode()"))
      })
    };

    let stopWatchGPSState = ()=>{
      this.bg.stopWatchingLocationMode().catch(error=>{
        bh.error(Utils.LOG.e(TAG, error, "gpsConnectionChange...->stopWatchGPSState()"))
      });
      setTimeout(()=>{
        watch();
      }, 100)
    };


    watch();

    return bh;
  }

  public gpsStopWatching(){
    this.bg.stopWatchingLocationMode();
  }

  /**
   * @description
   * Verifica si la tarjeta WiFi está habilitada,
   * No verifica que haya datos (internet)
   * @returns {Observable}
   */
  public isWiFiEnabled():Observable<boolean> {
    return Observable.create(observer=> {
      //Asumiré que sí esta habilitado si no es un dispositivo móvil
      if(!this.runsOnDevice){
        observer.next(true);
        return observer.complete()
      }

      try {
        this.diagnostic.isWifiEnabled().then(isEnabled=>{
          observer.next(isEnabled);
          observer.complete();
        }, error=>{
          observer.error(Utils.LOG.e(TAG, error, "checkWifiEnabled...->isWifiEnabled()"));
        }).catch(e=>{
          observer.error(Utils.LOG.e(TAG, e, "catch{checkWifiEnabled.isWifiEnabled()}"));
        });
      }
      catch (e) {
        observer.error(Utils.LOG.e(TAG, e, "catch2{checkWifiEnabled.isWifiEnabled()}"));
      }
    })
  }

  public switchToWifiSettings(){
    try {
      this.diagnostic.switchToWifiSettings();
    }
    catch (e) {
      Utils.LOG.e(TAG, e, "catch{switchToWifiSettings()}");
    }
  }

  /**
   * @description
   * Verifica si el GPS está habilitado
   * @returns {Observable}
   */
  public isLocationEnabled():Observable<boolean> {
    return Observable.create(observer=> {
      //Asumiré que sí esta habilitado si no es un dispositivo móvil
      /*if(!this.runsOnDevice){
       observer.next(true);
       return observer.complete()
       }*/

      this.diagnostic.isLocationEnabled().then(isEnabled=>{
        this.isGpsEnabled = isEnabled;
        observer.next(isEnabled);
        observer.complete();
      }, error=>{
        observer.error(Utils.LOG.e(TAG, error, "isLocationEnabled()"));
      }).catch(e=>{
        observer.error(Utils.LOG.e(TAG, e, "catch{isLocationEnabled...->isLocationEnabled()}"));
      });
    }).catch(e=>{
      return Observable.throw(e);
    })
  }

  public switchToLocationSettings(){
    try {
      this.diagnostic.switchToLocationSettings();
    }
    catch (e) {
      Utils.LOG.e(TAG, e, "catch{switchToLocationSettings()}");
    }
  }
}
