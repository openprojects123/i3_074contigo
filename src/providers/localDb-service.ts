import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {SQLite, SQLiteObject} from '@ionic-native/sqlite';
import {Utils} from "./utils-service";
import {latlngType} from "./locationAndMap/geolocation-service";

let TAG = "LocalDbSvc";

let sqlDB: any;
let DB: SQLiteObject;

@Injectable()
export class LocalDbSvc {

  private HasChanged:boolean = false;

  constructor(private sqlite: SQLite){
    Utils.platform.ready().then(()=>{
      if(Utils.runsOnDevice())
        sqlDB = window['plugins'].sqlDB;
    }).catch(e=>{
      Utils.LOG.e(TAG, e, "constructor->platform.ready()")
    })
  }

  /**
   * Intenta crear la BD a partir de archivo y cargarla, sino se crea,
   * intenta cargarla (tal ves ya existe), sino es que hubo un error.
   * @returns Observable<boolean>
   */
  public loadDB():Observable<boolean>{
    return Observable.create(observer=>{

      let dbName     = "CAPUFE.db";//"CAPUFE.sqlite";
      let dbLocation = "default";  //'default' || 'Documents' || 'Library';

      try{
        sqlDB.copy(dbName, 0, (resp)=>{
          Utils.LOG.i(
            TAG, "****************** sqlDB.copy: "+resp+" ************", "loadDB->sqlDB.copy()"
          );
          this.openDB(dbName, dbLocation).subscribe(resp=>{
            observer.next(resp);
            observer.complete();
          }, reason=>{
            observer.error(reason);
          });
        },reason=>{
          Utils.LOG.i(
            TAG, "Base de datos no copiada debido a: "+ JSON.stringify(reason), "loadDB->sqlDB.copy()"
          );

          this.openDB(dbName, dbLocation).subscribe(resp=>{
            observer.next(resp);
            observer.complete();
          }, reason=>{
            observer.error(reason);
          });

        });
      }
      catch(e){
        observer.error(Utils.LOG.e(TAG, e, "catch{loadDB->this.openDB()}"));
      }
    }).catch(e=>{
      return Observable.create(observer=>{
        observer.error(e)
      });
    })
  }

  /**
   *
   * @param dbName
   * @param dbLocation
   * @returns {Observable<R>|Promise<R>|Promise<T>|Promise<TResult|T>}
   */
  private openDB(dbName, dbLocation):Observable<boolean>{
    return Observable.create(observer=>{
      //try{
        this.sqlite.create({
          name: dbName,
          iosDatabaseLocation: dbLocation, // the location field is required
          //location: dbLocation // the location field is required
        }).then((sqliteObj:SQLiteObject) => {
          DB = sqliteObj;
          Utils.LOG.i(
            TAG, "****************** sqlDB.openDatabase  OK ************", "openDB->sqlite.create()"
          );

          observer.next(true);
          observer.complete();
        }, (error) => {
          observer.error(Utils.LOG.e(TAG, error, "lopenDB->sqlite.create()"));
        });
      /*}
      catch(e) {
        observer.error(Utils.LOG.e(TAG, e, "catch:openDB->sqlite.create()"));
      }*/
    }).catch(error=>{
      return Observable.create(observer=>{
        observer.error(Utils.LOG.e(TAG, error, "openDB->Observable.catch(DB.openDatabase)"))
      });
    })
  }

  public testDB():Observable<any>{
    return Observable.create(observer=> {

      if (!Utils.runsOnDevice()) {
        return observer.error(
          Utils.LOG.e(TAG, "No hay una instancia de la BD", "testDB()")
        );
      }

      DB.executeSql("SELECT name FROM sqlite_master WHERE type='table'", [])
        .then(resp=> {
          let tablas = [];
          for (let i = 0; i < resp.rows.length; i++) {
            tablas.push(resp.rows.item(i));
          }
          observer.next(tablas);
          observer.complete();
        },  reason=> {
          observer.error(Utils.LOG.e(TAG, reason, "testDB->DB.executeSql()"));
        });
    })
  }

  public populateIncidencias(incidenciasArray:Array<incidenciaType>):Observable<number>{
    return Observable.create(observer=> {

      if (!Utils.runsOnDevice()) {
        return observer.error(
          Utils.LOG.e(TAG, "No hay una instancia de la BD", "populateIncidencias()")
        );
      }

      let totalIncidencias = incidenciasArray.length;
      let totCiclos = 0;
      let totalInsert = 0;


      DB.executeSql("DELETE FROM incidencias", []).then(() => {
        incidenciasArray.forEach((mark) =>{
          let query = "INSERT INTO incidencias " +
            "(id, seguimiento, idtipo, idsubtipo, fhreporte, idautopista, km, mt, tipocierre, ficha, lat, lng) " +
            "values(?,?,?,?,?,?,?,?,?,?,?,?)";
          DB.executeSql(query, [
            mark.id,
            mark.seguimiento,
            mark.idtipo,
            mark.idsubtipo,
            mark.fhreporte,
            mark.idautopista,
            mark.km,
            mark.mt,
            mark.tipocierre,
            mark.ficha,
            mark.lat,
            mark.lng
          ]).then((t)=> {
            totalInsert++;
            totCiclos++;

            if(totCiclos == totalIncidencias){
              observer.next(totalInsert);
              observer.complete();
            }

          },  (reason)=>{
            totCiclos++;
            if(totCiclos == totalIncidencias){
              observer.next(totalInsert);
              observer.complete();
            }

            //can't insert on incidencias
            Utils.LOG.w(TAG, "No se pudo insertar la incidencia ya que: "+JSON.stringify(reason), "populateIncidencias()");
          });
        });
      },  (reason) =>{
        //Can't truncate table
        observer.error(Utils.LOG.w(TAG, reason, "catch:populateIncidencias()"));
      });
    })
  }

  public populateObras(obras:Array<obraType>):Observable<number>{
    return Observable.create(observer=> {
      if (!Utils.runsOnDevice()) {
        return observer.error(
          Utils.LOG.e(TAG, "No hay una instancia de la BD", "populateIncidencias()")
        );
      }

      let totalObras = obras.length;
      if(!totalObras){return observer.next(0)}

      let totCiclos = 0;
      let totalInsert = 0;

      DB.executeSql("DELETE FROM obras", []).then(() => {
        obras.forEach( (obra, idx) =>{
          let query = "INSERT INTO obras " +
            "(idObras, tipo, idTramo, kmInicial, kmFinal, nombreTramo, cuerpo) " +
            "values(?,?,?,?,?,?,?)";
          DB.executeSql(query, [
            obra.idObras,
            obra.tipo,
            obra.idTramo,
            obra.kmInicial,
            obra.kmFinal,
            obra.nombreTramo,
            obra.cuerpo
          ]).then((t)=> {
            totCiclos++;
            totalInsert++;
            if (totCiclos == totalObras){
              observer.next(totalInsert);
              observer.complete();
            }

          },  (reason)=>{
            totCiclos++;
            if (totCiclos == totalObras){
              observer.next(totalInsert);
              observer.complete();
            }

            //can't insert on obras
            Utils.LOG.w(TAG, "No se pudo insertar la obra ya que: "+JSON.stringify(reason), "populateIncidencias()");
          });
        });
      },  (reason) =>{
        //Can't truncate table
        observer.error(Utils.LOG.w(TAG, reason, "populateIncidencias()"));
      });
    })
  }

  public populateTramos(){
    //pendiente
  }

  /**
   * Obtiene TODAS las incidencias
   * Si es un dispositivo movil, las extrae de la DB sino, desde la API CAPUFE
   * @returns Observable<Array<incidenciaType>>
   */
  public getIncidencias():Observable<Array<incidenciaType>>{
    return Observable.create(observer=>{
      if (!Utils.runsOnDevice()) {
        observer.error(
          Utils.LOG.e(TAG, "No hay una instancia de la BD", "getIncidencias()")
        )
      }
      else{
        DB.executeSql("SELECT * FROM incidencias", []).then((resp)=>{
          Utils.LOG.i(TAG, "cargando "+resp.rows.length+" incidencias desde la BD", "getIncidencias()");
          let inc = [];
          for(let i=0; i<resp.rows.length; i++){
            inc.push(resp.rows.item(i));
          }
          observer.next(inc);
          observer.complete();
        }, (reason)=>{
          observer.error(Utils.LOG.e(TAG, reason, "getIncidencias->DB.executeSql(1)"));
        });
      }
    })
  }

  public getIncidenciasByIdAutopista(id:number):Observable<Array<incidenciaType>>{
    return Observable.create(observer=>{
      if (!Utils.runsOnDevice()) {
        return observer.error(
          Utils.LOG.e(TAG, "No hay una instancia de la BD", "getIncidenciasByIdAutopista()")
        );
      }

      DB.executeSql("SELECT * FROM incidencias WHERE idautopista = ?", [id]).then((resp)=>{
        let inc:Array<incidenciaType> = [];
        for(let i=0; i<resp.rows.length; i++){
          inc.push(resp.rows.item(i));
        }
        observer.next(inc);
        observer.complete();
      }, (reason)=>{
        return observer.error(Utils.LOG.e(TAG, reason, "getIncidenciasByIdAutopista()"));
      });
    })
  }

  /**
   *
   * @param tramosIds: Array<number>
   * @returns Observable<Array<incidenciaType>>
   */
  public getIncidenciasOnTramos(tramosIds: Array<number> | string):Observable<Array<incidenciaType>>{
    return Observable.create(observer=>{
      if (!Utils.runsOnDevice()) {
        return observer.error(
          Utils.LOG.e(TAG, "No hay una instancia de la BD", "getIncidenciasOnTramos()")
        );
      }

      if (tramosIds.length == 0) {
        observer.error("El parámetro de consulta (tramosIds) no contiene datos");
        return;
      }

      DB.executeSql("SELECT * FROM incidencias WHERE idautopista IN ("+tramosIds+")", [])
        .then((resp)=>{
          let inc = [];
          for(let i=0; i<resp.rows.length; i++){
            inc.push(resp.rows.item(i));
          }
          //Utils.LOG.i(TAG, "incidencias on tramos: "+JSON.stringify(inc), "getIncidenciasOnTramos->DB.executeSql()");
          observer.next(inc);
          observer.complete();
        }, sqliteError=>{
          observer.error(Utils.LOG.e(TAG, sqliteError, "getIncidenciasOnTramos()"));
        });
    })
  }

  /**
   *
   * @param tramosIds: Array<tramoType>
   * @returns Observable<Array<incidenciaType>>
   */
  public getIncidenciasOnTramos2(tramosIds: Array<tramoType> | string):Observable<Array<incidenciaType>>{
    return Observable.create(observer=>{
      if (!Utils.runsOnDevice()) {
        return observer.error(
          Utils.LOG.e(TAG, "No hay una instancia de la BD", "getIncidenciasOnTramos2()")
        );
      }

      if (tramosIds.length == 0) {
        observer.error("El parámetro de consulta (tramosIds) no contiene datos");
        return;
      }

      DB.executeSql("SELECT * FROM incidencias WHERE idautopista IN ("+tramosIds+")", [])
        .then((resp)=>{
          let inc = [];
          for(let i=0; i<resp.rows.length; i++){
            inc.push(resp.rows.item(i));
          }
          //Utils.LOG.i(TAG, "incidencias on tramos: "+JSON.stringify(inc), "getIncidenciasOnTramos->DB.executeSql()");
          observer.next(inc);
          observer.complete();
        }, sqliteError=>{
          observer.error(Utils.LOG.e(TAG, sqliteError, "getIncidenciasOnTramos2()"));
        });
    })
  }

  /**
   *
   * @param idsTramos:string (ejemplo: "1", "1,3,4,...")
   * @returns Array<obraType>>
   */
  public getObrasOnTramos(idsTramos: Array<number>|string):Observable<Array<obraType>>{
    return Observable.create(observer=> {
      if (!Utils.runsOnDevice()) {
        return observer.error(
          Utils.LOG.e(TAG, "No hay una instancia de la BD", "getObras()")
        );
      }

      if (idsTramos.length == 0) {
        observer.error("El parámetro de consulta (idsTramos) no contiene datos");
        return;
      }

      DB.executeSql("SELECT * FROM obras WHERE idTramo IN ("+idsTramos+")", []).then((t) => {
        let obras = [];
        for (let i = 0; i < t.rows.length; i++) {
          obras.push(t.rows.item(i));
        }

        observer.next(obras);
        observer.complete();
      }, error=>{
        observer.error(Utils.LOG.e(TAG, error, "getObras()"));
      })
    })
  }

  /**
   *
   * @param idsObras:string (ejemplo: "1", "1,3,4,...")
   * @returns Array<obraType>>
   */
  public getObrasByIds(idsObras:Array<number>|string):Observable<Array<obraType>>{
    return Observable.create(observer=> {
      if (!Utils.runsOnDevice()) {
        return observer.error(
          Utils.LOG.e(TAG, "No hay una instancia de la BD", "getObras()")
        );
      }

      if (idsObras.length == 0) {
        observer.error("El parámetro de consulta (idsObras) no contiene datos");
        return;
      }

      DB.executeSql("SELECT * FROM obras WHERE idTramo IN ("+idsObras+")", []).then((t) => {
        let obras = [];
        for (let i = 0; i < t.rows.length; i++) {
          obras.push(t.rows.item(i));
        }

        observer.next(obras);
        observer.complete();
      }, error=>{
        observer.error(Utils.LOG.e(TAG, error, "getObras()"));
      })
    })
  }

  /**
   * Obtiene el catalogo de Tramos
   * @returns
   * {
   *  tramos:[
   *   {id, nombre, acronimo, autopista, selected}
   *  ],
   *  numselected: number
   * }
   */
  public getCatTramos1():Observable<catTramosType>{
    return Observable.create(observer=> {

      if (!Utils.runsOnDevice()) {
        return observer.error(
          Utils.LOG.e(TAG, "No hay una instancia de la BD", "getCatTramos()")
        );
      }

      let query = "SELECT * FROM tramos";
      DB.executeSql(query, []).then( (t)=> {
        let tramos = [];
        let numSelected = 0;
        for (let i = 0; i < t.rows.length; i++) {
          if (t.rows.item(i).selected == 1 || t.rows.item(i).selected == true || t.rows.item(i).selected == 'true') {
            numSelected++;
          }
          tramos.push(t.rows.item(i));
        }

        observer.next({tramos: tramos, numSelected: numSelected});
        observer.complete();
      },  (reason)=> {
        observer.error(Utils.LOG.e(TAG, reason, "getCatTramos()"));
      });
    })
  }
  public getCatTramos():Observable<catTramosType>{
    return Observable.create(observer=> {

      if (!Utils.runsOnDevice()) {
        return observer.error(
          Utils.LOG.e(TAG, "No hay una instancia de la BD", "getCatTramos()")
        );
      }

      let query = "" +
        "SELECT idTramo as id, nombreTramo as nombre, nombreTramo as acronimo, nombreTramo as autopista, selected " +
        "FROM tramos2 ORDER BY nombreTramo";
      DB.executeSql(query, []).then((t)=> {
        let tramos = [];
        let numSelected = 0;
        for (let i = 0; i < t.rows.length; i++) {
          if (t.rows.item(i).selected == 1 || t.rows.item(i).selected == true || t.rows.item(i).selected == 'true') {
            numSelected++;
          }
          tramos.push(t.rows.item(i));
        }

        observer.next({tramos: tramos, numSelected: numSelected});
        observer.complete();
      },  (reason)=> {
        observer.error(Utils.LOG.e(TAG, reason, "getCatTramos()"));
      });
    })
  }

  /**
   * Obtiene un Tramo especifico por su id
   * @param id
   * @returns {any}
   */
  public getTramoById1(id:number):Observable<tramoType |null>{
    return Observable.create(observer=> {

      if (!Utils.runsOnDevice()) {
        observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD","getTramoById()"));
        return;
      }

      let query = "SELECT * FROM tramos WHERE id = ?";
      DB.executeSql(query, [id]).then( (t) =>{
        if(t.rows.length)
          observer.next(t.rows.item(0));
        else{
          observer.next(null);
        }
        observer.complete();
      },  (reason) =>{
        observer.error(Utils.LOG.e(TAG, reason, "getTramoById()"));
      });
    })
  }
  public getTramoById(id:number):Observable<tramoType|null>{
    return Observable.create(observer=> {

      if (!Utils.runsOnDevice()) {
        observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD","getTramoById()"));
        return;
      }

      let query = "SELECT idTramo as id, nombreTramo as nombre, nombreTramo as acronimo, nombreTramo as autopista, selected " +
        "FROM tramos2 WHERE id = ?";
      DB.executeSql(query, [id]).then( (t) =>{
        if(t.rows.length)
          observer.next(t.rows.item(0));
        else{
          observer.next(null);
        }
        observer.complete();
      },  (reason) =>{
        observer.error(Utils.LOG.e(TAG, reason, "getTramoById()"));
      });
    })
  }

  /**
   * Obtiene el numero de kilometros (numero de filas) que componen al tramo
   * @param idAutopista
   * @returns number
   */
  public getTotalKmsOfTramo1(idAutopista:number):Observable<number>{
    return Observable.create(observer=> {

      if (!Utils.runsOnDevice()) {
        observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "getLengthOfTramo()"));
        return;
      }

      let query = "SELECT COUNT(*) as longitud FROM coords_tramos WHERE idAutopista = ?";
      //console.log(query + " -> " + idAutopista);
      DB.executeSql(query, [idAutopista]).then( (t)=> {
        observer.next(t.rows.item(0).longitud);
        observer.complete();
      },  (reason) =>{
        observer.error(Utils.LOG.e(TAG, reason, "getLengthOfTramo()"));
      });
    })
  }
  public getTotalKmsOfTramo(idAutopista:number):Observable<number>{
    return Observable.create(observer=> {

      if (!Utils.runsOnDevice()) {
        observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "getLengthOfTramo()"));
        return;
      }

      let query = "SELECT COUNT(*) as longitud FROM coords_tramos2 WHERE fk_cvetramo = ?";
      //console.log(query + " -> " + idAutopista);
      DB.executeSql(query, [idAutopista]).then( (t)=> {
        observer.next(t.rows.item(0).longitud);
        observer.complete();
      },  (reason) =>{
        observer.error(Utils.LOG.e(TAG, reason, "getLengthOfTramo()"));
      });
    })
  }

  /**
   * Obtiene el punto inicial y final del tramo
   * @param idAutopista
   * @returns [latlngType]
   */
  public getStartEndOfTramo(idAutopista:number):Observable<Array<latlngType>>{
    return Observable.create(observer=> {

      if (!Utils.runsOnDevice()) {
        observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "getStartEndOfTramo()"));
        return;
      }

      let query = "SELECT latitud, longitud FROM coords_tramos2 WHERE fk_cvetramo = ? AND kilometro IN("+
        "(SELECT max(kilometro) FROM coords_tramos2 c WHERE fk_cvetramo = ?),"+
        "(SELECT min(kilometro) FROM coords_tramos2 b WHERE fk_cvetramo = ?)) ORDER BY kilometro";
      //console.log(query + " -> " + idAutopista);
      DB.executeSql(query, [idAutopista, idAutopista, idAutopista]).then( (t)=> {
        let tramos: Array<latlngType> = [];

        //En teoria deberia obtener sólo 2 registros, punto minimo y maximo...
        for(let i=0; i<t.rows.length; i++){
          tramos.push({lat: t.rows.item(i).latitud, lng: t.rows.item(i).longitud});
        }
        //...Pero hay tramos con kilometros repetidos
        if(tramos.length >2){
          tramos = [tramos[0], tramos[tramos.length-1]];
        }

        observer.next(tramos);
        observer.complete();
      },  (reason) =>{
        observer.error(Utils.LOG.e(TAG, reason, "getStartEndOfTramo()"));
      });
    })
  }

  /**
   * Obtiene los tramos seleccionados por el usuario
   * @returns {any}
   */
  public getSelectedTramos1():Observable<Array<tramoType>>{
    return Observable.create(observer=> {

      if (!Utils.runsOnDevice()) {
        observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "getSelectedTramos()"));
        return;
      }

      let query = "SELECT * FROM tramos where selected = 1 OR selected = 'true'";
      DB.executeSql(query, []).then( (t)=> {
        let tramos = [];
        for (let i = 0; i < t.rows.length; i++) {
          tramos.push(t.rows.item(i));
        }
        observer.next(tramos);
        observer.complete();
      },  (reason) =>{
        observer.error(Utils.LOG.e(TAG, reason, "getSelectedTramos()"));
      });
    })
  }
  public getSelectedTramos():Observable<Array<tramoType>>{
    return Observable.create(observer=> {

      if (!Utils.runsOnDevice()) {
        observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "getSelectedTramos()"));
        return;
      }

      let query = "SELECT idTramo as id, nombreTramo as nombre, nombreTramo as acronimo, nombreTramo as autopista, selected " +
        "FROM tramos2 where selected = 1 OR selected = 'true' ORDER BY nombre";
      DB.executeSql(query, []).then( (t)=> {
        let tramos = [];
        for (let i = 0; i < t.rows.length; i++) {
          tramos.push(t.rows.item(i));
        }
        observer.next(tramos);
        observer.complete();
      },  (reason) =>{
        observer.error(Utils.LOG.e(TAG, reason, "getSelectedTramos()"));
      });
    })
  }

  /**
   * Obtiene el arreglo/Matriz [[{lat, lng}, ...], ...] con el conjunto de
   * coordenadas de cada tramo seleccionado.
   * @param id_array: Arreglo simple con los id's de cada tramo
   * seleccionado [1, 2, ...]
   * @returns Observable<Array<Array<latlngType>>>
   */
  public getCoordsInTramos1(idAutopista:Array<number>|string):Observable<Array<Array<latlngType>>>{
    return Observable.create(observer=> {
      if (!Utils.runsOnDevice()) {
        observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "getTramosCoords()"));
        return;
      }

      if (idAutopista.length == 0) {
        observer.error("El parámetro de consulta (idAutopista) no contiene datos");
        return;
      }

      let query = "SELECT * FROM coords_tramos WHERE idAutopista IN (" + idAutopista + ") ORDER BY idAutopista";
      //console.log(query);
      DB.executeSql(query, []).then( (res) =>{
        let tramos = [];
        let points = [];

        if (!res.rows.length) {
          observer.next(tramos);
          observer.complete();
          return;
        }

        let curTramoId = res.rows.item(0).idAutopista;
        for (let i = 0; i < res.rows.length; i++) {
          if (res.rows.item(i).idAutopista != curTramoId) {
            curTramoId = res.rows.item(i).idAutopista;
            tramos.push(points);
            points = [];
          }

          points.push({lat: res.rows.item(i).lat, lng: res.rows.item(i).lng});
        }

        if (points.length) {tramos.push(points);}
        else
          Utils.LOG.w(TAG, "El tramo con idAutopista = "+curTramoId+" No tiene coordenadas", "getTramosCoords");

        //alert("Total tramos parseados: " + selectedTramos.length);

        observer.next(tramos);
        observer.complete();
      },  (reason) =>{
        observer.error(Utils.LOG.e(TAG, reason, "getTramosCoords()"));
      });
    })
  }
  public getCoordsInTramos(idAutopista:Array<number>|string):Observable<Array<Array<latlngType>>>{
    return Observable.create(observer=> {
      if (!Utils.runsOnDevice()) {
        observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "getTramosCoords2()"));
        return;
      }

      if (idAutopista.length == 0) {
        observer.error("El parámetro de consulta (idAutopista) no contiene datos");
        return;
      }

      let query = "SELECT idKilometro as id, kilometro as km, latitud as lat, longitud as lng, fk_cvetramo as idAutopista " +
        "FROM coords_tramos2 WHERE fk_cvetramo IN (" + idAutopista + ") ORDER BY fk_cvetramo";//GROUP BY kilometro <--los agrupo porque hay kilometros repetidos (dos km 0, dos km 1, dos 2, etc)
      //Utils.LOG.i(TAG, query, "getTramosCoords");
      DB.executeSql(query, []).then( (res) =>{
        let tramos = [];
        let points = [];

        if (!res.rows.length) {
          observer.next(tramos);
          observer.complete();
          return;
        }

        let curTramoId = res.rows.item(0).idAutopista;
        for (let i = 0; i < res.rows.length; i++) {
          //Si es un nuevo tramo...
          if (res.rows.item(i).idAutopista != curTramoId) {
            if(!points.length)
              Utils.LOG.w(TAG, "El tramo con idAutopista = "+curTramoId+" No tiene coordenadas", "getTramosCoords2()");

            curTramoId = res.rows.item(i).idAutopista;
            tramos.push(points);
            points = [];
          }

          if(res.rows.item(i).lat && res.rows.item(i).lng)
            points.push({lat: res.rows.item(i).lat, lng: res.rows.item(i).lng});
        }
        //Meto las coords del ultimo tramo
        tramos.push(points);

        //Utils.LOG.i(TAG, JSON.stringify(tramos), "x");

        observer.next(tramos);
        observer.complete();
      },  (reason) =>{
        observer.error(Utils.LOG.e(TAG, reason, "getTramosCoords2()"));
      });
    })
  }

  public getCoordsByTramo(idAutopista:number):Observable<Array<latlngType>>{
    return Observable.create(observer=> {
      if (!Utils.runsOnDevice()) {
        observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "getTramosCoords2()"));
        return;
      }


      let query = "SELECT idKilometro as id, kilometro as km, latitud as lat, longitud as lng, fk_cvetramo as idAutopista " +
        "FROM coords_tramos2 WHERE fk_cvetramo = ? GROUP BY kilometro";// <--los agrupo porque (o por si) hay kilometros repetidos (dos km 0, dos km 1, dos 2, etc)
      //Utils.LOG.i(TAG, query, "getTramosCoords");
      DB.executeSql(query, [idAutopista]).then( (res) =>{
        let points = [];

        for (let i = 0; i < res.rows.length; i++) {
          if(res.rows.item(i).lat && res.rows.item(i).lng)
            points.push({lat: res.rows.item(i).lat, lng: res.rows.item(i).lng});
        }

        //Utils.LOG.i(TAG, JSON.stringify(points), "x");

        observer.next(points);
        observer.complete();
      },  (reason) =>{
        observer.error(Utils.LOG.e(TAG, reason, "getCoordsByTramo2()"));
      });
    })
  }

  /**
   * Selecciona/deselecciona un tramo
   * @param {type} id :ID del tramo
   * @param {type} val :{1|0}
   * @returns {$q@call;defer.promise} {"ok" | error string}
   */
  public toggleTramo1(id, val:number):Observable<boolean>{
    return Observable.create(observer=> {

      if (!Utils.runsOnDevice()) {
        observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "toggleTramo()"));
        return;
      }

      let query = "UPDATE tramos set selected = ? WHERE id = ?";
      DB.executeSql(query, [val, id]).then((resp) =>{
        this.HasChanged = true;

        observer.next(true);
        observer.complete();
      },  (reason)=> {
        observer.error(Utils.LOG.e(TAG, reason, "toggleTramo()"));
      });
    })
  }
  public toggleTramo(id, val:number):Observable<boolean>{
    return Observable.create(observer=> {

      if (!Utils.runsOnDevice()) {
        observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "toggleTramo2()"));
        return;
      }

      let query = "UPDATE tramos2 set selected = ? WHERE idTramo = ?";
      DB.executeSql(query, [val, id]).then((resp) =>{
        this.HasChanged = true;

        observer.next(true);
        observer.complete();
      },  (reason)=> {
        observer.error(Utils.LOG.e(TAG, reason, "toggleTramo2()"));
      });
    })
  }

  public selectAllTramos1():Observable<boolean>{
    return Observable.create(observer=> {

      if (!Utils.runsOnDevice()) {
        observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "selectAllTramos()"));
        return;
      }

      let query = "UPDATE tramos SET selected = 1";
      DB.executeSql(query, []).then( (res)=> {
        this.HasChanged = true;
        observer.next(true);
        observer.complete();
      },  (reason) =>{
        observer.error(Utils.LOG.e(TAG, reason, "selectAllTramos()"));
      });
    })
  }
  public selectAllTramos():Observable<boolean>{
    return Observable.create(observer=> {

      if (!Utils.runsOnDevice()) {
        observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "selectAllTramos2()"));
        return;
      }

      let query = "UPDATE tramos2 SET selected = 1";
      DB.executeSql(query, []).then( (res)=> {
        this.HasChanged = true;
        observer.next(true);
        observer.complete();
      },  (reason) =>{
        observer.error(Utils.LOG.e(TAG, reason, "selectAllTramos2()"));
      });
    })
  }

  public unselectAllTramos1():Observable<boolean>{
    return Observable.create(observer=> {

      if (!Utils.runsOnDevice()) {
        observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "unselectAllTramos()"));
        return;
      }

      let query = "UPDATE tramos SET selected = 0";
      DB.executeSql(query, []).then( (res)=> {
        this.HasChanged = true;
        observer.next("ok");
        observer.complete();
      },  (reason) =>{
        observer.error(Utils.LOG.e(TAG, reason, "unselectAllTramos()"));
      });
    })
  }
  public unselectAllTramos():Observable<boolean>{
    return Observable.create(observer=> {

      if (!Utils.runsOnDevice()) {
        observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "unselectAllTramos2()"));
        return;
      }

      let query = "UPDATE tramos2 SET selected = 0";
      DB.executeSql(query, []).then( (res)=> {
        this.HasChanged = true;
        observer.next("ok");
        observer.complete();
      },  (reason) =>{
        observer.error(Utils.LOG.e(TAG, reason, "unselectAllTramos2()"));
      });
    })
  }


  /**
   * Obtiene el catalogo de Puntos de Interes (POI)
   * @returns {any}
   */
  public getCatPOI():Observable<catPOIType>{
    return Observable.create(observer=> {

      if (!Utils.runsOnDevice()) {
        return observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "getCatPOI()"));
      }

      let query = "SELECT * FROM poi";
      DB.executeSql(query, []).then( (p) =>{
        let poi = [];
        let numSelected = 0;
        for (let i = 0; i < p.rows.length; i++) {
          if (p.rows.item(i).selected == 1 || p.rows.item(i).selected == true || p.rows.item(i).selected == "true") {
            numSelected++;
          }
          poi.push(p.rows.item(i));
        }

        observer.next({poi: poi, numSelected: numSelected});
        observer.complete();
      },  (reason) =>{
        observer.error(Utils.LOG.e(TAG, reason, "getCatPOI()"));
      });
    })
  }

  /**
   * Obtiene los Puntos de interes seleccionados por el usuario
   * [{poi1}, {poi2},...]
   * @returns {any}
   */
  public getSelectedPOI():Observable<[POIType]>{
    return Observable.create(observer=>{
      if(!Utils.runsOnDevice()){
        observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "getSelectedPOI()"));
        return;
      }

      let query = "SELECT * FROM poi where selected = 1";
      DB.executeSql(query, []).then((p) =>{
        let poi = [];
        for(let i=0; i<p.rows.length; i++){
          poi.push(p.rows.item(i));
        }
        observer.next(poi);
        observer.complete();
      },  (reason) =>{
        observer.error(Utils.LOG.e(TAG, reason, "getSelectedPOI()"));
      });
    })
  }

  /**
   * Obtiene el subconjunto de poi cuyo idautopista in (idTramo) &&
   * clavecategoria in poiArr.id
   * @param idTramosStr: puede ser un entero (number) o un array de enteros (object)
   * @param poiArr: Catalogo poi
   * @returns {any}
   */
  public getPOICoordsInTramos(idsTramos:string | Array<number>, idsPOI:string | Array<number>):Observable<[coordsPOIType]>{
    return Observable.create(observer=>{

      if (!Utils.runsOnDevice()) {
        observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "getPoiByTramo()"));
        return;
      }

      if (idsTramos.length == 0 || idsPOI.length==0) {
        observer.error("Algunos de los parámetros de consulta (idsTramos, idsPOI) no contienen datos");
        return;
      }


      //Utils.LOG.i(TAG, "idsTramos: "+JSON.stringify(idsTramos)+"; idsPOI: "+JSON.stringify(idsPOI), "getPoiByTramo()");
      let query = "SELECT * FROM coords_poi " +
        "WHERE idautopista IN (" + idsTramos + ") AND clavecategoria IN (" + idsPOI + ") "+
        "ORDER BY idautopista";


      DB.executeSql(query, []).then( (p)=> {
        let poi = [];
        for (let i = 0; i < p.rows.length; i++) {
          poi.push(p.rows.item(i));
        }
        observer.next(poi);
        observer.complete();
      },  (reason) =>{
        observer.error(Utils.LOG.e(TAG, reason, "getPOICoordsByTramo()"));
      });
    })
  }

  /**
   * Obtiene el subconjunto de poi cuyo idautopista in (idTramo) &&
   * clavecategoria in poiArr.id
   * @param idTramos
   * @param poiArr
   * @returns {any}
   */
  public getPoiWithNameByTramo(idTramos:Array<number>|string, idCategorias:Array<number>|string):Observable<any>{
    return Observable.create(observer=> {

      if (!Utils.runsOnDevice()) {
        observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "getPoiWithNameByTramo()"));
        return;
      }

      if (idTramos.length == 0 || idCategorias.length == 0) {
        observer.error("Algunos de los parámetros de consulta (idTramos, idCategorias) no contienen datos");
        return;
      }


      //let query = "SELECT * FROM coords_poi where idautopista in ("+idTramos+") and clavecategoria in ("+idStr+")";
      let query =
        "SELECT c1.id, c1.km, c1.mt, c1.lat, c1.lng, c1.cuerpo, c1.detalle, " +
        "c2.nombre AS nombrepoi, c3.nombre AS nombretramo, c3.autopista AS nombrepista, " +
        "FROM coords_poi c1 " +
        "INNER JOIN poi AS c2 ON c1.clavecategoria = c2.id " +
        "INNER JOIN tramos AS c3 ON c1.idautopista=c3.id " +
        "WHERE idautopista IN (" + idTramos + ") AND clavecategoria IN (" + idCategorias + ")"+
        "ORDER BY idautopista";

      Utils.LOG.i(TAG, query, "getPoiWithNameByTramo()");

      DB.executeSql(query, []).then( (p)=> {
        let poi = [];
        for (let i = 0; i < p.rows.length; i++) {
          poi.push(p.rows.item(i));
        }
        observer.next(poi);
        observer.complete();
      },  (reason) =>{
        observer.error(Utils.LOG.e(TAG, reason, "getPoiWithNameByTramo()"));
      });

    })
  }

  /**
   * Selecciona/deselecciona un punto de interes
   * @param id
   * @param val
   * @returns {any}
   */
  public togglePOI(id, val:number):Observable<boolean>{
    return Observable.create(observer=> {

      if (!Utils.runsOnDevice()) {
        observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "togglePOI()"));
        return;
      }

      let query = "UPDATE poi set selected = ? WHERE id = ?";
      DB.executeSql(query, [val, id]).then( (resp) =>{
        this.HasChanged = true;
        observer.next(true);
        observer.complete();
      },  (reason) =>{
        observer.error(Utils.LOG.e(TAG, reason, "togglePOI()"));
      });
    })
  }

  public selectAllPOI():Observable<boolean>{
    return Observable.create(observer=> {

      if (!Utils.runsOnDevice()) {
        observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "selectAllPOI()"));
        return;
      }

      let query = "UPDATE poi SET selected = 1";
      DB.executeSql(query, []).then( () =>{
        this.HasChanged = true;
        observer.next(true);
        observer.complete();
      },  (reason) =>{
        observer.error(Utils.LOG.e(TAG, reason, "selectAllPOI()"));
      });
    })
  }

  public unselectAllPOI():Observable<boolean>{
    return Observable.create(observer=> {

      if (!Utils.runsOnDevice()) {
        observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "unselectAllPOI()"));
        return;
      }

      let query = "UPDATE poi SET selected = 0";
      DB.executeSql(query, []).then( () =>{
        this.HasChanged = true;
        observer.next(true);
        observer.complete();
      },  (reason)=> {
        observer.error(Utils.LOG.e(TAG, reason, "unselectAllPOI()"));
      });
    })
  }

  public hasModelChanged(){return this.HasChanged;}

  /**
   * Pone el estatus del modelo en: sin cambios
   * @param status
   */
  public updateModelStatus(status:boolean){this.HasChanged = status;}

  public findNeighbors1(latlng: latlngType, distance: number):Observable<Array<coordsTramoType>>{
    return Observable.create(observer=>{

      if (!Utils.runsOnDevice()) {
        return observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "findNeighbors()"));
      }

      Utils.LOG.i(TAG, "Intentando obtener el tramo mas cercano...", "findNeighbors()");

      let query = "SELECT * from coords_tramos "+
        "WHERE lat BETWEEN "+(latlng.lat - distance)+" AND "+(latlng.lat + distance)+" "+
        "AND lng BETWEEN "+(latlng.lng - distance)+" AND "+(latlng.lng + distance);

      //console.log(query);

      DB.executeSql(query, [])
        .then((coords) =>{
          let points:Array<coordsTramoType> = [];
          for(let i=0; i<coords.rows.length; i++){
            points.push(coords.rows.item(i));
          }
          observer.next(points);
          observer.complete();
        },  (reason) =>{
          observer.error(Utils.LOG.w(TAG, reason, "findNeighbors()"));
        });
    });
  }
  public findNeighbors(latlng: latlngType, distance: number):Observable<Array<coordsTramoType>>{
    return Observable.create(observer=>{

      if (!Utils.runsOnDevice()) {
        return observer.error(Utils.LOG.e(TAG, "No hay una instancia de la BD", "findNeighbors()"));
      }

      Utils.LOG.i(TAG, "Intentando obtener el tramo mas cercano...", "findNeighbors2()");

      let query = "SELECT idKilometro as id, kilometro as km, latitud as lat, longitud as lng, fk_cvetramo as idAutopista FROM coords_tramos2 "+
        "WHERE lat BETWEEN "+(latlng.lat - distance)+" AND "+(latlng.lat + distance)+" "+
        "AND lng BETWEEN "+(latlng.lng - distance)+" AND "+(latlng.lng + distance);

      //console.log(query);

      DB.executeSql(query, [])
        .then((coords) =>{
          let points:Array<coordsTramoType> = [];
          for(let i=0; i<coords.rows.length; i++){
            points.push(coords.rows.item(i));
          }
          observer.next(points);
          observer.complete();
        },  (reason) =>{
          observer.error(Utils.LOG.w(TAG, reason, "findNeighbors2()"));
        });
    });
  }


}

export interface obraType{
  idObras:     number,
  tipo:        string,
  idTramo:     number,
  kmInicial:   number,
  kmFinal:     number,
  nombreTramo: string,
  cuerpo:      string
}

export interface tramoType{
  id:        number,
  nombre:    string,
  acronimo:  string,
  autopista: string,
  selected:  boolean
}

export interface catTramosType{
  tramos:      [tramoType],
  numSelected: number
}

export interface coordsTramoType{
  id:          number,
  idAutopista: number,
  km:          number,
  lat:         number,
  lng:         number
}

export interface neighborType{
  tramo:     coordsTramoType,
  distancia: number
}

export interface latlngType {
  lat: number,
  lng: number
}


export interface POIType{
  id:       number,
  nombre:   string,
  selected: any
}

export interface catPOIType{
  poi:         [POIType],
  numSelected: number
}

export interface coordsPOIType{
  id:             number,
  clavecategoria: number,
  idautopista:    number,
  cuerpo:         string,
  km:             number,
  mt:             number,
  lat:            number,
  lng:            number,
  detalle:        string,
  foto1:          string,
  foto2:          string,
  foto3:          string,
  validado:       number
}

export interface incidenciaType{
  id:           number,
  seguimiento:  number,
  idtipo:       number,
  idsubtipo:    number,
  fhreporte:    string,
  idautopista:  number,
  km:           number,
  mt:           number,
  tipocierre:   number,
  ficha:        string,
  lat:          number,
  lng:          number
}
