import { Injectable } from '@angular/core';
import {Observable}   from 'rxjs/Rx';
import {Utils}        from "../utils-service";
import {
  Geolocation,
  Geoposition
}                     from '@ionic-native/geolocation';

let TAG = "GeolocationService";

declare let google:any;

@Injectable()
export class GeolocationService {

  constructor(private geolocation: Geolocation) {
  }

  /**
   *
   * @param options
   * @returns Observable<Geoposition>
   */
  public getCurrentPosition(options?):Observable<GeolocationServiceResponse>{
    return Observable.create(observer=>{
      this.geolocation.getCurrentPosition(options).then((pos:Geoposition) => {
        let response: GeolocationServiceResponse = {
          serviceProvider:    "network",
          coords:{
            latitude:         pos.coords.latitude,
            longitude:        pos.coords.longitude,
            accuracy:         pos.coords.accuracy,
            speed:            pos.coords.speed,
            altitude:         pos.coords.altitude,
            altitudeAccuracy: pos.coords.altitudeAccuracy,
            heading:          pos.coords.heading
          },
          timestamp: pos.timestamp
        };
        observer.next(response);
        observer.complete();
      }, error=>{
        let e;
        switch (error.code) {
          case 1:
            e = "no se otorgaron los permisos suficientes para usar la Geolocalización";
            break;
          case 2:
            e = "falló uno o más elementos de la geolocalización";
            break;
          case 3:
            e = "se agotó el tiempo de espera de la petición";
            break;
        }

        observer.error(Utils.LOG.w(TAG, {
          reason: (e || error.message),
          statusText: error.message
        }, "getCurrentPosition()"));
      }).catch(e=>{
        observer.error(Utils.LOG.e(TAG, JSON.stringify(e), "getCurrentPosition().catch()"));
      })
    })
  }

  /**
   * @description
   * Obtiene los componentes de una direccion (los que se puedan):
   * [estado, municipio, colonia, calle, etc]
   * @param latLng
   * @returns {observer}
   * onNext: {[string]} componentes de una direccion
   * onError: parsedError:string
   */
  public geocode(parameters: string | latlngType):Observable<any>{
    let p:any;
    if(typeof parameters != "string"){
      Utils.LOG.i(TAG, "Geocoding from location to address", "geocode(location)");
      p = {latLng: parameters};
    }
    else{
      Utils.LOG.i(TAG, "Geocoding from address to location", "geocode(address)");
      p = {address: parameters};
    }

    return Observable.create(observer=> {
      function geocoderSuccess(results, status) {
        if (status === "OK") {
          if(typeof parameters == "string"){
            if(results[0]){
              let loc = {
                lat:results[0].geometry.location.lat(),
                lng:results[0].geometry.location.lng()
              }
              observer.next(loc);
              observer.complete();
            }
            else{
              observer.error(Utils.LOG.e(TAG,
                'Geocoder no pudo determinar la localización', "geocode=>geocoderSuccess(address)")
              );
            }
          }
          else if (results[1]) {
            // Display address as text in the page

            let direccion = {};
            let result = results[0].address_components;
            for(let i in result){
              if(result[i].types[0] === 'country')
                direccion["pais"] = result[i].long_name;
              if(result[i].types[0] === 'state')
                direccion["estado"] = result[i].long_name;
              if(result[i].types[0] === 'administrative_area_level_1')
                direccion["areaLvl1"] = result[i].long_name;//como el estado
              if(result[i].types[0] === 'locality')
                direccion["municipio"] = result[i].long_name;
              if(result[i].types[0] === 'administrative_area_level_2')
                direccion["areaLvl2"] = result[i].long_name;//como el municipio...
              if(result[i].types[0] === 'route')
                direccion["calle"] = result[i].long_name;
              if(result[i].types[0] === "political")
                direccion["colonia"] = result[i].long_name;
              if(result[i].types[0] === 'street_number')
                direccion["calleNumero"]= result[i].long_name;
              if(result[i].types[0] === 'postal_code')
                direccion["codigoPostal"] = result[i].long_name;
            }

            if(!direccion["estado"]){direccion["estado"] = direccion["areaLvl1"];}
            if(!direccion["municipio"]){direccion["municipio"] = direccion["areaLvl2"];}


            /*if(direccion["colonia"]){fullDir = "Col: " + direccion["colonia"] + ", ";}
             if(direccion["calle"] && direccion["calle"] !== "Unnamed Road"){fullDir  += "Calle: " + direccion["calle"]+ ", "}
             if(direccion["calleNumero"]){fullDir += "No.: "+direccion["calleNumero"] + ", ";}
             if(direccion["municipio"]){fullDir += direccion["municipio"]+".";}*/

            observer.next(direccion);
            observer.complete();
          } else {
            observer.error(Utils.LOG.e(TAG,
              'Geocoder no pudo determinar la dirección', "geocode=>geocoderSuccess(location)")
            );
          }
        }
        else {
          observer.error(Utils.LOG.e(TAG,
            'hubo un error en Geocoder, status: ' + status, "geocode=>geocoderSuccess(status != OK)")
          );
        }
      } // end of reverseGeocoderSuccess

      try{
        let geocoder = new google.maps.Geocoder();
        geocoder.geocode(p, geocoderSuccess);
      }
      catch(e){
        observer.error(Utils.LOG.e(TAG, e, "catch{geocode()}"));
      }
    }).catch(e=>{
      return Observable.create(observer=>{
        observer.error(Utils.LOG.e(TAG, e, "Observable.catch{geocode()}"));
      });
    })
  }
}


export interface GeolocationServiceResponse{
  /**
   * Service provider
   */
  serviceProvider: string; //"network"
  /**
   * A Coordinates object defining the current location
   */
  coords: Coordinates;
  /**
   * A timestamp representing the time at which the location was retrieved.
   */
  timestamp: number;
}


export interface latlngType {
  lat: number, lng: number
}
