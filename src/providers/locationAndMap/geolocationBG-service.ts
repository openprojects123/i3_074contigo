import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {
  BackgroundGeolocation,
  BackgroundGeolocationConfig,
  BackgroundGeolocationResponse
} from '@ionic-native/background-geolocation';
import {Utils} from "../utils-service";
//import set = Reflect.set;


let TAG = "GeolocationBGService";

const config: BackgroundGeolocationConfig = {
  desiredAccuracy: 10,
  stationaryRadius: 20,
  distanceFilter: 30,
  debug: false, //  enable this hear sounds for background-geolocation life-cycle.
  stopOnTerminate: true, // enable this to clear background location settings when the app terminates

  interval: 1000, //<-- Android
  fastestInterval: 5000, //<-- Android

  // Activity Recognition config
  activityType: 'AutomotiveNavigation',
  pauseLocationUpdates:false,
};

@Injectable()
export class GeolocationBGService {
  public onLocationSuccessCallback:Function = null;
  public onLocationErrorCallback:Function   = null;

  constructor(private BG: BackgroundGeolocation) {

  }


  //Android-only
  public isGPSEnabled():Promise<any>{
    return this.BG.isLocationEnabled();
  }

  public showLocationSettings():void{
    return this.BG.showLocationSettings();
  }

  //Android-only
  public watchLocationMode():Promise<any>{
    return this.BG.watchLocationMode();
  }

  public stopWatchingLocationMode():Promise<any>{
    return this.BG.stopWatchingLocationMode();
  }

  /**
   * @description
   * Inicializa el servicio (plugin) android/ios de geolocalizacion,
   * y solicita una localizacion inicial.
   * Tambien se usa solo para activar el dialogo de permisos en iOS.
   * @returns {Observable}
   * onNext:  string (android) | true (ios)
   * onError: parsedError:string
   */
  public initPlugin():void{
    try{
      Utils.LOG.i(TAG, "Iniciando BGLocation plugin...", "initPlugin()");

      Utils.platform.ready().then(()=>{
        this.BG.configure(config).subscribe((pos: BackgroundGeolocationResponse)=>{
            let response: GeolocationBGServiceResponse = {
              serviceProvider:    pos.serviceProvider,
              coords:{
                latitude:         pos.latitude,
                longitude:        pos.longitude,
                accuracy:         pos.accuracy,
                speed:            pos.speed,
                altitude:         pos.altitude,
                altitudeAccuracy: pos.altitudeAccuracy,
                heading:          pos.bearing
              },
              timestamp: pos.timestamp
            };
            this.onBGLocationSuccesCbk(response);
          },
          (error)=>{
            this.onBGLocationErrorCbk(error);
          }
        );
      })
    }
    catch(e){
      Utils.showAlert("No se pudo iniciar el plugin de geolocalización ya que"+Utils.LOG.e(TAG, e, "catch{initPlugin()}"));
    }
  }


  /**
   * @description
   * Obtiene la geolocalizacion sólo una vez
   * @returns {observer}: boolean | parsedError:{string}
   */
  public getCurrentPosition():Observable<boolean>{
    return Observable.create(observer=> {
      try{
        Utils.LOG.i(TAG, "Intentando obtener su ubicacion...", "getCurrentPosition()");

        /*/fixme*******************************************************************
         //*                             PARA TESTING                             *
         //************************************************************************
         Utils.LOG.w(TAG, "*** getCurrentPosition() en modo DEBUG!", "getCurrentPosition()");
         setTimeout(()=>{
         this.onBGLocationSuccesCbk(...);
         observer.next(true);
         Utils.LOG.d(TAG, "Fin getCurrentPosition() en modo DEBUG!", "getCurrentPosition()");
         }, 5*GLOBALS.ONE_SECOND);
         return;
         //************************************************************************/


        this.BG.start().then((resp) => {
          //Sólo devuelve "OK"
          observer.next(true);
        }, error =>{
          observer.error(Utils.LOG.e(TAG, error, "getCurrentPosition->this.BG.start()"));
        }).catch(GeolocationError=>{

          let e;
          if (GeolocationError.code === 1) {
            e = "no se otorgaron los permisos suficientes para usar la geolocalización";
          }
          else if (GeolocationError.code === 2) {
            e = "falló uno o más elementos de la geolocalización";
          }
          else if (GeolocationError.code === 3) {
            e = "se agotó el tiempo de espera de la petición";
          }

          return observer.error(
            Utils.LOG.w(TAG, (e || GeolocationError), "getCurrentPosition()->getStationaryLocation")
          );
        })

      }
      catch(e){
        observer.error(Utils.LOG.e(TAG, e, "catch{getCurrentPosition()}"));
      }
    });
  }

  private onBGLocationSuccesCbk(location: GeolocationBGServiceResponse){
    if(this.onLocationSuccessCallback != null)
      this.onLocationSuccessCallback(location);
    else
      Utils.showAlert("BGLocation::onLocationSuccessCallback no está definida");

    if(!Utils.platform.is("android"))
      this.BG.finish().then(()=>{
        //do nothing
      }, (error)=>{
        Utils.LOG.w(TAG, "No se pudo ejecutar BG.finish() ya que: "+JSON.stringify(error), "onBGLocationSuccesCbk()->this.BG.finish()");
      });
  }

  private onBGLocationErrorCbk(error){
    if(this.onLocationErrorCallback != null)
      this.onLocationErrorCallback(Utils.LOG.e(TAG, error, "initPlugin()->BG.configure->onError()"));
    else
      Utils.showAlert("BGLocation::onLocationErrorCallback no está definida");
  }

  public stopBGLocation(){
    this.BG.stop().then(()=>{
      Utils.LOG.i(TAG, "Geolocalización detenida satisfactoriamente.", "stopBGLocation->this.BG.stop->onSuccess()");
    }, error=>{
      Utils.LOG.e(TAG, "No sé si regresó algo aqui: "+error, "stopBGLocation->this.BG.stop->onError()");
    });
  }
}

export interface GeolocationBGServiceResponse{
  /**
   * Service provider
   */
  serviceProvider: string; //"network | gps"

  /**
   * A Coordinates object defining the current location
   */
  coords: Coordinates;
  /**
   * A timestamp representing the time at which the location was retrieved.
   */
  timestamp: number;
}
