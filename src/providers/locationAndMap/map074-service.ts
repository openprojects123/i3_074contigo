import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {
  LocalDbSvc,
  catTramosType,
  latlngType,
  incidenciaType,
  neighborType,
  coordsTramoType,
  coordsPOIType,
  catPOIType
} from "../localDb-service";
import {MapsGoogleService} from "./mapsGoogle-service";
import {Utils} from "../utils-service";
import { Storage } from '@ionic/storage';
import {GLOBALS} from "../../app/globals";
import {APIType} from "../app-data-provider";



let TAG = "Map074Service";
let Gmaps: MapsGoogleService;


declare let google:any;

let POIContainer     = [];
let incidsContainer  = [];
let tramosContainer  = [];
let obrasContainer   = [];
let roi        = null;//Cuadrado de ubicame
let flecha     = null;//linea hacia el tramo mas cercano
let totalTramosSelected = 0;
let totalPOISelected    = 0;
let showIncidencias = false;
//let showPOI         = false;
let showObras       = false;
let showTrafico     = false;

@Injectable()
export class Map074Service {
  private styles = {
    //default: null,
    day:
      [
        {
          "featureType": "administrative",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#444444"
            }
          ]
        },
        {
          "featureType": "landscape",
          "elementType": "all",
          "stylers": [
            {
              "color": "#ebf0e3"
            }
          ]
        },
        {
          "featureType": "poi.attraction",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "poi.business",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "poi.government",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "poi.medical",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "on"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#c3dcc3"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#1e5e09"
            }
          ]
        },
        {
          "featureType": "poi.place_of_worship",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "poi.school",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "poi.sports_complex",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "all",
          "stylers": [
            {
              "saturation": -100
            },
            {
              "lightness": 45
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "simplified"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "road.arterial",
          "elementType": "geometry",
          "stylers": [
            {
              "visibility": "on"
            }
          ]
        },
        {
          "featureType": "road.arterial",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#0d5b18"
            }
          ]
        },
        {
          "featureType": "road.arterial",
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "road.local",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#808d83"
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "all",
          "stylers": [
            {
              "color": "#2994ff"
            },
            {
              "visibility": "on"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#ffffff"
            }
          ]
        }
      ],
    night:
      [
        {
          "featureType": "administrative",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#f2f2f2"
            }
          ]
        },
        {
          "featureType": "administrative",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#333f42"
            }
          ]
        },
        {
          "featureType": "administrative.province",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#cbcbcb"
            }
          ]
        },
        {
          "featureType": "landscape",
          "elementType": "all",
          "stylers": [
            {
              "color": "#585e64"
            }
          ]
        },
        {
          "featureType": "poi.attraction",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "poi.business",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "poi.government",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "poi.medical",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "on"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry.fill",
          "stylers": [
            {
              "color": "#334433"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#d2e4d2"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#424242"
            }
          ]
        },
        {
          "featureType": "poi.place_of_worship",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "poi.school",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "poi.sports_complex",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "all",
          "stylers": [
            {
              "saturation": -100
            },
            {
              "lightness": 45
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "simplified"
            },
            {
              "color": "#56595b"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "road.arterial",
          "elementType": "geometry",
          "stylers": [
            {
              "visibility": "on"
            },
            {
              "color": "#787979"
            }
          ]
        },
        {
          "featureType": "road.arterial",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#a5f0ff"
            }
          ]
        },
        {
          "featureType": "road.arterial",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#314e55"
            }
          ]
        },
        {
          "featureType": "road.arterial",
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "road.local",
          "elementType": "all",
          "stylers": [
            {
              "color": "#727272"
            }
          ]
        },
        {
          "featureType": "road.local",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#cfeed7"
            }
          ]
        },
        {
          "featureType": "road.local",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#333f34"
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "all",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "all",
          "stylers": [
            {
              "color": "#0a5983"
            },
            {
              "visibility": "on"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#75d0ff"
            }
          ]
        }
      ]
  };

  private mapStylesList = [
    //strokeColor & strokeOpacity aplican solo para los tramos
    {id:0, value: "day",   title: "Modo diurno",   strokeColor: "#00F", strokeOpacity: 0.4},
    {id:1, value: "night", title: "Modo nocturno", strokeColor: "#FFF", strokeOpacity: 0.7}
  ];

  public mapStyle:{id, value, title, strokeColor, strokeOpacity:number};

  public distFromCenter = GLOBALS.DEBUG_MODE ?
          0.06    //una distancia mayor :)
          : 0.009;//Aprox +/- 1000 metros

  private catTramos:catTramosType;


  constructor(private DB: LocalDbSvc,
              public maps: MapsGoogleService,//<-necesito que la vea cierto controlador
              private storage: Storage){

    Gmaps = maps;

    if(Utils.runsOnDevice())
      this.DB.getCatTramos().subscribe(tramosCat=>{
        this.catTramos = tramosCat;
        totalTramosSelected = tramosCat.numSelected;
      }, reason=>{
        Utils.showAlert("No se pudo cargar el catálogo de tramos ya que"+reason);
      });

    this.storage.get("mapStyle").then(style=>{
      if(!style){
        this.storage.set("mapStyle", JSON.stringify(this.mapStylesList[0])).then(()=>{
          this.mapStyle = this.mapStylesList[0];
        });
      }
      else{
        this.mapStyle = JSON.parse(style);
      }
    });

    this.storage.get("mustShowIncidencias").then((resp)=>{
      showIncidencias = resp;
    });

    this.storage.get("mustShowObras").then((resp)=>{
      showObras = resp;
    });

    this.storage.get("mustShowTrafico").then((resp)=>{
      showTrafico = resp;
    });

    this.storage.get("catPOI").then((resp)=>{
      if(resp){
        let catPOI:catPOIType = JSON.parse(resp);
        totalPOISelected = catPOI.numSelected;
      }
    });
  }

  public setMapStyle(style):Observable<boolean>{
    return Observable.create(observer=>{
      if(!this.maps.isSdkLoaded()){
        Utils.showAlert("No es posible cambiar el estilo del mapa ya que este no ha sido cargado");
        observer.next(false);
        return observer.complete();
      }

      this.maps.setMapStyle(this.styles[style.value]);

      this.storage.set("mapStyle", JSON.stringify(style)).then(()=>{
        this.mapStyle = style;
        observer.next(true);
        observer.complete();
      });
    })
  }

  public getMapStylesList():Array<any>{
    return this.mapStylesList;
  }

  public getSelectedMapStyle(){
    return this.mapStyle;
  }

  public loadCatTramos():Promise<catTramosType>{
    return new Promise((resolve, reject)=>{
      this.storage.get("catTramos").then(tramosStr=>{

        if(tramosStr){
          let tramosCat = JSON.parse(tramosStr);
          totalTramosSelected = tramosCat.numSelected;
          console.info("Se encontraron "+totalTramosSelected+" tramos en localstorage");
          resolve(tramosCat);
        }
        else{
          /*if(Utils.runsOnDevice())
           this.DB.getCatTramos().subscribe(tramosCat=>{
           totalTramosSelected = tramosCat.numSelected;
           console.info("Se encontraron "+totalTramosSelected+" tramos en DB");
           resolve(tramosCat);
           }, reason=>{
           reject(reason);
           });*/
        }
      }, storageError=>{
        reject(Utils.LOG.e(TAG, storageError, "loadCatTramos->storage.get()"));
      });
    })
  }

  public setMustShowIncidencias(val:boolean){
    //console.log("setting mustShowIncidencias to val: "+val);
    this.storage.set("mustShowIncidencias", val).then((resp)=>{
      showIncidencias = val;
      //console.log("set mustShowIncidencias: "+resp)
    }, storageError=>{
      Utils.LOG.w(TAG, storageError, "setMustShowIncidencias()");
    });
  }

  public setMustShowObras(val:boolean){
    //console.log("setting setMustShowObras to val: "+val);
    this.storage.set("mustShowObras", val).then((resp)=>{
      showObras = val;
      //console.log("set setMustShowObras: "+resp)
    }, storageError=>{
      Utils.LOG.w(TAG, storageError, "setMustShowObras()");
    });
  }

  public setMustShowTrafico(val:boolean){
    //console.log("setting setMustShowTrafico to val: "+val);
    this.storage.set("mustShowTrafico", val).then((resp)=>{
      showTrafico = val;
      //console.log("set setMustShowObras: "+resp)
    }, storageError=>{
      Utils.LOG.w(TAG, storageError, "setMustShowTrafico()");
    });
  }

  public mustShowIncidencias():boolean{
    return showIncidencias;
  }

  public mustShowObras():boolean{
    return showObras;
  }

  public mustShowTrafico():boolean{
    return showTrafico;
  }

  public setTotalPOISelected(val:number){
    totalPOISelected = val;
  }

  public getTotalPOISelected():number{
    return totalPOISelected;
  }

  public setTotalTramosSelected(val:number){
    totalTramosSelected = val;
  }

  public getTotalTramosSelected():number{
    return totalTramosSelected;
  }

  public drawSelectedTramos(idsTramosArray: Array<number>):Observable<number>{
    return Observable.create(observer=>{
      let totalIdsTramos = idsTramosArray.length;
      if(totalIdsTramos === 0)return observer.next(0);
      //Utils.LOG.i(TAG, "Se dibujarán "+tramos.length+" tramos...", "drawSelectedTramos()");

      //Borro los tramos...
      //Utils.LOG.i(TAG, "Borrando "+tramosRef.length+" tramos...", "drawSelectedTramos()");
      tramosContainer.forEach(function(tr){
        tr.setMap(null);
      });
      tramosContainer = [];

      let errores = "";
      idsTramosArray.forEach((tramoId, idx)=>{
        this.DB.getCoordsByTramo(tramoId).subscribe((coordsArray:Array<latlngType>)=>{
          if(coordsArray.length){
            tramosContainer.push(
              Gmaps.drawPolyline(coordsArray, {
                strokeColor:   this.mapStyle.strokeColor, //colors[idx % colors.length],
                strokeOpacity: this.mapStyle.strokeOpacity,
                strokeWeight:  6,
              })
            );
          }

          if(idx == totalIdsTramos-1){
            if(errores)
              Utils.LOG.w(TAG, errores, "drawSelectedTramos()");

            observer.next(tramosContainer.length);
            observer.complete();
          }
        }, reason=>{
          errores+= "Tramo ["+tramoId+"]: "+reason+"<br>";
          Utils.LOG.w(TAG, "No se pudo obtener las coordenadas del tramo ["+tramoId+"] ya que"+reason, "drawSelectedTramos()");
          if(idx == totalIdsTramos-1){
            observer.next(tramosContainer.length);
            observer.complete();
          }
        });
      });
    });
  }

  /**
   * Exclusivo cuando la app corre en una PC
   * @param _idsTramos
   * @returns {any}
   */
  public drawSelectedTramos2(_idsTramos: Array<number>):Observable<number>{
    return Observable.create(observer=>{
      if(_idsTramos.length === 0)return observer.next(0);
      //Utils.LOG.i(TAG, "Se dibujarán "+tramos.length+" tramos...", "drawSelectedTramos()");

      //Borro los tramos...
      //Utils.LOG.i(TAG, "Borrando "+tramosRef.length+" tramos...", "drawSelectedTramos()");
      tramosContainer.forEach(function(tr){
        tr.setMap(null);
      });
      tramosContainer = [];

      let idsTramosString = "";
      let totalIds = _idsTramos.length;
      _idsTramos.forEach((id, idx)=>{
        idsTramosString += id+(idx == totalIds-1 ? "":",")
      });

      Utils.httpPost(
        GLOBALS.API2_URL+"tramos/coordinates",
        {idsTramosString: idsTramosString}).subscribe((API:APIType)=>{
        if(!API.result){
          return observer.error(Utils.LOG.e(TAG, API, "getTramosCoordinatesFromServer(API.result==false)"));
        }
        else if(!API.data){
          return observer.error(Utils.LOG.e(TAG, "El servidor no devolvió datos", "getTramosCoordinatesFromServer(API.result==true)"));
        }
        else{
          let catCoordinates:any = API.data;
          let totalCoords = catCoordinates.length;

          let tramos = [];
          let points = [];

          if (!totalCoords) {
            observer.next(0);
            observer.complete();
            return;
          }

          let curTramoId = catCoordinates[0].idTramo;
          for (let i = 0; i < totalCoords; i++) {
            if (catCoordinates[i].idTramo != curTramoId) {
              curTramoId = catCoordinates[i].idTramo;
              tramos.push(points);
              points = [];
            }

            points.push({lat: parseFloat(catCoordinates[i].lat), lng: parseFloat(catCoordinates[i].lng)});
          }
          tramos.push(points);

          tramos.forEach((path, idx)=>{
            //Utils.LOG.i(TAG, "PATH: "+JSON.stringify(path),"drawSelectedTramos->getTramosCoords()");
            tramosContainer.push(
              Gmaps.drawPolyline(path, {
                strokeColor:   this.mapStyle.strokeColor, //colors[idx % colors.length],
                strokeOpacity: this.mapStyle.strokeOpacity,
                strokeWeight: 6
              })
            );
          });

          observer.next(tramosContainer.length);
          observer.complete();
        }
      }, httpError =>{
        observer.error(Utils.LOG.w(TAG, JSON.stringify(httpError), "getTramosCoordinatesFromServer()"))
      })
    });
  }

  public updateTramosColor(){
    let style = this.mapStyle;
    //console.info(style);
    tramosContainer.forEach(function(tr){
      tr.setOptions({strokeColor: style.strokeColor, strokeOpacity: style.strokeOpacity});
    });
  }

  public drawSelectedObras(obras: Array<Array<latlngType>>):Observable<number>{
    return Observable.create(observer=>{
      let total = 0;
      try{

        //Utils.LOG.i(TAG, "Borrando "+obrasRef.length+" obras...", "drawSelectedObras()");
        //Borro las obras...
        obrasContainer.forEach((tr, idx)=>{
          tr.setMap(null);
        });
        obrasContainer = [];

        if(obras.length === 0){
          return observer.next(0);
        }

        //let labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        //let colors = ["#FF00FF", "#0FF"];
        obras.forEach((obra, idx)=>{
          let totalKms = obra.length;

          //Inicio de la obra
          Gmaps.placeMarker(Gmaps.createMarker({
            lat: obra[0].lat,
            lng: obra[0].lng
          }, {
            label: "i"+(idx+1)
          }));

          //Fin de la obra
          Gmaps.placeMarker(Gmaps.createMarker({
            lat: obra[totalKms-1].lat,
            lng: obra[totalKms-1].lng
          }, {
            label: "f"+(idx+1)
          }));

          obrasContainer.push(
            Gmaps.drawPolyline(obra, {
              strokeColor:   "#ff0081",//colors[idx % 2],
              strokeOpacity: .8,
              strokeWeight: 4
            })
          );

          total++;
          if(idx == obras.length-1)
            observer.next(total);
        });
      }catch(e){
        observer.next(total);
      }
    });
  }

  public drawSelectedPOI(coordsPOI: Array<coordsPOIType>):Observable<number>{
    return Observable.create(observer=>{

      //Primero Borro los marcadores, si hay...
      Utils.LOG.i(TAG, "Borrando "+POIContainer.length+" POI", "drawSelectedPOIs()");
      POIContainer.forEach(m=>{
        m.setMap(null);
      });
      POIContainer = [];

      let iconType = [
        "plazacobro",
        "gasolinera",
        "sm",
        "parador",
        "torre",
        "hotel",
        "vulca",
        "abarrotes",
        "refa",
        "comida",
        "entronques",
        "gobermatico"
      ];

      coordsPOI.forEach(poi=>{
        let marker = Gmaps.createMarker(
          {lat: poi.lat, lng: poi.lng},
          {
            icon: {
              url:    './assets/img/markers/'+iconType[poi.clavecategoria-1]+'.png',
              size:   [20, 32],
              origin: [0, 0],
              anchor: [0, 32]
            }
          }
        );

        let markerInfoWindow = Gmaps.createInfoWindow({
          content: poi.detalle,
          maxWidth: 300
        });

        marker.addListener('click', ()=>{
          Gmaps.openInfoWindow(markerInfoWindow, marker);
        });

        Gmaps.placeMarker(marker);

        POIContainer.push(marker);
      });

      observer.next(coordsPOI.length);

    });
  }

  public drawIncidencias(incidencias: Array<incidenciaType>):number{
    //console.log("Incidencias: "+JSON.stringify(incidencias));
    let numIncidencias = 0;

    //Limpio los markers de incidencias, si hay...
    incidsContainer.forEach((m)=>{
      m.setMap(null);
    });
    incidsContainer = [];

    incidencias.forEach((incid)=>{
      //console.log("Dibujando incidente: {"+incid.lat+", "+incid.lng+"}");
      let url = (incid.idtipo == 2)?
        'assets/img/markers/personas.png': 'assets/img/markers/accidente.png';


      let marker = Gmaps.createMarker(
        {lat: incid.lat, lng: incid.lng},
        {
          icon: {
            url: url,
            size: [(incid.idtipo == 2 ? 32 : 20), 32],
            origin: [0, 0],
            anchor: [(incid.idtipo == 2 ? 16 : 3), 32]
          }
        }
      );


      let markerInfoWindow = Gmaps.createInfoWindow({
        content: "Reporte: <b>"+incid.id+"</b><br>"+ incid.ficha ,
        maxWidth: 300
      });


      marker.addListener('click', function() {
        Gmaps.openInfoWindow(markerInfoWindow, marker);
      });

      incidsContainer.push(marker);

      Gmaps.placeMarker(marker);

      numIncidencias++;

    });

    Utils.LOG.i(TAG, "Se han colocado "+numIncidencias+" marcadores de incidentes", "drawIncidencias");
    return numIncidencias;
  }

  public clearIncidencias(){
    //Limpio los markers de incidencias, si hay...
    incidsContainer.forEach(function(m){
      m.setMap(null);
    });
    incidsContainer = [];
  }

  public clearObras(){
    //Borro los tramos en obras
    obrasContainer.forEach(function(m){
      m.setMap(null);
    });
    obrasContainer = [];
  }

  public getNearestTramo(latlng: latlngType, distFromCenter: number):Observable<neighborType>{
    return Observable.create(observer=>{
      //Utils.LOG.i(TAG, JSON.stringify(latlng)+"; distFromCenter: "+distFromCenter, "getNearestTramo()");
      //Paso 1:
      this.DB.findNeighbors(latlng, distFromCenter).subscribe((coordTramosAray:Array<coordsTramoType>)=>{
        let neighbor: neighborType = {
          tramo: null,
          distancia: -1
        };

        if(!coordTramosAray.length){
          observer.next(neighbor);
          return;
        }

        //Paso 2:
        let minPoint = this.getMinPoint(latlng, coordTramosAray);
        //Utils.LOG.i(TAG, "minPoint ID: "+ coordTramosAray[minPoint.id].id + "; Dist: "+minPoint.distancia, "getNearestTramo->findNeighbors()");

        if(minPoint.id > -1 && minPoint.distancia>-1){
          neighbor.tramo     = coordTramosAray[minPoint.id];
          neighbor.distancia = minPoint.distancia;
          observer.next(neighbor);
        }else{
          observer.error(
              Utils.LOG.e(TAG, "No se encontró un tramo cercano [paso 2] {dist=-1}", "getNearestTramo().findNeighbors()")
          );
        }
      }, reason=>{
        observer.error(reason);
      });
    });
  }

  public getMinPoint(latlng: latlngType, objWithCoordsArray:Array<any>):{id: number, distancia: number}{
    let minDist, id=-1;

    objWithCoordsArray.forEach((point, idx)=>{
      let dist = Math.sqrt(
        Math.pow((latlng.lat-point.lat),2) + Math.pow((latlng.lng-point.lng),2)
      );

      if(idx===0){
        minDist = dist;
        id = idx;
      }
      else{
        if(dist < minDist){
          minDist = dist;
          id = idx;
        }
      }
    });

    if(id == -1){
      Utils.LOG.w(TAG, "No se puede el punto mínimo ya que el el id es = -1", "getMinPoint()");
      return {id: id, distancia: -1};
    }

    Utils.LOG.i(TAG, "Tramo mas cercano: "+JSON.stringify(objWithCoordsArray[id]), "getMinPoint()");

    Utils.LOG.i(TAG, "Calculando distancia...", "getMinPoint()");
    //Distancia en Metros
    let dist = Gmaps.calcDistance(
      latlng,
      {lat: objWithCoordsArray[id].lat, lng: objWithCoordsArray[id].lng}
    );

    return {id: id, distancia: dist};
  }

  /**
   * Obtiene la incidencia mas cercana a mi ubicacion
   * @param location
   * @param incidenciasArray
   * @returns {{id: number, dist: number}}
   */
  public getClosestIncidencia(location: latlngType, incidenciasArray:Array<incidenciaType>):{id: number, distancia: number}{
    //console.log(JSON.stringify(myLocation));
    //return Observable.create(observer=>{
    if(!incidenciasArray.length){
      //observer.next({id: -1, dist: -1});
      return {id: -1, distancia: -1};
    }

    //observer.next(this.getMinPoint(location, incidenciasArray));
    return this.getMinPoint(location, incidenciasArray);
    //});
  }

  /**
   * Pinta una "Region De Interés"
   * @param center
   * @param distFromCenter
   */
  public drawROI(center: latlngType, distFromCenter: number, clearLast=true){

    //Elimino la ROI anterior si había...
    if(roi && clearLast){roi.setMap(null);}

    Utils.LOG.i(TAG, "Dibujando la Región de Interés...", "drawROI()");
    //Coordenadas de un cuadrado de lados = 2*distFromCenter
    let roiCoordinates = [
      {lat: center.lat - distFromCenter, lng: center.lng - distFromCenter},
      {lat: center.lat - distFromCenter, lng: center.lng + distFromCenter},
      {lat: center.lat + distFromCenter, lng: center.lng + distFromCenter},
      {lat: center.lat + distFromCenter, lng: center.lng - distFromCenter}
    ];

    roi = Gmaps.drawPolygon(roiCoordinates, {fillOpacity: 0.1});
  }


  public drawArrow(pointA: latlngType, pointB: latlngType, clearLast?:boolean){
    clearLast = clearLast || true;
    //Si ya habia una, la elimino...
    if(flecha && clearLast){flecha.setMap(null);}

    //Dibujo la nueva...
    flecha = Gmaps.drawArrow({
      pointA: pointA,
      pointB: pointB,
    });
  }

}
