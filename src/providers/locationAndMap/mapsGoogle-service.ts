import { Injectable }       from '@angular/core';
import {Observable}         from 'rxjs/Rx';
import {Utils}              from "../utils-service";

declare let google:any;
let lastMapObj:any;//referencia al último mapa creado

let TAG = "MapsGoogleService";

@Injectable()
export class MapsGoogleService {
  private apiKey:         string;
  private defaultZoom     = 13;
  public showTrafficLayer = false;

  private isLoadingSDK = false;

  /**
   * Referencia a los servivicios de GoogleMaps
   * Por si es necesario usar alguna funcion distinta a las que
   * he incluido en esta clase... se considerarán para incluirse posteriormente
   * Dependiendo de su relevancia :)
   */
  public googleReference:any;

  constructor() {
    let subsc = this.loadSDK().subscribe(()=>{
      //do nothing
      subsc.unsubscribe();
      this.googleReference = google;
    }, reason=>{
      Utils.LOG.e(TAG, reason, "constructor");
    });
  }

  public setShowTrafficLayer(val:boolean){
    this.showTrafficLayer = val;
  }

  public setZoom(val:number){
    this.defaultZoom = val;
  }

  public hasMap():boolean{
    return lastMapObj != null;
  }

  public create(htmlContainer:any, center:latlngType):Observable<any>{
    return Observable.create(observer=>{
      if(!center.lat || !center.lng ){
        return observer.error(Utils.LOG.w(TAG, "no se recibió la ubicación inicial", "create()"))
      }
      else if(!htmlContainer){
        return observer.error(Utils.LOG.w(TAG, "no se recibió un contenedor para el mapa", "create()"))
      }
      else if(!this.isSdkLoaded()){
        return observer.error(Utils.LOG.w(TAG, "el SDK no ha sido cargado", "create()"))
      }

      lastMapObj = this.createMap(htmlContainer, center);
      let map = lastMapObj;
      if(map)
        observer.next(map);
      else
        observer.error(Utils.LOG.w(TAG, "ocurrio un error desconocido", "create()"));
    })
  }

  public loadSDK():Observable<boolean>{
    return Observable.create(observer=>{

      if(this.isSdkLoaded()){
        Utils.LOG.i(TAG, "GoogleMaps SDK already loaded", "loadSDK()");
        observer.next(true);
        return observer.complete();
      }

      Utils.LOG.i(TAG, "Inicializando GoogleMaps SDK", "loadSDK()");
      //Load the SDK
      window['mapInit'] = () => {
        this.isLoadingSDK = false;

        Utils.LOG.i(TAG, "GoogleMaps SDK inicializado correctamente", "loadSDK()");
        observer.next(true);
        observer.complete();
      };

      if(this.isLoadingSDK)return;

      this.isLoadingSDK = true;

      let script = document.createElement("script");
      script.id  = "googleMaps";

      if(this.apiKey){
        script.src = 'https://maps.googleapis.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit&libraries=geometry&sensor=false&v=3';
      } else {
        script.src = 'https://maps.googleapis.com/maps/api/js?callback=mapInit&libraries=geometry&sensor=false&v=3';
      }

      if(!document.body.appendChild(script)){
        observer.error(
          Utils.LOG.e(TAG, "no se pudo cargar el SDK de los mapas", "loadSDK()")
        );
      }
    })
  }

  public isSdkLoaded() {
    try{
      return (typeof google != "undefined" && typeof google.maps != "undefined");
    }catch(e){
      Utils.LOG.w(TAG, e, "catch{isSdkLoaded()}");
      return false;
    }
  }

  private createMap(mapContainer:any, center:latlngType):any {
    if(!this.isSdkLoaded()){
      return Utils.LOG.w(TAG, "el SDK no ha sido cargado", "createMap()");
    }

    let map = new google.maps.Map(mapContainer, {
      disableDefaultUI: true,
      center:           center,
      zoom:             this.defaultZoom,
      zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_CENTER
      },
      mapTypeControl:    false,
      streetViewControl: false,
      zoomControl:       true
    });

    if(!map){
      Utils.LOG.w(TAG, "No se pudo crear el mapa", "createMap()");
      return;
    }

    if(this.showTrafficLayer){
      let trafficLayer = new google.maps.TrafficLayer();
      trafficLayer.setMap(map);
    }

    //let myMap = map;
    // Add a style-selector control to the map.
    //let styleControl = document.getElementById('style-selector-control');
    //map.controls[google.maps.ControlPosition.TOP_RIGHT].push(styleControl);

    map.infowindow = null;
    map.marker     = null;

    Utils.LOG.i(TAG, "Mapa creado correctamente", "createMap()");
    return map;
  }

  public createCustomMap(mapContainer:any, options:any):any {
    if(!this.isSdkLoaded()){
      return Utils.LOG.w(TAG, "el SDK no ha sido cargado", "createCustomMap()");
    }

    let map = new google.maps.Map(mapContainer, options);

    if(!map){
      Utils.LOG.w(TAG, "No se pudo crear el mapa", "createCustomMap()");
      return;
    }

    Utils.LOG.i(TAG, "Custom Map creado correctamente", "createCustomMap()");
    return map;
  }

  public createTrafficLayer():any{
    if(!this.isSdkLoaded()){
      return Utils.LOG.w(TAG, "el SDK no ha sido cargado", "createTrafficLayer()");
    }

    let tl = new google.maps.TrafficLayer();

    if(!tl){
      return Utils.LOG.w(TAG, "No se pudo crear la capa de tráfico", "createTrafficLayer()");
    }

    return tl;
  }

  public setMapStyle(style, yourMap?){
    if(!this.isSdkLoaded()){
      return Utils.LOG.w(TAG, "el SDK no ha sido cargado", "setMapStyle()");
    }

    (yourMap || lastMapObj).setOptions({styles: style});
  }

  public getLastMap(){return lastMapObj;}

  public getMapCenter(yourMap?):latlngType{
    if(!this.isSdkLoaded()){
      Utils.LOG.w(TAG, "el SDK no ha sido cargado", "getMapCenter()");
      return;
    }

    return {
      lat: (yourMap||lastMapObj).getCenter().lat(),
      lng: (yourMap||lastMapObj).getCenter().lng()
    };
  }

  public setMapCenter(latlng:latlngType, yourMap?) {
    if(!this.isSdkLoaded()){
      return Utils.LOG.w(TAG, "el SDK no ha sido cargado", "setMapCenter()");
    }
    (yourMap||lastMapObj).setCenter(latlng);
  }

  public reloadMap(yourMap?){
    if(!this.isSdkLoaded()){
      return Utils.LOG.w(TAG, "el SDK no ha sido cargado", "reloadMap()");
    }

    Utils.LOG.i(TAG, "Centrando mapa...", "reloadMap()");

    google.maps.event.trigger((yourMap||lastMapObj), 'resize');

    (yourMap||lastMapObj).setCenter(this.getMapCenter());
    (yourMap||lastMapObj).setZoom(this.defaultZoom);
  }

  public drawPolyline(path, config, yourMap?){
    if(!this.isSdkLoaded()){
      return Utils.LOG.w(TAG, "el SDK no ha sido cargado", "drawPolyline()");
    }

    try{
      config.path = path;
      config.map  = (yourMap||lastMapObj);

      /* Otros posibles valores
       strokeColor:   "#0000FF",
       strokeOpacity: 1.0,
       strokeWeight:  3
       */

      return new google.maps.Polyline(config);
    }
    catch(e){
      Utils.LOG.i(TAG, e.message, "drawPolyline()");
    }
  }

  /**
   * Dibuja un poligono
   * @param path
   * @param config
   * @returns {any}
   */
  public drawPolygon(path: Array<latlngType>, config, yourMap?){
    if(!this.isSdkLoaded()){
      return  Utils.LOG.w(TAG, "el SDK no ha sido cargado", "drawPolygon()");
    }

    config.path = path;
    config.map  = (yourMap||lastMapObj);

    /*
     Otros posibler valores para config:
     strokeColor:    '#000',
     strokeOpacity: 1.0,
     strokeWeight:  2,
     fillColor:     '#fff',
     fillOpacity:   0.1
     */

    return new google.maps.Polygon(config);
  }

  public drawArrow(config, yourMap?){
    if(!this.isSdkLoaded()){
      return Utils.LOG.w(TAG, "el SDK no ha sido cargado", "drawArrow()");
    }

    let lineSymbol = {
      path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
    };

    // Create the polyline and add the symbol via the 'icons' property.
    return new google.maps.Polyline({
      path: [config.pointA, config.pointB],
      icons: [{
        icon: lineSymbol,
        offset: '100%'
      }],
      strokeColor: config.strokeColor || '#FF0000',
      map: (yourMap||lastMapObj)
    });
  }

  //En metros
  public calcDistance(pointA, pointB){
    try{
      if(this.isSdkLoaded()){
        //Utils.LOG.i(TAG, "calcDistance: ["+JSON.stringify(pointA)+"; "+JSON.stringify(pointB)+"]", "calcDistance()");
        return google.maps.geometry.spherical.computeDistanceBetween (
          new google.maps.LatLng(pointA.lat, pointA.lng),
          new google.maps.LatLng(pointB.lat, pointB.lng)
        );
      }

      Utils.LOG.w(TAG, "No se cargó la librería de mapas", "calcDistance()");
      //Utils.LOG.w(TAG, JSON.stringify(pointA)+"->"+JSON.stringify(pointB), "calcDistance()");

      /**
       * SOURCE:  http://www.movable-type.co.uk/scripts/latlong.html
       */
      let toRadians = (val)=>{return Math.PI * val/180;};

      let lat1 = pointA.lat;
      let lon1 = pointA.lng;

      let lat2 = pointB.lat;
      let lon2 = pointB.lng;

      let R = 6371e3; // metres
      let p1 = toRadians(lat1);
      let p2 = toRadians(lat2);
      let d1 = toRadians(lat2-lat1);
      let d2 = toRadians(lon2-lon1);

      let a = Math.sin(d1/2) * Math.sin(d1/2) +
        Math.cos(p1) * Math.cos(p2) *
        Math.sin(d2/2) * Math.sin(d2/2);
      let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

      return R * c;//en metros
    }
    catch(e){
      Utils.LOG.e(TAG, e, "calcDistance()");
      return -1;
    }
  }

  public setCenterChangedListener(callbackFn:(newCenter:latlngType)=>void, yourMap?){
    if(!this.isSdkLoaded()){
      return Utils.LOG.w(TAG, "el SDK no ha sido cargado", "setCenterChangedListener()");
    }
    google.maps.event.addListener(yourMap || lastMapObj, "center_changed", ()=>{
      callbackFn(this.getMapCenter((yourMap||lastMapObj)));
    });
  }

  public setClickMapListener(callbackFn:(position:latlngType)=>void, yourMap?) {
    if(!this.isSdkLoaded()){
      return  Utils.LOG.w(TAG, "el SDK no ha sido cargado", "setClickMapListener()");
    }
    google.maps.event.addListener((yourMap||lastMapObj), 'click', e=>{
      callbackFn(e.latLng);
    });
  }

  public createMarker(position: latlngType, config){
    if(!this.isSdkLoaded()){
      return Utils.LOG.w(TAG, "el SDK no ha sido cargado", "createMarker()");
    }

    config['position'] = position;//new google.maps.LatLng(position.lat, position.lng);

    /*Algunos de los posibles valores
     {
     map: google.maps,
     position: {lat, lng},
     animation:google.maps.Animation.DROP || google.maps.Animation.BOUNCE,
     icon: {
     url: string,
     size:   new google.maps.Size(n,m),
     origin: new google.maps.Point(x,y),
     anchor: new google.maps.Point(x,y])
     },
     label: string
     }*/

    if(config.icon){
      config.icon.size   = new google.maps.Size(config.icon.size[0], config.icon.size[1]);
      config.icon.origin = new google.maps.Point(config.icon.origin[0], config.icon.origin[1]);
      config.icon.anchor = new google.maps.Point(config.icon.anchor[0], config.icon.anchor[1])
    }

    return new google.maps.Marker(config);
  }

  public placeMarker (marker, yourMap?) {
    marker.setMap((yourMap||lastMapObj));
  }

  public deleteMarker(marker:any){
    if(!this.isSdkLoaded()){
      return Utils.LOG.w(TAG, "el SDK no ha sido cargado", "deleteMarker()");
    }
    marker.setMap(null);
  }

  public createInfoWindow(config){
    if(!this.isSdkLoaded()){
      return Utils.LOG.w(TAG, "el SDK no ha sido cargado", "createInfoWindow()");
    }

    /*
     * Algunos posibles valores:
     {
       content: string,
       maxWidth: number
     }
     */

    return new google.maps.InfoWindow(config);
  }

  public openInfoWindow(infoWindow, obj, yourMap?){
    infoWindow.open((yourMap||lastMapObj), obj);
  }

  public openMarkerInfoWindow(infoWindow, marker, yourMap?){
    infoWindow.open((yourMap||lastMapObj), marker);
  }

  public centerMapMarkers(markers:any, yourMap?){
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0; i < markers.length; i++) {
      bounds.extend(new google.maps.LatLng(markers[i].lat, markers[i].lng));
    }
    (lastMapObj||yourMap).fitBounds(bounds);
  }

  public calcRoadDistanceAndDuration(origin:latlngType, destination:latlngType):Observable<roadDistDurationType>{
    return Observable.create(observer=> {
      if(!this.isSdkLoaded()){
        return observer.error(Utils.LOG.e(TAG, "el SDK no ha sido cargado", "calcRoadDistanceAndDuration()"));
      }

      let objConfigDS ={
        origin:       origin,
        destination:  destination,
        travelMode:   google.maps.TravelMode.DRIVING,
        provideRouteAlternatives:false
      };

      let onSuccess = (resultados, status)=>{
        if(status == 'OK'){
          let resultDistance   = resultados.routes[0].legs[0].distance;//.text;
          let resultDuration   = resultados.routes[0].legs[0].duration;//.text;

          observer.next({
            status:     true,
            statusText: status,
            distance:   resultDistance,
            duration:   resultDuration
          });
          observer.complete();
        }else{
          observer.next({
            status:     false,
            statusText: status,
            distance:   -1,
            duration:   -1
          });
          observer.complete();
        }
      };

      let ds = new google.maps.DirectionsService();
      ds.route(objConfigDS, onSuccess);
    })
  }

}

export interface roadDistDurationType{
  status:     boolean,
  statusText: string,
  distance:   {text:string, value:number},
  duration:   {text:string, value:number}
}


export interface latlngType{
  lat:number, lng: number
}
