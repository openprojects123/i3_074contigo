import { Injectable }           from '@angular/core';
import 'rxjs/add/operator/map';
import {
  Observable,
  BehaviorSubject
}                               from "rxjs";
import { Storage }              from '@ionic/storage';
import { GLOBALS }              from "../app/globals";
import { Utils }                from "./utils-service";

let TAG = "UserService";

@Injectable()
export class UserService {

  private id:         number;
  private token:      string;
  private username:   string;
  private rememberMe: boolean = false;
  private extraInfo:  Object;

  private loginStatus    = false;
  private registerStatus = false;
  private comfirmStatus  = false;

  //"Publica" el estado actual del login
  public onLoginStateChange: BehaviorSubject<boolean>;

  //public onLocalUserLoaded: BehaviorSubject<boolean>;

  constructor(private storage: Storage) {
    //this.onLocalUserLoaded  = new BehaviorSubject<boolean>(false);
    this.onLoginStateChange = new BehaviorSubject<boolean>(this.loginStatus);

    this.loadLocalUser().subscribe(resp_bool=>{
      this.onLoginStateChange.next(this.loginStatus)
    },(e)=>{
      Utils.LOG.w(TAG, e, "constructor")
    });
  }

  public getId():number{return this.id}

  public getToken():string{return this.token}

  public getUsername(){return this.username}

  public getExtraInfo(){return this.extraInfo}

  public isLoggedIn(){ return this.loginStatus}

  public isRegistered(){return this.registerStatus}

  public isComfirmed(){return this.comfirmStatus}

  public mustRemember(){return this.rememberMe}

  public setExtraInfo(extraInfo: Object):Observable<boolean>{
    return Observable.create(observer=>{
      this.storage.get("User").then(user=>{
        if(!user){
          observer.error(TAG, "No existe el usuario en memoria.", "setExtraInfo()")
          return;
        }

        let localUser = JSON.parse(user);
        localUser.extraInfo = extraInfo;

        this.storage.set("User", JSON.stringify(localUser)).then(()=>{
          observer.next(true);
        }, e=>{
          observer.error(Utils.LOG.e(TAG, e, "setExtraInfo->storage.set(User)"))
        });
      }, e=>{
        observer.error(Utils.LOG.e(TAG, e, "setExtraInfo->storage.get(User)"))
      })/*.catch(storageError=> {
       observer.error(Utils.LOG.e(TAG, storageError, "setExtraInfo->catch{storage...}"))
       });*/
    })
  }

  public loadLocalUser():Observable<boolean>{
    return Observable.create(observer=>{
      //Utils.LOG.i(TAG, "Cargando Usuario desde localstorage...", "loadLocalUser()");

      this.registerStatus = false;
      this.comfirmStatus  = false;
      this.loginStatus    = false;

      this.storage.get("User").then(usr=>{
        if(!usr){
          Utils.LOG.w(TAG, "Los datos del usuario son nulos.", "loadLocalUser()");
          observer.next();
          return observer.complete();
        }

        let localUser;
        localUser = JSON.parse(usr);

        if(!localUser.id){
          Utils.LOG.e(TAG, "El id del usuario es nulo.", "loadLocalUser()");
          observer.next();
          return observer.complete();
        }

        this.id        = localUser.id;
        this.token     = localUser.token;
        this.username  = localUser.username;
        this.rememberMe = localUser.rememberMe;
        this.extraInfo = localUser.extraInfo;

        this.registerStatus = this.rememberMe;
        this.comfirmStatus  = this.rememberMe;
        this.loginStatus    = this.rememberMe;

        observer.next(localUser);
        return observer.complete();

      },storageError=>{
        observer.error(Utils.LOG.w(TAG, JSON.stringify(storageError), "loadLocalUser()"));
      });
    });
  }

  /**
   * Almacena los datos del usuario en memoria (cuando NO se activa la bandera rememberMe)
   * @param user
   */
  private persistUserInMemory(user: {id:number, token: string, username:string, rememberMe: boolean, extraInfo:Object}){
    Utils.LOG.i(TAG, "Almacenando los datos de usuario en memoria...", "persistUserInMemory()");
    this.id         = user.id;
    this.token      = user.token;
    this.username   = user.username;
    this.rememberMe = user.rememberMe;
    this.extraInfo  = user.extraInfo;
  }

  /**
   * Almacena los datos del usuario en LocalStorage (cuando SI se activa la bandera rememberMe)
   * @param user
   */
  private persistUserInLocalStorage(user: {id:number, token: string, username:string, rememberMe: boolean, extraInfo:Object}):Observable<boolean>{
    return Observable.create(observer=>{
      Utils.LOG.i(TAG, "Almacenando los datos de usuario en localStorage...", "persistUserInLocalStorage()");
      this.storage.set("User", JSON.stringify(user)).then(()=>{
        //Utils.LOG.i(TAG, "Los datos del usuario han sido almacenados en local storage!", "persistUserInLocalStorage()");
        this.persistUserInMemory(user);
        observer.next(true);
      }).catch(storageError=>{
        observer.error(
          Utils.LOG.e(TAG, JSON.stringify(storageError), "persistUserInLocalStorage().set(User)")
        )
      })
    })
  }

  public clearUserData():Observable<boolean>{
    return Observable.create(observer=>{
      this.storage.remove("User").then(()=>{
        observer.next(true);
        observer.complete();
      }, error=>{
        observer.error(Utils.LOG.w(TAG, error, "clearUserData->storage.remove('User')"));
      })
    });
  }

  //******************** METODOS PARA EL MANEJO DE LA SESION ************************
  public login(loginField:string, passwordField:string, rememberMeField:boolean):Observable<boolean>{
    return Observable.create(observer=>{
      /*/fixme: ********************** FOR TESTING **********************
       Utils.showToast("Login en modo TESTING",{duration:5*GLOBALS.ONE_SECOND});
       let usr = {
       id:         -1,
       token:      "NO_TOKEN",
       username:   loginField,
       rememberMe: rememberMeField,
       extraInfo:  {nombre:'Fake', paterno:'User',telefonos:[]}
       };

       this.persistUserInLocalStorage(usr).subscribe(bool_resp=>{
       this.registerStatus = true;
       this.comfirmStatus  = true;
       this.loginStatus    = true;

       setTimeout(()=>{
       observer.next(true);
       observer.complete();
       this.onLoginStateChange.next(this.loginStatus);
       }, 4*GLOBALS.ONE_SECOND);

       }, reason=>{
       observer.error(reason);
       });
       if(1<2)return;
       //*********************** END FOR TESTING **********************/

      let data = {
        loginField:    loginField,
        passwordField: passwordField
      };

      Utils.httpPost(GLOBALS.API_URL+"usuario/login", data).subscribe(API=>{
        if(!API.result){
          return observer.error(Utils.LOG.e(TAG, API, "login()"));
        } else if(!API.data){
          return observer.error(Utils.LOG.e(TAG, "ocurrió un error interno en el servidor", "login()"));
        }

        //Utils.LOG.i(TAG, "User data: "+JSON.stringify(API.data), "login")

        let usr = {
          id:        API.data.id,
          token:     API.data.token,
          username:  API.data.username,
          rememberMe: rememberMeField,
          extraInfo: API.data.extraInfo
        };

        this.persistUserInLocalStorage(usr).subscribe(resp_bool=>{
          this.registerStatus = true;
          this.comfirmStatus  = true;
          this.loginStatus    = true;

          observer.next(this.loginStatus);
          observer.complete();
          this.onLoginStateChange.next(this.loginStatus);
        },reason=>{
          observer.error(reason);
        });
      }, reason=>{
        observer.error(Utils.LOG.e(TAG, reason, "login()"));
      });
    })
  }//<--login()

  public logout():Observable<boolean>{
    return Observable.create(observer=>{
      this.comfirmStatus  = false;
      this.loginStatus    = false;
      this.registerStatus = false;
      this.rememberMe     = false;

      this.clearUserData().subscribe(()=>{
        this.onLoginStateChange.next(this.loginStatus);
        observer.next(true);
      }, reason=>{
        observer.error(
          Utils.LOG.w(TAG, "No se pudo limpiar los datos de sesión ya que"+reason, "logout->clearUserData()")
        );
      });
    });
  }

  public register(formData:any):Observable<any>{
    return Observable.create(observer=>{
      formData.passwordField = formData.passwordField.toString();

      /*/fixme: User-service REGISTER() FOR TESTING...************************
       if(1>0){
       Utils.LOG.e(TAG, "User-service REGISTER() FOR TESTING...", "register()");
       return observer.next(true);
       }
       //*********************************************************************/
      Utils.httpPost(GLOBALS.API_URL+"usuario/new", formData).subscribe(API=> {
        if (!API.result) {
          return observer.error(Utils.LOG.e(TAG, API, "register()"));
        }
        else if (!API.data[0]) {
          return observer.error(Utils.LOG.e(TAG, "ocurrio un error desconocido en el servidor", "register()"));
        }

        this.registerStatus = true;

        observer.next(API.data);
        observer.complete()
      }, httpError=>{
        observer.error(Utils.LOG.e(TAG, httpError, "register()"));
      })
    });
  }

  public changePassword(uid, oldPass, newPass):Observable<any>{
    return Observable.create(observer=> {
      Utils.httpPut(GLOBALS.API_URL + "usuario/passChange", {userId:uid, oldPassword:oldPass, newPassword:newPass}).subscribe(API => {
        if (!API.result) {
          observer.error(Utils.LOG.e(TAG, API, "passChange()"));
        }
        else if (!API.data) {
          observer.error(Utils.LOG.e(TAG, "ocurrio un error desconocido en el servidor", "passChange()"));
        }

        observer.next(API.data);
        observer.complete();
      }, httpError=>{
        observer.error(Utils.LOG.e(TAG, httpError, "passChange()"));
      })
    })
  }

  public recoveryPassword(username):Observable<any>{
    return Observable.create(observer=> {
      Utils.httpPost(GLOBALS.API_URL + "usuario/passRecovery", {loginField:username}).subscribe(API => {
        if (!API.result) {
          observer.error(Utils.LOG.e(TAG, API, "passRecovery()"));
        }
        else if (!API.data) {
          observer.error(Utils.LOG.e(TAG, "ocurrio un error desconocido en el servidor", "passRecovery()"));
        }

        observer.next(API.data);
        observer.complete();
      }, httpError=>{
        observer.error(Utils.LOG.e(TAG, httpError, "passRecovery()"));
      })
    })
  }
}
