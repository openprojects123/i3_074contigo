import { Injectable } from '@angular/core';
import {
  Http,
  Headers,
  RequestOptions
}                     from '@angular/http';
import {Observable}   from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import {GLOBALS}      from "../app/globals";
import {DomSanitizer} from "@angular/platform-browser";
import {
  LoadingController,
  AlertController,
  ToastController,
  ActionSheetController,
  Platform,
  ActionSheetOptions,
  ToastOptions,
  LoadingOptions,
  AlertOptions
}                     from 'ionic-angular';


let TAG = "Utils";

@Injectable()
export class Utils {

  private static http:            Http;
  private static loadingCtrl:     LoadingController;
  private static alertCtrl:       AlertController;
  private static sanitizer:       DomSanitizer;
  private static toastCtrl:       ToastController;
  private static actionSheetCtrl: ActionSheetController;
  private static _runsOnDevice:   boolean;

  public static platform:         Platform;

  constructor(private _http: Http,
              private _loadingCtrl:     LoadingController,
              private _alertCtrl:       AlertController,
              private _sanitizer:       DomSanitizer,
              private _toastCtrl:       ToastController,
              private _actionSheetCtrl: ActionSheetController,
              private _platform:        Platform)
  {
    Utils.http            = _http;
    Utils.loadingCtrl     = _loadingCtrl;
    Utils.alertCtrl       = _alertCtrl;
    Utils.sanitizer       = _sanitizer;
    Utils.toastCtrl       = _toastCtrl;
    Utils.actionSheetCtrl = _actionSheetCtrl;
    Utils.platform        = _platform;
    Utils._runsOnDevice   = _platform.is("cordova");
  }

  static assembleHTMLItem(content:string) {
    return this.sanitizer.bypassSecurityTrustHtml(content);
  }

  /**
   * para saber si se esta ejecutando en un dispositivo movil (true) o en explorador web (false)
   * @returns {boolean}
   */
  static runsOnDevice():boolean{
    return this._runsOnDevice;
  }

  //Objeto generico configurable: Alertas, Confirm, Prompt
  static createAlertCtrl(options: AlertOptions) {
    return this.alertCtrl.create(options);
  }

  //alert
  static showAlert(message:string, config?:{
    title?:string,
    subTitle?:string,
    enableBackdropDismiss?: boolean,
    cancelText?:string,
    cancelHandler?: (value: any) => boolean | void,
    okText?:string,
    okHandler?:(value?: any) => boolean | void,
    cssClass?:string
  }):Promise<any> {
    config = config || {};
    return this.createAlertCtrl({
      title:   config.title || "Advertencia",
      message: message,
      enableBackdropDismiss: config.enableBackdropDismiss && true,
      cssClass: config.cssClass,
      buttons: [{
        text: config.okText || 'Aceptar',
        handler: config.okHandler
      }]
    }).present();
  }

  //confirm
  static showConfirm(
    message:string,
    config:{
      title?:string,
      subTitle?:string,
      enableBackdropDismiss?: boolean,
      cancelText?:string,
      cancelHandler?: (value: any) => boolean | void,
      okText?:string,
      okHandler:(value?: any) => boolean | void,
      cssClass?:string
    }
  ):Promise<any>{
    return this.createAlertCtrl({
      title: config.title || "Confirmar acción",
      subTitle: config.subTitle,
      message: message,
      enableBackdropDismiss: config.enableBackdropDismiss && true,
      cssClass: config.cssClass,
      buttons: [{
        role: "cancel",
        text: config.cancelText || 'Cancelar',
        handler: config.cancelHandler
      },
        {
          text: config.okText || 'Aceptar',
          handler: config.okHandler
        }]
    }).present();
  }

  static showPrompt(config:{
    title:string,
    subTitle?:string,
    message?:string,
    cssClass?:string,
    inputs:[any],
    buttons:[any],
    enableBackdropDismiss?: boolean
  }):Promise<any> {
    return this.createAlertCtrl({
      title:    config.title,
      subTitle: config.subTitle,
      message:  config.message,
      cssClass: config.cssClass,
      inputs:   config.inputs,
      buttons:  config.buttons,
      enableBackdropDismiss: config.enableBackdropDismiss
    }).present();
  }

  //Objeto generico configurable
  static createToastCtrl(options:ToastOptions) {
    return this.toastCtrl.create(options);
  }

  //toast default duration: 3 seconds
  static showToast(message:string, options?:ToastOptions):Promise<any> {
    options = options || {};
    options["message"]  = message;
    if(!options.duration)
      options["duration"] = 3*GLOBALS.ONE_SECOND;

    return this.createToastCtrl(options).present();
  }

  //Objeto generico configurable
  static createActionSheetCtrl(options:ActionSheetOptions) {
    return this.actionSheetCtrl.create(options);
  }

  //actionSheet
  static showActionSheet(opts:ActionSheetOptions):Promise<any> {
    return this.createActionSheetCtrl({
      title:                 opts.title,
      subTitle:              opts.subTitle,
      cssClass:              opts.cssClass,
      enableBackdropDismiss: opts.enableBackdropDismiss,
      buttons:               opts.buttons
    }).present();
  }

  //Objeto generico configurable
  static createLoadingCtrl(options:LoadingOptions) {
    return this.loadingCtrl.create(options);
  }

  //Loader
  static showLoading(message?:string) {
    let l =  this.createLoadingCtrl({
      content: message || "Espere..."
    });

    l.present();
    return l;
  }

  //composeError
  static composeError(message:any, fromMethod?:string):ComposedError{
    let
      status:     number = -1, //HTTP Status
      statusText: string, //HTTP Status Text
      reason:     string, //errores enviados por el servicio HTTP
      url:        string; //URL consultada o método ejecutado

    if(typeof message === "string"){//Error personalizado o de algun plugin/servicio
      status     = -98;
      statusText = message;
      reason     = "Hay un error de configuración en la APP";
    }
    else if(typeof message === "object"){
      if(message.error_message){//Errores de la API (webserver)
        status     = message.status || -99;
        statusText = message.statusText;
        reason     = message.error_message
      }
      else if(message.message){//Errores del bloque try-catch
        status     = -99;
        statusText = message.message;
        reason     = "Hubo un error interno en la aplicación";
      }
      else if(message.status >= 400 && message.status <500){//Errores 4**
        status     = message.status;
        statusText = message.statusText;
        reason     = "No se encontró el recurso solicitado";
      }
      else if(message.status >= 500 && message.status <600){//Errores 5**
        let e500;
        try{
          e500 = JSON.parse(message._body);
        }
        catch(e){
          this.LOG.e(TAG, "El error 500 recibido no se pudo convertir a JSON", "composeError()");
          console.error(JSON.stringify(message))
        }
        status     = message.status;
        statusText = e500 ? e500.api_exception : message.statusText;
        reason     = "Hubo un error interno en el servidor remoto";
      }
      else if(message.reason){//Errores personalizados (por mi)
        status     = -100;
        statusText = message.statusText || "Error interno en la APP";
        reason     = message.reason;
      }
      else if(message.status ===-1 || status === -1){
        statusText = "Hubo un error desconocido";
        reason     = "Es posible que el internet se haya desconectado o que el servidor haya rechazado explícitamente la conexión";
      }
    }


    url = message.url || fromMethod || "(url/método desconocido)";

    let fullError =
      "status: "      +status+
      "; statusText: "+(statusText || "**Estatus desconocido")+
      "; reason: "    +(reason     || "**Ocurrio un error desconocido")+
      "; url: "       +url;

    let fecObj = this.getDate();
    return {
      date:       fecObj.YMD +"T"+fecObj.HMS,
      status:     status,
      statusText: statusText || "**Estatus desconocido",
      reason:     reason     || "**Ocurrio un error desconocido",
      url:        url,
      fullError:  fullError
    };
  }

  /**
   * imprime mensages simples si el mensaje es string: message, si no ComposedError.reason
   * i: ((fromClass:string, message:any, fromMethod:string)=>(any|string));
   *
   * w: ((fromClass:string, message:any, fromMethod:string)=>(any|ComposedError));
   * e: ((fromClass:string, message:any, fromMethod:string)=>ComposedError);
   * d: ((fromClass:string, message:any, fromMethod:string)=>ComposedError)}}
   */
  static LOG = {
    i: (fromClass:string, message:any, fromMethod:string)=>{
      if(!GLOBALS.DEBUG_MODE || !(GLOBALS.LOG_LEVEL === GLOBALS.LOG_INFO))return;

      fromClass = fromClass.toUpperCase();

      let outMsg = "["+fromClass+(fromMethod? "."+fromMethod:"")+" INFO]: ";

      if(typeof message === "string" ||typeof message === "boolean")
        outMsg += message;
      else{
        let compErr = Utils.composeError(message, fromClass+"."+fromMethod);
        outMsg+= (GLOBALS.DEBUG_MODE? compErr.fullError : compErr.reason);
      }

      console.log(outMsg);

      return outMsg;
    },
    w: (fromClass:string, message:any, fromMethod:string)=>{
      if(!(GLOBALS.LOG_LEVEL === GLOBALS.LOG_INFO ||
          GLOBALS.LOG_LEVEL === GLOBALS.LOG_WARN))return;

      fromClass = fromClass.toUpperCase();

      let outMsg = "["+fromClass+(fromMethod? "."+fromMethod:"")+" WARN]: ";

      let compErr = Utils.composeError(message, fromClass+"."+fromMethod);

      outMsg+= GLOBALS.DEBUG_MODE? compErr.fullError : compErr.reason;

      console.warn(outMsg);

      return ": "+(GLOBALS.DEBUG_MODE ? compErr.reason+"<hr>"+compErr.fullError : compErr.reason);
    },
    e: (fromClass:string, message:any, fromMethod:string)=>{
      fromClass = fromClass.toUpperCase();

      let outMsg = "["+fromClass+(fromMethod? "."+fromMethod:"")+" ERROR]: ";

      let compErr = Utils.composeError(message, fromClass+"."+fromMethod);

      outMsg+= GLOBALS.DEBUG_MODE? compErr.fullError : compErr.reason;

      console.error(compErr.fullError);

      return ": "+(GLOBALS.DEBUG_MODE ? compErr.reason+"<hr>"+compErr.fullError : compErr.reason);
    },
    d:(fromClass:string, message:string, fromMethod:string)=>{
      fromClass = fromClass.toUpperCase();

      let outMsg = "["+fromClass+(fromMethod? "."+fromMethod:"")+" DEBUG]: ";

      //let compErr = Utils.composeError(message, fromClass+"."+fromMethod);

      outMsg+= message;//GLOBALS.DEBUG_MODE? compErr.fullError : compErr.reason;

      console.log(outMsg);
      //console.info(Utils.LOG.d.caller)

      return ": "+(GLOBALS.DEBUG_MODE ? "<hr>"+outMsg : outMsg);
    }
  };


  //httpGet
  static httpGet(url:string){
    return this.http.get(url)
      .map(res=>res.json())
      .retry(3)
      .catch(e=>{
        console.error(`ERROR httpGet(${url})`);
        return Observable.throw(e);
      });
  }

  static httpPost(url:string, body:any, options?: any){

    if(!options){
      //let headers = new Headers({ 'Content-Type': 'application/json' });
      let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
      options = new RequestOptions({ headers: headers });
    }

    return this.http.post(url, body, options)
      .map(res=>res.json())
      .retry(3)
      .catch(e=>{
        console.error(`ERROR httpPost(${url})`);
        return Observable.throw(e);
      });
  }

  static httpPut(url:string, body:any, options?: any){

    if(!options){
      //let headers = new Headers({ 'Content-Type': 'application/json' });
      let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
      options = new RequestOptions({ headers: headers });
    }

    return this.http.put(url, body, options)
      .map(res=>res.json())
      .catch(e=>{
        console.log("ERROR httpPut("+url+")");
        return Observable.throw(e);
      });

  }

  static httpDelete(url:string, options?: any){

    if(!options){
      //let headers = new Headers({ 'Content-Type': 'application/json' });
      let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
      options = new RequestOptions({ headers: headers });
    }

    return this.http.delete(url, options)
      .map(res=>res.json())
      .catch(e=>{
        console.log("ERROR httpDelete("+url+")");
        return Observable.throw(e);
      });
  }

  //0-based month
  /**
   * Obtiene el nombre de un mes dado
   * @param month: number 0-11
   * @returns {string: nombre del mes correspondiente en español}
   */
  static getNameOfMonth(month: number){
    let meses = [
      "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
      "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
    return meses[month]
  }

  //0-based month
  /**
   * Obtiene el numero de dias que tiene un mes de un año dado
   * @param iMonth: number 0-11
   * @param iYear: number
   * @returns {number: 1-31}
   */
  static daysInMonth(iMonth, iYear) {
    return 32 - new Date(iYear, iMonth, 32).getDate();
  }

  //myFecha
  static getDate(date?:Date, opts?:string){
    let D;
    if(typeof date !== "undefined")
      D = date;
    else if(typeof opts !== "undefined")
      D = new Date(opts);
    else
      D = new Date();

    return {
      DATEOBJECT:     D, //new Date(opts?)
      DAYOFMONTH:     D.getDate(),//1-31:number
      DAYOFMONTH_STR: ("0"+D.getDate()).slice(-2),//01-31:string
      DAYSINMONTH:    (month:number, year:number)=>{return this.daysInMonth(month, year)},
      DAYOFWEEK:      D.getDay(),//0-6
      FULLYEAR:       D.getFullYear(),//YYYY
      HOURS:          D.getHours(),//0-23
      MILLISECONDS:   D.getMilliseconds(),
      SECONDS:        D.getSeconds(),//0-59
      MINUTES:        D.getMinutes(),//0-59
      MONTH:          D.getMonth(),//0-11
      MONTH_STR:      ("0"+(D.getMonth()+1)).slice(-2),//01-12:string
      NAMEOFMONTH:    this.getNameOfMonth(D.getMonth()),
      TIME:           D.getTime(),//milliseconds since January 1, 1970
      YMD:            D.toJSON().slice(0,10),//YYYY-MM-DD
      HMS:            D.toLocaleTimeString("es-MX")//hh:mm:ss
    }
  }
}

interface ComposedError{
  date:       string,
  status:     number, //HTTP Status
  statusText: string, //HTTP Status Text
  reason:     string, //errores enviados por el servicio HTTP
  url:        string;
  fullError:  string;
}
